#include <stdio.h>
#include <sstream> 
#include <iomanip>  
#include <cmath>
#include <iostream>
#include <fstream>
#include <math.h>
#include <string.h>
#include <exception>
#include <cstdlib>
#include <cstdio>
#include <algorithm>

#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"

using namespace std;

#include "./HEPTopTagger.hh"

//----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  vector<fastjet::PseudoJet> hadrons;
  
  //  jet definition 
  double conesize=1.5;
  fastjet::JetDefinition jet_def(fastjet::cambridge_algorithm,conesize);

  // Read input and convert MeV->GeV
  ifstream fin("input.dat",ifstream::in );  
  vector<fastjet::PseudoJet> input_clusters(0);
  while(!fin.eof()){
    double x,y,z,e;
    fastjet::PseudoJet p;
    fin >> x >> y >> z >> e;
    if(!fin.eof()){
      p.reset(x/1000., y/1000., z/1000., e/1000.);
      input_clusters.push_back(p);
    }
  }
  cout << "ReadEvent: " << input_clusters.size() << " particles are read" << endl;
  
  // run the jet finding; find the hardest jet
  fastjet::ClusterSequence clust_seq(input_clusters, jet_def);  
  double ptmin_jet=200.;
  vector<fastjet::PseudoJet> jets = sorted_by_pt(clust_seq.inclusive_jets(ptmin_jet));

  
  for(unsigned ijet=0; ijet<jets.size(); ijet++)
    {      
       HEPTopTagger::HEPTopTagger tagger(jets[ijet]);

       // Unclustering, Filtering & Subjet Settings
      tagger.set_max_subjet_mass(30.);
      tagger.set_mass_drop_threshold(0.8);
      tagger.set_filtering_R(0.3);
      tagger.set_filtering_n(5);
      tagger.set_filtering_minpt_subjet(30.); 

      // How to select among candidates
      tagger.set_mode(HEPTopTagger::TWO_STEP_FILTER);
  
      // Requirements to accept a candidate
      tagger.set_top_minpt(200); 
      tagger.set_top_mass_range(150., 200.); 
      tagger.set_fw(0.15); 

      // Run the tagger
      tagger.run();      

      // Look at output if we have a tag:
      if (tagger.is_tagged()){
	cout << "Input fatjet: " << ijet << "  pT = " << jets[ijet].perp() << std::endl;      
	cout << "Output: pT = " << tagger.t().perp() << " Mass = " << tagger.t().m() << " f_rec = " << tagger.f_rec() << std::endl;
      }

    } // end of top tagger
  
  //*********************
  return 0;  // end of main  
}
