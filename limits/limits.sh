#! /bin/bash

source $HOME/local/bin/thisroot.sh

cd cls_code
make

python yoda_to_cls.py ../sm/ttb.yoda
python yoda_to_cls.py ../sm/ttj.yoda

for dir in ../$1/*_dir/; do
  python yoda_to_cls.py $dir/atom_results.yoda
  for d in $dir/*.dat; do
    file=${d##*/atom_results_}
    ./test_hypo $2 ../sm/ttj_$file ../sm/ttb_$file $dir/atom_results_$file
  done
done
exit
