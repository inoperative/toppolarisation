#! /usr/bin/env python

import sys
import os
from math import *
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as mc
from matplotlib import _cntr as cntr



def grayify_cmap(cmap):
    """Return a grayscale version of the colormap"""
    cmap = plt.cm.get_cmap(cmap)
    colors = cmap(np.arange(cmap.N))

    # convert RGBA to perceived greyscale luminance
    # cf. http://alienryderflex.com/hsp.html
    RGB_weight = [0.299, 0.587, 0.114]
    luminance = np.sqrt(np.dot(colors[:, :3] ** 2, RGB_weight))
    colors[:, :3] = luminance[:, np.newaxis]

    return cmap.from_list(cmap.name + "_grayscale", colors, cmap.N)


def writeHistos(filename):
  namelist = []
  with open(filename) as f:
    outfileName = os.path.splitext(filename)[0]
    lineno = 0
    beginhisto = 100000
    printline = False
    fileout = open(filename+"_fixed", 'w')
    for line in f.readlines():
      lineno+=1
      if lineno - beginhisto == 1:
       printline = True
      if printline:
        fileout.write(line)
      if "END YODA_HISTO1D" in line:
        printline = False
      if "BEGIN YODA_HISTO1D" in line:
        words = line.split(' ')
        strings = words[-1].split('/')
        if not "0" in strings[3]:
          continue
        else:
          beginhisto = lineno
          fileout.write(line)


def main():

  writeHistos(sys.argv[1])
  os.remove(sys.argv[1])
  os.rename(sys.argv[1]+"_fixed",sys.argv[1])

if __name__ == "__main__":
  main()



