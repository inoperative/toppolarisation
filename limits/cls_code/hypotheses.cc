#include "TFile.h"
#include <TApplication.h>
#include <TGClient.h>
#include <iostream>
#include <fstream>
#include <string>
#include "TLegend.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLatex.h"
#include "mclimit_csm.h"
#include "TRandom.h"
#include "TMinuit.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TH1F.h"
#include "TF1.h"
#include "TH2F.h"
#include <stdlib.h>
#include "math.h"
#include "TMath.h"

using namespace std;


vector<float> read_in( const char* file )
{
  std::ifstream input( (char*)file);
  std::vector<float> floats,floats_out;
  float value;

  cout << "read in from file " << file << endl;
  while(input >> value)
    {
      // store it in the vector.
      floats.push_back(value);
    }

  for(unsigned qq=0; qq<floats.size(); qq++)
    {
      qq=qq+1;
      floats_out.push_back(floats[qq]);
      qq=qq+1;
    }

  for(unsigned qq=0; qq<floats.size(); qq++)
    {
      cout << floats[qq] << " " << floats[qq+1] << " " << floats[qq+2] << endl;
      qq=qq+2;
    }
  floats.clear();
  return floats_out;
}


int main(int argc, char **argv)
{
  char *ename[1];
  char en0[]="error0";
  //  char en1[]="error1";
  //  char en2[]="error2";
  //  char en3[]="error3";
  //  char en4[]="error4";
  double nps_low[1];
  double nps_high[1];
  double lowsigma[1];
  double highsigma[1];
  int i;
  double chisq;

  if (argc<2)
    {
      cout << "Use lumi + include_qq as input: ./test_hypo [lumi in /fb] [include_qq=1,0] " << endl;
      return(0);
    }

  double lumi=(double)atof(argv[1]);
  //  cout << "lumi = " << lumi << endl;

  //Define number of histograms provided shape errors to be included
  //NOTE - in this example systematics are **NOT** included.
  TH1 *lowshape[5];
  TH1 *highshape[5];

  gROOT->Time();
  //-----------------------
  gSystem->Load("libPhysics.so");
  gSystem->CompileMacro("mclimit_csm.C","k");

  //Define the models for your two hypotheses..
  //e.g. H0 = null hypothesis of background only
  //     H1 =test hypothesis of Higgs signal + background at M_H = XGeV.
  cout << "calling mclimit_csm and model constructors" << endl;
  csm_model* nullhyp = new csm_model();
  csm_model* testhyp = new csm_model();

  bool real = true;

  int nbins;

  if(real)
    nbins = 20;
  else
    nbins = 50;
  //Define histogram of same binning as signal and background distributions for
  //Pseudodata..
  TH1F *h1 = new TH1F("h1","Pseudodata",nbins,0,nbins);

  //Background Distributions for discriminating variable.
  //In this example the discriminating variable is m_gammagamma,
  //but can be any robust distribution that well separates signal from background
  TH1 *anomalous = new TH1F("anomalous","Observable",nbins,0,nbins);
  TH1 *standard = new TH1F("standard","Observable",nbins,0,nbins);
  TH1 *standardup = new TH1F("standardup","Observable",nbins,0,nbins);
  TH1 *standarddw = new TH1F("standarddw","Observable",nbins,0,nbins);
  TH1 *measurement = new TH1F("bkgestmc","Observable",nbins,0,nbins);
  TH1 *standard2 = new TH1F("standard2","Observable",nbins,0,nbins);



  vector<float> bin_cont;

  if(!real) bin_cont=read_in("./histofiles/events_10_0.txt"); // this is the background
  else bin_cont=read_in(argv[2]); // this is the background
  for (unsigned kk=1;kk<=bin_cont.size();kk++)
    {
      standard->SetBinContent(  kk   ,  0.975*bin_cont[kk-1] ); // NLO K factor
      cout <<  bin_cont[kk-1] << endl;
    }
  bin_cont.clear();
  /////////////////////////////////////
  if(!real) bin_cont=read_in("./histofiles/events_10_0.txt"); // this is the background
  else bin_cont=read_in(argv[3]); // this is the background
  for (unsigned kk=1;kk<=bin_cont.size();kk++)
    {
      standard->AddBinContent(  kk   ,  bin_cont[kk-1] );
      cout <<  bin_cont[kk-1] << endl;
    }
  bin_cont.clear();
  /////////////////////////////////////
  if(!real)bin_cont=read_in("./histofiles/events_7_-2.txt"); // this is the signal
  else bin_cont=read_in(argv[4]); // this is the signal
  for (unsigned kk=1;kk<=bin_cont.size();kk++)
    {
      anomalous->SetBinContent(  kk   ,  bin_cont[kk-1] );
    }
  bin_cont.clear();
  /////////////////////////////////////
  if(!real)bin_cont=read_in("./histofiles/events_10_0.txt"); //this is the measurement histogram -> no impact
  else bin_cont=read_in(argv[2]); //this is the measurement histogram -> no impact
  for (unsigned kk=1;kk<=bin_cont.size();kk++)
    {
      measurement->SetBinContent(  kk   , bin_cont[kk-1] );
    }

  anomalous -> Scale(lumi);
  standard -> Scale(lumi);
  measurement -> Scale(lumi);

  TH1 *atlasdata = new TH1F("Channel 1","Mass_Higgs_hgg",nbins,0,nbins);
  TH1 *hbs = new TH1F("hbs","signal + Background",nbins,0,nbins);
  hbs->Add(anomalous,standard,0.,1.) ;

  atlasdata->Add(measurement,anomalous,1.,0.);


  // output quantils

  cout << "data histogram" << endl;
  cout << "----------------" << endl;
  Double_t alpha=0.158;
//  for (int kk=1;kk<=nbins;kk++)
//    {
//      if (atlasdata->GetBinContent(kk)>0)
//        {
//          myfile << kk << "       " << atlasdata->GetBinContent(kk) << "   " <<
//            TMath::ChisquareQuantile(1.-alpha, 2*(atlasdata->GetBinContent(kk)+1))/2.  << "   " <<
//            TMath::ChisquareQuantile(  alpha , 2*(atlasdata->GetBinContent(kk)  ))/2.  << "   " <<
//	    standard->GetBinContent(kk) << "   " << anomalous->GetBinContent(kk) << endl;
//	}
//    }




  //Definition of systematics affecting rate for BACKGROUND
  //Systematics are **NOT** included in this example.
  ename[0]=en0;
  //  ename[1]=en1;
  //  ename[2]=en2;
  //  ename[3]=en3;
  //  ename[4]=en4;

  //Shape systematics - define histograms which represent shape uncertainties
  //and state how many sigmas from the mean these distributions represent
  //Only interpolation is possible, no extrapolation so providing 2sigma histos
  //will allow shape uncertainties out to 2sigma but not beyond.
  //Systematics on shape are **NOT** included in this example

  /*
  for (i=0;i<1;i++)
    {
      nps_low[i] = 0;
      nps_high[i] = 0;
      lowshape[i] = 0;
      highshape[i] = 0;
    }

  */

  nps_low[0] = -1;
  nps_high[0] = 1;
  lowshape[0] = (TH1F*)standarddw->Clone("standard");
  lowshape[0]->Scale(1.);
  highshape[0] = (TH1F*)standardup->Clone("standard");
  highshape[0]->Scale(1.);
  lowsigma[0]=-1;
  highsigma[0]=1;

  //Now define channels for expected distributions - in this example
  //there is only one channel.

  //NOTE - Unscaled distributions are given as input to the expected distribution
  //models - then, the scale factor is provided separately so that the code can
  //do the scaling and if need be apply rate uncertainties to the scale factor.

  //NULL HYPOTHESIS MODEL DETAILS
  //  cout << "adding null hypothesis template #1" << endl;



  //                                v nuissance flag
  nullhyp->add_template(standard,1.,0,ename,nps_low,nps_high,
			lowshape,lowsigma,highshape,highsigma,0,0,(char*)"Channel 1");


  //plus signal (TEST HYPOTHESIS MODEL DETAILS contd.)
  //  cout << "adding test hypothesis template #2" << endl;
  //                                v  nuissance flag
  testhyp->add_template(standard,1.,0,ename,nps_low,nps_high,
			lowshape,lowsigma,highshape,highsigma,0,0,(char*)"Channel 1");

  //Definition of systematics affecting rate for SIGNAL
  //Systematics on rate are **NOT** included in this example.
  //ename[0]=en0;
  //  ename[1]=en1;
  //  ename[2]=en2;
  //  ename[3]=en3;
  //  ename[4]=en4;
  //Systematics on shape are **NOT** included in this example.



  for (i=0;i<1;i++)
    {
      nps_low[i] = 0;
      nps_high[i] = 0;
      lowshape[i] = 0;
      highshape[i] = 0;
    }

  /*
  nps_low[0] = -1;
  nps_high[0] = 1;
  lowshape[0] = (TH1F*)anomalous->Clone("anomalous");
  lowshape[0]->Scale(0.97);
  highshape[0] = (TH1F*)anomalous->Clone("anomalous");
  highshape[0]->Scale(1.03);
  lowsigma[0]=-1;
  highsigma[0]=1;
  */

  cout << "show integrated no of events" << endl;
  cout << anomalous->Integral() << "    " << standard->Integral() << endl;


  testhyp->add_template(anomalous,1.,0,ename,nps_low,nps_high,
			lowshape,lowsigma,highshape,highsigma,0,1,(char*)"Channel 1");


  //use the same hypotheses in the ensemble as in the fit
  //  cout << "Cloning null hypothesis" << endl;
  csm_model* nullhyp_pe = nullhyp->Clone();
//  cout << "Cloning test hypothesis" << endl;
  csm_model* testhyp_pe = testhyp->Clone();

  TCanvas *mycanvas = new TCanvas("Canvas1","");
  mycanvas->SetFillColor(kWhite);
  mycanvas->Divide(2,1);
  mycanvas->cd(1);
  testhyp->plotwithdata((char*)"Channel 1",h1);

  //get best fit to the 'data'
  csm* mycsm = new csm();
  mycsm->set_htofit(atlasdata,(char*)"Channel 1");
  mycsm->set_modeltofit(nullhyp);
  //  mycsm->set_modeltofit(testhyp);
  chisq = mycsm->chisquared();
  cout << "Chi-squared = " << chisq<< endl;
  csm_model* bestsignalfit = mycsm->getbestmodel();
  delete mycsm;

  mycanvas->cd(2);
  bestsignalfit->print();
  bestsignalfit->plotwithdata((char*)"Channel 1",atlasdata);
  mycanvas->SaveAs( "./channel_distribution.pdf" );


  cout << "calling mclimit_csm constructor" << endl;
  mclimit_csm* mymclimit = new mclimit_csm();


  mymclimit->set_datahist(atlasdata,(char*)"Channel 1");

  cout << "setting hypotheses" << endl;
  mymclimit->set_null_hypothesis(nullhyp);
  mymclimit->set_test_hypothesis(testhyp);
  mymclimit->set_null_hypothesis_pe(nullhyp_pe);
  mymclimit->set_test_hypothesis_pe(testhyp_pe);

  cout << "setting data histogram" << endl;
  // h1->Print("all");


  //number of pseudo-experiments.
  //if want to measure 1-CLb i.e. discovery p-value, need to
  //run at least 1E8 pseudoexperiments.
  //For CLs, only need to run 1E5
  mymclimit->set_npe(1e5);
  //somewhere in between to optomxise achieving good confirmation
  //of gaussian shape of tPDF.
  mymclimit->run_pseudoexperiments();

  //Numerous results can be calculated within mclimits from the PDFs:
  //Some examples are below but refer to CDF8128 (Tom Junk's note) for more detail.

  cout << "Getting results" << endl;
  //expected confidences for exclusion and discovery under H0 and H1.
  cout << "\n clsb_exp_bmed NULL: " << mymclimit->clsbexpbmed() << endl;
  ofstream myfile;
  string str(argv[4]);
  cout << str+".lim" << endl;
  myfile.open((str+".lim").c_str());

  cout << " cls TEST: " << mymclimit->cls() << endl;
  cout << " cls_exp_bmed NULL   : " << mymclimit->clsexpbmed() << endl;
  myfile << mymclimit->clsexpbmed() << endl;
  myfile.close();
//  cout << " cls_exp_smed TEST -2: " << mymclimit->clsexpbm2()<< endl;
//  cout << " cls_exp_smed TEST -1: " << mymclimit-> clsexpbm1()<< endl;
//  cout << " cls_exp_smed TEST +1: " << mymclimit-> clsexpbp1()<< endl;
//  cout << " cls_exp_smed TEST +2: " << mymclimit-> clsexpbp2()<< endl;
    //    cout << " cls_exp_smed TEST: " << mymclimit->clsexpsmed() << endl;


  cout << " clb_exp_bmed NULL: " << mymclimit->clbexpbmed() << endl;
//  cout << " clb_exp_smed TEST: " << mymclimit->clbexpsmed() << endl;
//  cout << " " << endl;
//  cout << " 1-cl_exp_bmed NULL: " << mymclimit->omclbexpbmed() << endl;
//  cout << " 1-clb_exp_smed NULL: " << mymclimit->omclbexpsmed() << " < 2.9*10^(-7) ?? "<< endl;


  /*

  cout << " cls_exp_smed TEST -2:" << mymclimit->clsexpbm2w()<< endl; // Expected cls in null hypothesis -- 2 sigma low edge
  cout << " cls_exp_smed TEST -1: " << mymclimit-> clsexpbm1w()<< endl; // Expected cls in null hypothesis -- 2 sigma low edge
  cout << " cls_exp_smed TEST   : " << mymclimit-> clsexpbmedw()<< endl; // Expected cls in null hypothesis -- median
  cout << " cls_exp_smed TEST +1: " << mymclimit-> clsexpbp1w()<< endl; // Expected cls in null hypothesis -- 1 sigma upper edge
  cout << " cls_exp_smed TEST +2: " << mymclimit-> clsexpbp2w()<< endl; // Expected cls in null hypothesis -- 2 sigma upper edge
  */
//    cout << " s95 strength  : " << mymclimit->s95med()<< endl; // Expected cls in null hypothesis -- median
//      cout << " cls_exp_smed TEST -2   : " << mymclimit->s95m2()<< endl; // Expected cls in null hypothesis -- 2 sigma low edge
//      cout << " cls_exp_smed TEST -1   : " << mymclimit->s95m1()<< endl; // Expected cls in null hypothesis -- 2 sigma low edge
//      cout << " cls_exp_smed TEST +1   : " << mymclimit->s95p1()<< endl; // Expected cls in null hypothesis -- 1 sigma upper edge
//      cout << " cls_exp_smed TEST +2   : " << mymclimit->s95p2()<< endl; // Expected cls in null hypothesis -- 2 sigma upper edge

   /*
   cout << " cls_exp_smed TEST      : " << mymclimit->s95() << endl; // Expected cls in null hypothesis -- median
   */

  //Measurement of Discovery Potential of Nsigma result assuming H1 true.
  /*cout << "p2sigmat: " << mymclimit->p2sigmat() << endl;
  cout << "p3sigmat: " << mymclimit->p3sigmat() << endl;
  cout << "p5sigmat: " << mymclimit->p5sigmat() << endl;*/

  //These are useful but take a while to run so are commented out in the example.

  //tPDF distributions for each hypothesis.


  /*
  TH1* myH0 = new TH1F("h0_hypo","",100,-20.,20.);
  TH1* myH1 = new TH1F("h1_hypo","",100,-20.,20.);

  mymclimit->tshists(myH1,myH0) ;

  TCanvas *mycanvas2 = (TCanvas *) new TCanvas("Canvas2","");
  mycanvas2->SetFillColor(kWhite);
  mycanvas2->SetLogy();
  gStyle->SetOptStat(0);


  myH0->Scale(1./myH0->Integral());
  myH1->Scale(1./myH1->Integral());


  myH0->Draw("");
  myH0->SetLineColor(kRed);
  myH0->SetLineWidth(3);
  myH1->Draw("same");
  myH1->SetLineColor(kBlue);
  myH1->SetLineWidth(3);

  myH0->GetXaxis()->SetTitle( "Log-Likelihood ratio" );
  myH0->GetYaxis()->SetTitle( "a.u." );


  //Legend for Plot
  TLegend *leg = new TLegend(0.7, 0.7, 0.98, 0.99);
  leg->SetFillColor(kWhite);
  //  leg->SetHeader("The Legend Title");
  leg->AddEntry(myH0,"Background hypothesis","L");
  leg->AddEntry(myH1,"Signal+Background hypothesis","L");


  cout << "\n\033[1m Fit null hypothesis \033[0m \n" << endl;
  myH0->Fit("gaus","0",0,21);
  cout << "\n\033[1m Fit test hypothesis \033[0m \n" << endl;
  myH1->Fit("gaus","0");
  cout << "" << endl;
  //DETERMINE CONFIDENCE LEVELS FROM FIT DETAILS
  TF1 *myh0func = myH0->GetFunction("gaus");
  TF1 *myh1func = myH1->GetFunction("gaus");


  Double_t h0max=myh0func->GetMaximumX();
  Double_t h1max=myh1func->GetMaximumX();

  myh0func->Draw("same");
  myh1func->Draw("same");

  double max1=myH0->GetMaximum();
  double max2=myH1->GetMaximum();
  max1=1.2*max(max1,max2);
  myH0->GetYaxis()->SetRangeUser(1e-3, max1);
  myH1->GetYaxis()->SetRangeUser(1e-3, max1);


  mycanvas2->SaveAs( "./llhr_distribution.pdf" );
  leg->Draw();

  cout << "Max H1 at " << h1max << " Max H0 at " << h0max << endl;

  */



  //tidying up!
  delete mymclimit;

  delete nullhyp;
  delete testhyp;
  delete nullhyp_pe;
  delete testhyp_pe;
  delete h1;


}
