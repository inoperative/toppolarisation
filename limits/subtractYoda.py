#! /usr/bin/env python

import sys
import os
import yoda

filenameWp=sys.argv[1]
filenameSM=sys.argv[2]

histosWp = yoda.read(filenameWp)
histosSM = yoda.read(filenameSM)
histosttj = yoda.read(sys.argv[3])

for i,key in enumerate(histosWp.keys()):
  histoWp = histosWp[key]
  histoSM = histosSM[key]
  histottj = histosttj[key]
  histoWp -= histoSM
  yoda.write([histoWp,histoSM,histottj],filename="./test/"+str(i)+".dat")
