#! /bin/bash

source $HOME/local/bin/thisroot.sh

cd cls_code_Wprime_noSM
make

python yoda_to_cls.py ../Wprime/ttb_SM/atom_results.yoda
python yoda_to_cls.py ../Wprime/ttj/atom_results.yoda

python yoda_to_cls.py ../Wprime/ttb_Wp_onlyRtb_noSM/atom_results.yoda
for d in ../Wprime/ttb_Wp_onlyRtb_noSM/*.dat; do
  file=${d##*/atom_results_}
  ./test_hypo $1 ../Wprime/ttj/atom_results_$file ../Wprime/ttb_SM/atom_results_$file ../Wprime/ttb_Wp_onlyRtb_noSM/atom_results_$file
done
exit
