#! /bin/bash

for dir in ./*_dir/; do
  echo "Looking at ${dir%_dir}..."
  for d in $dir/*.dat.lim; do
    echo "histo: ${d%.dat.lim}"
    cat $d
  done
  echo "	"
done

exit
