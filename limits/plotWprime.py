#! /usr/bin/env python

import sys
import os
import glob
from math import *
import numpy as np
from scipy import interpolate
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as mc


def grayify_cmap(cmap):
    """Return a grayscale version of the colormap"""
    cmap = plt.cm.get_cmap(cmap)
    colors = cmap(np.arange(cmap.N))

    # convert RGBA to perceived greyscale luminance
    # cf. http://alienryderflex.com/hsp.html
    RGB_weight = [0.299, 0.587, 0.114]
    luminance = np.sqrt(np.dot(colors[:, :3] ** 2, RGB_weight))
    colors[:, :3] = luminance[:, np.newaxis]

    return cmap.from_list(cmap.name + "_grayscale", colors, cmap.N)

def plotUsedPoints(coords_under, coords_over, name = None):
  font = {#'family' : 'cursive',
          #'weight' : 'bold',
          'size'   : 16}
  plt.rc('font', **font)
  cmap = plt.cm.YlGnBu
#  levels = [0, 2, 4, 6, 8, 10, 4*pi, 100]
#  plt.contourf(xgrid,ygrid,zs, levels=levels, colors=('#000099', '#3333FF', '#0099FF', '#66CC66', '#CCFF00', '#CC3333', '#C0C0C0', '#990000')) #reshape Z too!
  plt.xlabel("$M_A$ [GeV]")
  plt.ylabel("$M_H$ [GeV]", labelpad=0)
  plt.xlim([0,500])
  plt.ylim([125,500])
#  plt.figtext(0.47,0.77, "$\sqrt{s} = "+str(sqrts)+"$ TeV", size = 20)
#  if widthCheck:
#    plt.figtext(0.48,0.7, "$\Gamma_\\xi = M_\\xi /"+width+"$", size = 20)
#  elif couplingCheck:
#    plt.figtext(0.48,0.7, "$g_q / g_\chi = "+str(ratio)+"$", size = 20)
#  plt.figtext(0.47,0.6, "$\int\/ \mathrm{L \/ dt} = "+lumi+" \/ \mathrm{fb}^{-1}$", size = 20)
#  cbar = plt.colorbar()
#  ticklabels = ["0", "2", "4", "6", "8", "10", "4 $\pi$"]
#  cbar.set_ticklabels(ticklabels)
#  cbar.ax.set_ylabel("95% C.L. upper limit on $\sqrt{g_q g_\chi}$")
#  coords_filled = list()
#  coords_notfilled = list()
#  for i in dm:
#    for j in med:
#      foldername = folder+"/Med"+str(j)+"_DM"+str(i)
#      if os.path.isfile(foldername+"/Atom.signal"):
#        coords_filled.append([i,j])
#      else:
#        coords_notfilled.append([i,j])
  plt.plot(*zip(*coords_over), marker='.', color='black', ls='')
  plt.plot(*zip(*coords_under), marker='x', color='red', ls='')
  if name is None:
    plt.savefig("excluded_points"+".png", format="png", dpi=300 )
  else:
    plt.savefig(name+"_excluded_points"+".png", format="png", dpi=300 )

def plotUsedPointsColored(coords_x,coords_y,passed, name = None):
  font = {#'family' : 'cursive',
          #'weight' : 'bold',
          'size'   : 16}
  plt.rc('font', **font)
  ax1 = plt.figure()
  cmap = plt.cm.YlGnBu
  plt.xlabel("$M_A$ [GeV]")
  plt.ylabel("$M_H$ [GeV]", labelpad=0)
  sc = plt.scatter(coords_x,coords_y,c=passed,s=15,cmap=cmap,lw = 0.1,norm=matplotlib.colors.LogNorm(),vmin=1,vmax=1040)
  plt.xlim([0,500])
  plt.ylim([125,500])
#  m = plt.cm.ScalarMappable(cmap=cmap)
#  m.set_array(passed)
  cbar = plt.colorbar(ticks=[1, 10, 100, 1040])
  cbar.ax.set_yticklabels(['< 1', '10', '100', 'SM'])  # vertically oriented colorbar
  if name is None:
    plt.savefig("passed_events"+".png", format="png", dpi=300 )
  else:
    plt.savefig(name+".png", format="png", dpi=300 )

def main():

  if len(sys.argv) > 1:
    if sys.argv[1][-1] == '/':
      folder = sys.argv[1][:-1]
    else:
      folder = sys.argv[1]

  paths = [os.path.join(folder,o) for o in os.listdir(folder) if not os.path.isdir(os.path.join(folder,o))]
  params = []

  for tmp in paths:
    tmp = tmp.replace(folder+"/", '')
    params.append(tmp)

  coords_under = list()
  coords_over = list()
  coords_x = []
  coords_y = []
  passed = []

  for param in paths:
    n_passed = 0
    best_lim = 1.
    if os.path.isdir(param+"_dir/"):
      with open(param+"_dir/atom_results.data") as f:
        for line in f.readlines():
          if "Total passed weight of all events normalised to luminosity:" in line:
            words = line.split(' ')
            n_passed = float(words[-1])
    for limit_file in glob.glob(param+"_dir/*.lim"):
      with open(limit_file) as f:
        for line in f.readlines():
          words = line.split(' ')
          lim = float(words[0])
          if lim < best_lim:
            best_lim = lim
#    print param
#    print n_passed
#    print best_lim
    with open(param) as f:
      for line in f.readlines():
        if "# Mh1, lightest CP-even Higgs" in line:
          words = line.split(' ')
          m_h = float(words[12])
        if "# Mh2, heaviest CP-even Higgs" in line:
          words = line.split(' ')
          m_H = float(words[12])
        if "# Mh3, CP-odd Higgs" in line:
          words = line.split(' ')
          m_A = float(words[12])
        if "# Mhc" in line:
          words = line.split(' ')
          m_Hc = float(words[12])
#    print m_h, m_H, m_A, m_Hc
    if(best_lim < .1):
      coords_under.append([m_A,m_H])
    else:
      coords_over.append([m_A,m_H])
    passed.append(n_passed)
    coords_x.append(m_A)
    coords_y.append(m_H)
  plotUsedPoints(coords_under,coords_over,sys.argv[2])
  plotUsedPointsColored(coords_x,coords_y,passed,sys.argv[2])

if __name__ == "__main__":
  main()
