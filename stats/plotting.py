import matplotlib.pyplot as plt
import matplotlib
import matplotlib.lines as mlines
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import matplotlib.patches as mpatches

import numpy as np
from scipy import interpolate
from scipy import optimize
import pickle

matplotlib.rcParams.update({'font.size': 26})
matplotlib.rcParams['xtick.major.size'] = 6
matplotlib.rcParams['xtick.major.width'] = 1
matplotlib.rcParams['xtick.minor.size'] = 4
matplotlib.rcParams['xtick.minor.width'] = 1
matplotlib.rcParams['ytick.major.size'] = 6
matplotlib.rcParams['ytick.major.width'] = 1
matplotlib.rcParams['ytick.minor.size'] = 4
matplotlib.rcParams['ytick.minor.width'] = 1
matplotlib.rcParams['xtick.major.pad']='8'
matplotlib.rcParams['ytick.major.pad']='10'

for dir in ["./30tev/graviton_dilep/", "./35tev/graviton_dilep/", "./40tev/graviton_dilep/"]:
    lumis = [1,10,100,1000] # in fb
    mus = [1,10,50,100,500,1000,2000,10000]
    for model in ["c1.yoda", "c2.yoda"]:
        with open (dir+model+".lim", 'rb') as fp:
            result = pickle.load(fp)
        # result = [[[1, 1, 1.0, 0.9894265364564212, 1.0, 0.9966517152062669, 0.9959816644838672, 0.994828447107497, 0.9996484375, 1.0, 0.9981756061963047, 0.9981789486463518], [1, 10, 0.9921851174138456, 0.9913180741910024, 0.9911495624502784, 0.9926619507470135, 0.9933107872880091, 0.9952759534508584, 0.99328783980311, 0.989409984871407, 0.992658332350513, 0.9919774067779666], [1, 100, 0.9663266511756134, 0.9500096880449526, 0.7815533980582524, 0.9904237705284825, 0.9986095717884131, 0.968231556653724, 0.9538225713738264, 0.952, 0.9961007529580495, 0.9985497905252981], [1, 1000, 0.9088934523339389, 0.8266666666666667, 0.1, 0.9987769913988411, 1.0, 0.9589770271351957, 0.696969696969697, 0.1, 0.9999599342922393, 1.0]], [[10, 1, 0.9689962315861597, 0.9657318564072167, 0.9493115632996223, 0.9628328109665368, 0.9724309276498937, 0.9817783208740656, 0.983617427936186, 0.9851069246435845, 0.9810040705563093, 0.9843249773482332], [10, 10, 0.872822697193276, 0.825536660161695, 0.8147706968433591, 0.9083345497007737, 0.9335555555555556, 0.9423002850319698, 0.9092879773219943, 0.8885286545246496, 0.961010059048693, 0.9683411564965545], [10, 100, 0.6300808200275971, 0.38902439024390245, 0.2268041237113402, 0.8687783817640874, 0.9795482480001612, 0.8057343706804628, 0.6266354227689904, 0.3625, 0.9384784215359119, 0.9908118237320921], [10, 1000, 0.2680300968542384, 0.0, 0.0, 0.956479884481168, 1.0, 0.5482977356672555, 0.08064516129032258, 0.0, 0.9915077711905144, 1.0]], [[50, 1, 0.8240126490908466, 0.8224789519386869, 0.8024535221955229, 0.8400025467163277, 0.8524134218553742, 0.9001011268419532, 0.904168305151396, 0.892621168847777, 0.9095648752336295, 0.9265797573782365], [50, 10, 0.46582229898292077, 0.36298649722001586, 0.2872373784885544, 0.5653189997663005, 0.6579108874432307, 0.6951059730250482, 0.6234849766052202, 0.5590860056879474, 0.7629033209461452, 0.8297202797202797], [50, 100, 0.022572469507365755, 0.0008851515822084533, 0.0, 0.16217887831061828, 0.5630985915492958, 0.21241460081983213, 0.05404315386166566, 0.004366812227074236, 0.5538416893611322, 0.8829714377794787], [50, 1000, 0.0, 0.0, 0.0, 0.006755944027905858, 0.62492, 0.002750538148768237, 0.0, 0.0, 0.47978766025641023, 0.99774]], [[100, 1, 0.6848595249772297, 0.6679807103490508, 0.645726902762144, 0.6976662404092071, 0.7291053295679866, 0.8184169501133787, 0.8064897685605015, 0.796, 0.8310980584108337, 0.8408884034225378], [100, 10, 0.1660053164849559, 0.10665408211420481, 0.06542775456344843, 0.24876152073732719, 0.35106719559979555, 0.44688137288852975, 0.3636728837876614, 0.2703508597554915, 0.5424741840991804, 0.643412392897057], [100, 100, 3.970932772108168e-05, 0.0, 0.0, 0.001558452961060626, 0.032408430612203894, 0.01601083320057352, 0.0010602205258693808, 0.0, 0.13388488568632684, 0.5056263461964289], [100, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0015021932020750296, 0.4912]], [[500, 1, 0.11792689033550326, 0.1040832018129578, 0.08739282012626025, 0.13344316309719934, 0.1555226970279462, 0.3471720818291215, 0.325299306868305, 0.30202261084614024, 0.3772878401497789, 0.40369960852284614], [500, 10, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0012734429266033805, 0.0008295804693055226, 0.00022333891680625348, 0.00466315240688177, 0.011204005249598989], [500, 100, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [500, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]], [[1000, 1, 0.00947147711662208, 0.007310310057978321, 0.0059626472182526, 0.013262941024980685, 0.015009159132541512, 0.10863559717289133, 0.09160490827359981, 0.07464212678936605, 0.11893322874791647, 0.1326378093985412], [1000, 10, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [1000, 100, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [1000, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]], [[2000, 1, 0.0, 0.00012819964958762445, 5.1224259809445757e-05, 6.449948400412796e-05, 2.919281856663261e-05, 0.006314498087609151, 0.006229546977881166, 0.005082005082005082, 0.00850453575240128, 0.009976720984369804], [2000, 10, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [2000, 100, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [2000, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]]]
        print result

        x = np.array(np.log10(mus))
        # x = np.array([np.log10(result[mu][0][1]),np.log10(result[mu][1][1]),np.log10(result[mu][2][1]),np.log10(result[mu][3][1])])

        lim_angle = []
        lim_angle_m1 = []
        lim_angle_m2 = []
        lim_angle_p1 = []
        lim_angle_p2 = []

        lim_mtt = []
        lim_mtt_m1 = []
        lim_mtt_m2 = []
        lim_mtt_p1 = []
        lim_mtt_p2 = []

        for i in range(4):
            y_angle = np.array([result[0][i][2],result[1][i][2],result[2][i][2],result[3][i][2],result[4][i][2],result[5][i][2],result[6][i][2],result[7][i][2]])
            y_angle_m1 = np.array([result[0][i][3],result[1][i][3],result[2][i][3],result[3][i][3],result[4][i][3],result[5][i][3],result[6][i][3],result[7][i][3]])
            y_angle_m2 = np.array([result[0][i][4],result[1][i][4],result[2][i][4],result[3][i][4],result[4][i][4],result[5][i][4],result[6][i][4],result[7][i][4]])
            y_angle_p1 = np.array([result[0][i][5],result[1][i][5],result[2][i][5],result[3][i][5],result[4][i][5],result[5][i][5],result[6][i][5],result[7][i][5]])
            y_angle_p2 = np.array([result[0][i][6],result[1][i][6],result[2][i][6],result[3][i][6],result[4][i][6],result[5][i][6],result[6][i][6],result[7][i][6]])

            y_mtt = np.array([result[0][i][7],result[1][i][7],result[2][i][7],result[3][i][7],result[4][i][7],result[5][i][7],result[6][i][7],result[7][i][7]])
            y_mtt_m1 = np.array([result[0][i][8], result[1][i][8], result[2][i][8], result[3][i][8], result[4][i][8], result[5][i][8], result[6][i][8], result[7][i][8]])
            y_mtt_m2 = np.array([result[0][i][9], result[1][i][9], result[2][i][9], result[3][i][9], result[4][i][9], result[5][i][9], result[6][i][9], result[7][i][9]])
            y_mtt_p1 = np.array([result[0][i][10],result[1][i][10],result[2][i][10],result[3][i][10],result[4][i][10],result[5][i][10],result[6][i][10],result[7][i][10]])
            y_mtt_p2 = np.array([result[0][i][11],result[1][i][11],result[2][i][11],result[3][i][11],result[4][i][11],result[5][i][11],result[6][i][11],result[7][i][11]])

            new_y_angle = interpolate.UnivariateSpline(x, y_angle-0.05)
            new_y_angle_m1 = interpolate.UnivariateSpline(x, y_angle_m1-0.05)
            new_y_angle_m2 = interpolate.UnivariateSpline(x, y_angle_m2-0.05)
            new_y_angle_p1 = interpolate.UnivariateSpline(x, y_angle_p1-0.05)
            new_y_angle_p2 = interpolate.UnivariateSpline(x, y_angle_p2-0.05)

            new_y_mtt = interpolate.UnivariateSpline(x, y_mtt-0.05)
            new_y_mtt_m1 = interpolate.UnivariateSpline(x, y_mtt_m1-0.05)
            new_y_mtt_m2 = interpolate.UnivariateSpline(x, y_mtt_m2-0.05)
            new_y_mtt_p1 = interpolate.UnivariateSpline(x, y_mtt_p1-0.05)
            new_y_mtt_p2 = interpolate.UnivariateSpline(x, y_mtt_p2-0.05)

            # mu_angle = optimize.brentq(new_y_angle, 0.0, 3.3, args=())
            # mu_angle_m1 = optimize.brentq(new_y_angle_m1, 0.0, 3.3, args=())
            # mu_angle_m2 = optimize.brentq(new_y_angle_m2, 0.0, 3.3, args=())
            # mu_angle_p1 = optimize.brentq(new_y_angle_p1, 0.0, 3.3, args=())
            # mu_angle_p2 = optimize.brentq(new_y_angle_p2, 0.0, 3.3, args=())
            #
            # mu_mtt = optimize.brentq(new_y_mtt, 0.0, 3.3, args=())
            # mu_mtt_m1 = optimize.brentq(new_y_mtt_m1, 0.0, 3.3, args=())
            # mu_mtt_m2 = optimize.brentq(new_y_mtt_m2, 0.0, 3.3, args=())
            # mu_mtt_p1 = optimize.brentq(new_y_mtt_p1, 0.0, 3.3, args=())
            # mu_mtt_p2 = optimize.brentq(new_y_mtt_p2, 0.0, 3.3, args=())

            # print lumis[i]
            if new_y_angle.roots().size == 0:
                mu_angle = 4.
            else:
                mu_angle = min(new_y_angle.roots()  )
            if new_y_angle_m1.roots().size == 0:
                mu_angle_m1 = 4.
            else:
                mu_angle_m1 = min(new_y_angle_m1.roots() )
            if new_y_angle_m2.roots().size == 0:
                mu_angle_m2 = 4.
            else:
                mu_angle_m2 = min(new_y_angle_m2.roots() )
            if new_y_angle_p1.roots().size == 0:
                mu_angle_p1 = 4.
            else:
                mu_angle_p1 = min(new_y_angle_p1.roots() )
            if new_y_angle_p2.roots().size == 0:
                mu_angle_p2 = 4.
            else:
                mu_angle_p2 = min(new_y_angle_p2.roots() )


            if new_y_mtt.roots().size == 0:
                mu_mtt = 4.
            else:
                mu_mtt = min(new_y_mtt.roots()  )
            if new_y_mtt_m1.roots().size == 0:
                mu_mtt_m1 = 4.
            else:
                mu_mtt_m1 = min(new_y_mtt_m1.roots() )
            if new_y_mtt_m2.roots().size == 0:
                mu_mtt_m2 = 4.
            else:
                mu_mtt_m2 = min(new_y_mtt_m2.roots() )
            if new_y_mtt_p1.roots().size == 0:
                mu_mtt_p1 = 4.
            else:
                mu_mtt_p1 = min(new_y_mtt_p1.roots() )
            if new_y_mtt_p2.roots().size == 0:
                mu_mtt_p2 = 4.
            else:
                mu_mtt_p2 = min(new_y_mtt_p2.roots() )

            # mu_mtt = min(new_y_mtt.roots() )
            # mu_mtt_m1 = min(new_y_mtt_m1.roots() )
            # mu_mtt_m2 = min(new_y_mtt_m2.roots() )
            # mu_mtt_p1 = min(new_y_mtt_p1.roots() )
            # mu_mtt_p2 = min(new_y_mtt_p2.roots() )

            lim_angle.append(pow(10.,mu_angle))
            lim_angle_m1.append(pow(10.,mu_angle_m1))
            lim_angle_m2.append(pow(10.,mu_angle_m2))
            lim_angle_p1.append(pow(10.,mu_angle_p1))
            lim_angle_p2.append(pow(10.,mu_angle_p2))

            lim_mtt.append(pow(10.,mu_mtt))
            lim_mtt_m1.append(pow(10.,mu_mtt_m1))
            lim_mtt_m2.append(pow(10.,mu_mtt_m2))
            lim_mtt_p1.append(pow(10.,mu_mtt_p1))
            lim_mtt_p2.append(pow(10.,mu_mtt_p2))

        plt.yscale('log')
        plt.xscale('log')
        plt.ylim([10,10000])
        # plt.plot(np.linspace(0,3), new_y(np.linspace(0,3)), color='black',linestyle='dotted', linewidth=2)
        polygon1 = Polygon(np.array([[lumis[0],lim_angle_p1[0]], [lumis[0],lim_angle_m1[0]], [lumis[1],lim_angle_m1[1]],[lumis[2],lim_angle_m1[2]],[lumis[3],lim_angle_m1[3]],[lumis[3],lim_angle_p1[3]],[lumis[2],lim_angle_p1[2]],[lumis[1],lim_angle_p1[1]]]), True,color='lime')
        polygon2 = Polygon(np.array([[lumis[0],lim_angle_p2[0]], [lumis[0],lim_angle_m2[0]], [lumis[1],lim_angle_m2[1]],[lumis[2],lim_angle_m2[2]],[lumis[3],lim_angle_m2[3]],[lumis[3],lim_angle_p2[3]],[lumis[2],lim_angle_p2[2]],[lumis[1],lim_angle_p2[1]]]), True,color='yellow')
        p2=PatchCollection([polygon2],color='yellow')
        p1=PatchCollection([polygon1],color='lime')
        fig, ax = plt.subplots()
        ax.set_yscale('log')
        ax.set_xscale('log')
        ax.set_ylim([10,10000])

        ax.add_collection(p2)
        ax.add_collection(p1)
        sigma1 = mpatches.Patch(color='lime')
        sigma2 = mpatches.Patch(color='yellow')

        plt.plot(lumis, lim_angle, color='black',linestyle='dotted', linewidth=2)
        plt.plot(lumis, lim_mtt, color='red',linestyle='dotted', linewidth=2)
        angle=mlines.Line2D([], [], color='black',linestyle='dotted', linewidth=2)
        mtt=mlines.Line2D([], [], color='red',linestyle='dotted', linewidth=2)
        tmp=mlines.Line2D([], [], color='red',linestyle=' ', linewidth=2)
        plt.legend([angle,sigma1,sigma2,mtt,tmp],(r"$\theta_l+m_{tt}$", r"$\pm 1 \sigma$", r"$\pm 2 \sigma$", "$m_{tt}$", " "),loc=3, numpoints=1, fontsize=22,frameon=False,labelspacing=0.2,handlelength=2.6,handletextpad=0.5)
        plt.text(1.4,15.,r"Di-leptonic channel, $c=1$, $m(G^1) = 3$ TeV", fontsize = 18)
        plt.xlabel(r"$\mathcal{L} \hspace{1} [\mathrm{fb}^{-1}]$", fontsize = 24)
        plt.ylabel(r"95% C.L. limit on $\mu$", fontsize = 22)
        #
        plt.tight_layout()
        plt.savefig(dir+model+".limits.pdf", dpi=100)
        plt.close()

for dir in ["./30tev/graviton_semil/", "./35tev/graviton_semil/", "./40tev/graviton_semil/"]:
    lumis = [1,10,100,1000] # in fb
    mus = [1,10,50,100,500,1000,2000,10000]
    for model in ["c1.yoda", "c2.yoda"]:
        with open (dir+model+".lim", 'rb') as fp:
            result = pickle.load(fp)
        # result = [[[1, 1, 1.0, 0.9894265364564212, 1.0, 0.9966517152062669, 0.9959816644838672, 0.994828447107497, 0.9996484375, 1.0, 0.9981756061963047, 0.9981789486463518], [1, 10, 0.9921851174138456, 0.9913180741910024, 0.9911495624502784, 0.9926619507470135, 0.9933107872880091, 0.9952759534508584, 0.99328783980311, 0.989409984871407, 0.992658332350513, 0.9919774067779666], [1, 100, 0.9663266511756134, 0.9500096880449526, 0.7815533980582524, 0.9904237705284825, 0.9986095717884131, 0.968231556653724, 0.9538225713738264, 0.952, 0.9961007529580495, 0.9985497905252981], [1, 1000, 0.9088934523339389, 0.8266666666666667, 0.1, 0.9987769913988411, 1.0, 0.9589770271351957, 0.696969696969697, 0.1, 0.9999599342922393, 1.0]], [[10, 1, 0.9689962315861597, 0.9657318564072167, 0.9493115632996223, 0.9628328109665368, 0.9724309276498937, 0.9817783208740656, 0.983617427936186, 0.9851069246435845, 0.9810040705563093, 0.9843249773482332], [10, 10, 0.872822697193276, 0.825536660161695, 0.8147706968433591, 0.9083345497007737, 0.9335555555555556, 0.9423002850319698, 0.9092879773219943, 0.8885286545246496, 0.961010059048693, 0.9683411564965545], [10, 100, 0.6300808200275971, 0.38902439024390245, 0.2268041237113402, 0.8687783817640874, 0.9795482480001612, 0.8057343706804628, 0.6266354227689904, 0.3625, 0.9384784215359119, 0.9908118237320921], [10, 1000, 0.2680300968542384, 0.0, 0.0, 0.956479884481168, 1.0, 0.5482977356672555, 0.08064516129032258, 0.0, 0.9915077711905144, 1.0]], [[50, 1, 0.8240126490908466, 0.8224789519386869, 0.8024535221955229, 0.8400025467163277, 0.8524134218553742, 0.9001011268419532, 0.904168305151396, 0.892621168847777, 0.9095648752336295, 0.9265797573782365], [50, 10, 0.46582229898292077, 0.36298649722001586, 0.2872373784885544, 0.5653189997663005, 0.6579108874432307, 0.6951059730250482, 0.6234849766052202, 0.5590860056879474, 0.7629033209461452, 0.8297202797202797], [50, 100, 0.022572469507365755, 0.0008851515822084533, 0.0, 0.16217887831061828, 0.5630985915492958, 0.21241460081983213, 0.05404315386166566, 0.004366812227074236, 0.5538416893611322, 0.8829714377794787], [50, 1000, 0.0, 0.0, 0.0, 0.006755944027905858, 0.62492, 0.002750538148768237, 0.0, 0.0, 0.47978766025641023, 0.99774]], [[100, 1, 0.6848595249772297, 0.6679807103490508, 0.645726902762144, 0.6976662404092071, 0.7291053295679866, 0.8184169501133787, 0.8064897685605015, 0.796, 0.8310980584108337, 0.8408884034225378], [100, 10, 0.1660053164849559, 0.10665408211420481, 0.06542775456344843, 0.24876152073732719, 0.35106719559979555, 0.44688137288852975, 0.3636728837876614, 0.2703508597554915, 0.5424741840991804, 0.643412392897057], [100, 100, 3.970932772108168e-05, 0.0, 0.0, 0.001558452961060626, 0.032408430612203894, 0.01601083320057352, 0.0010602205258693808, 0.0, 0.13388488568632684, 0.5056263461964289], [100, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0015021932020750296, 0.4912]], [[500, 1, 0.11792689033550326, 0.1040832018129578, 0.08739282012626025, 0.13344316309719934, 0.1555226970279462, 0.3471720818291215, 0.325299306868305, 0.30202261084614024, 0.3772878401497789, 0.40369960852284614], [500, 10, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0012734429266033805, 0.0008295804693055226, 0.00022333891680625348, 0.00466315240688177, 0.011204005249598989], [500, 100, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [500, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]], [[1000, 1, 0.00947147711662208, 0.007310310057978321, 0.0059626472182526, 0.013262941024980685, 0.015009159132541512, 0.10863559717289133, 0.09160490827359981, 0.07464212678936605, 0.11893322874791647, 0.1326378093985412], [1000, 10, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [1000, 100, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [1000, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]], [[2000, 1, 0.0, 0.00012819964958762445, 5.1224259809445757e-05, 6.449948400412796e-05, 2.919281856663261e-05, 0.006314498087609151, 0.006229546977881166, 0.005082005082005082, 0.00850453575240128, 0.009976720984369804], [2000, 10, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [2000, 100, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [2000, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]]]
        # print result

        x = np.array(np.log10(mus))
        # x = np.array([np.log10(result[mu][0][1]),np.log10(result[mu][1][1]),np.log10(result[mu][2][1]),np.log10(result[mu][3][1])])

        lim_angle = []
        lim_angle_m1 = []
        lim_angle_m2 = []
        lim_angle_p1 = []
        lim_angle_p2 = []

        lim_mtt = []
        lim_mtt_m1 = []
        lim_mtt_m2 = []
        lim_mtt_p1 = []
        lim_mtt_p2 = []

        for i in range(4):
            y_angle = np.array([result[0][i][2],result[1][i][2],result[2][i][2],result[3][i][2],result[4][i][2],result[5][i][2],result[6][i][2],result[7][i][2]])
            y_angle_m1 = np.array([result[0][i][3],result[1][i][3],result[2][i][3],result[3][i][3],result[4][i][3],result[5][i][3],result[6][i][3],result[7][i][3]])
            y_angle_m2 = np.array([result[0][i][4],result[1][i][4],result[2][i][4],result[3][i][4],result[4][i][4],result[5][i][4],result[6][i][4],result[7][i][4]])
            y_angle_p1 = np.array([result[0][i][5],result[1][i][5],result[2][i][5],result[3][i][5],result[4][i][5],result[5][i][5],result[6][i][5],result[7][i][5]])
            y_angle_p2 = np.array([result[0][i][6],result[1][i][6],result[2][i][6],result[3][i][6],result[4][i][6],result[5][i][6],result[6][i][6],result[7][i][6]])

            y_mtt = np.array([result[0][i][7],result[1][i][7],result[2][i][7],result[3][i][7],result[4][i][7],result[5][i][7],result[6][i][7],result[7][i][7]])
            y_mtt_m1 = np.array([result[0][i][8], result[1][i][8], result[2][i][8], result[3][i][8], result[4][i][8], result[5][i][8], result[6][i][8], result[7][i][8]])
            y_mtt_m2 = np.array([result[0][i][9], result[1][i][9], result[2][i][9], result[3][i][9], result[4][i][9], result[5][i][9], result[6][i][9], result[7][i][9]])
            y_mtt_p1 = np.array([result[0][i][10],result[1][i][10],result[2][i][10],result[3][i][10],result[4][i][10],result[5][i][10],result[6][i][10],result[7][i][10]])
            y_mtt_p2 = np.array([result[0][i][11],result[1][i][11],result[2][i][11],result[3][i][11],result[4][i][11],result[5][i][11],result[6][i][11],result[7][i][11]])

            new_y_angle = interpolate.UnivariateSpline(x, y_angle-0.05)
            new_y_angle_m1 = interpolate.UnivariateSpline(x, y_angle_m1-0.05)
            new_y_angle_m2 = interpolate.UnivariateSpline(x, y_angle_m2-0.05)
            new_y_angle_p1 = interpolate.UnivariateSpline(x, y_angle_p1-0.05)
            new_y_angle_p2 = interpolate.UnivariateSpline(x, y_angle_p2-0.05)

            new_y_mtt = interpolate.UnivariateSpline(x, y_mtt-0.05)
            new_y_mtt_m1 = interpolate.UnivariateSpline(x, y_mtt_m1-0.05)
            new_y_mtt_m2 = interpolate.UnivariateSpline(x, y_mtt_m2-0.05)
            new_y_mtt_p1 = interpolate.UnivariateSpline(x, y_mtt_p1-0.05)
            new_y_mtt_p2 = interpolate.UnivariateSpline(x, y_mtt_p2-0.05)

            # mu_angle = optimize.brentq(new_y_angle, 0.0, 3.3, args=())
            # mu_angle_m1 = optimize.brentq(new_y_angle_m1, 0.0, 3.3, args=())
            # mu_angle_m2 = optimize.brentq(new_y_angle_m2, 0.0, 3.3, args=())
            # mu_angle_p1 = optimize.brentq(new_y_angle_p1, 0.0, 3.3, args=())
            # mu_angle_p2 = optimize.brentq(new_y_angle_p2, 0.0, 3.3, args=())
            #
            # mu_mtt = optimize.brentq(new_y_mtt, 0.0, 3.3, args=())
            # mu_mtt_m1 = optimize.brentq(new_y_mtt_m1, 0.0, 3.3, args=())
            # mu_mtt_m2 = optimize.brentq(new_y_mtt_m2, 0.0, 3.3, args=())
            # mu_mtt_p1 = optimize.brentq(new_y_mtt_p1, 0.0, 3.3, args=())
            # mu_mtt_p2 = optimize.brentq(new_y_mtt_p2, 0.0, 3.3, args=())

            # print lumis[i]
            if new_y_angle.roots().size == 0:
                mu_angle = 4.
            else:
                mu_angle = min(new_y_angle.roots()  )
            if new_y_angle_m1.roots().size == 0:
                mu_angle_m1 = 4.
            else:
                mu_angle_m1 = min(new_y_angle_m1.roots() )
            if new_y_angle_m2.roots().size == 0:
                mu_angle_m2 = 4.
                # mu_angle_m2 = 0.7
            else:
                mu_angle_m2 = min(new_y_angle_m2.roots() )
            if new_y_angle_p1.roots().size == 0:
                mu_angle_p1 = 4.
            else:
                mu_angle_p1 = min(new_y_angle_p1.roots() )
            if new_y_angle_p2.roots().size == 0:
                mu_angle_p2 = 4.
            else:
                mu_angle_p2 = min(new_y_angle_p2.roots() )


            if new_y_mtt.roots().size == 0:
                mu_mtt = 4.
            else:
                mu_mtt = min(new_y_mtt.roots()  )
            if new_y_mtt_m1.roots().size == 0:
                mu_mtt_m1 = 4.
            else:
                mu_mtt_m1 = min(new_y_mtt_m1.roots() )
            if new_y_mtt_m2.roots().size == 0:
                mu_mtt_m2 = 4.
            else:
                mu_mtt_m2 = min(new_y_mtt_m2.roots() )
            if new_y_mtt_p1.roots().size == 0:
                mu_mtt_p1 = 4.
            else:
                mu_mtt_p1 = min(new_y_mtt_p1.roots() )
            if new_y_mtt_p2.roots().size == 0:
                mu_mtt_p2 = 4.
            else:
                mu_mtt_p2 = min(new_y_mtt_p2.roots() )

            lim_angle.append(pow(10.,mu_angle))
            lim_angle_m1.append(pow(10.,mu_angle_m1))
            lim_angle_m2.append(pow(10.,mu_angle_m2))
            lim_angle_p1.append(pow(10.,mu_angle_p1))
            lim_angle_p2.append(pow(10.,mu_angle_p2))

            lim_mtt.append(pow(10.,mu_mtt))
            lim_mtt_m1.append(pow(10.,mu_mtt_m1))
            lim_mtt_m2.append(pow(10.,mu_mtt_m2))
            lim_mtt_p1.append(pow(10.,mu_mtt_p1))
            lim_mtt_p2.append(pow(10.,mu_mtt_p2))

        plt.yscale('log')
        plt.xscale('log')
        plt.ylim([10,1000])
        # plt.plot(np.linspace(0,3), new_y(np.linspace(0,3)), color='black',linestyle='dotted', linewidth=2)
        polygon1 = Polygon(np.array([[lumis[0],lim_angle_p1[0]], [lumis[0],lim_angle_m1[0]], [lumis[1],lim_angle_m1[1]],[lumis[2],lim_angle_m1[2]],[lumis[3],lim_angle_m1[3]],[lumis[3],lim_angle_p1[3]],[lumis[2],lim_angle_p1[2]],[lumis[1],lim_angle_p1[1]]]), True,color='lime')
        polygon2 = Polygon(np.array([[lumis[0],lim_angle_p2[0]], [lumis[0],lim_angle_m2[0]], [lumis[1],lim_angle_m2[1]],[lumis[2],lim_angle_m2[2]],[lumis[3],lim_angle_m2[3]],[lumis[3],lim_angle_p2[3]],[lumis[2],lim_angle_p2[2]],[lumis[1],lim_angle_p2[1]]]), True,color='yellow')
        p2=PatchCollection([polygon2],color='yellow')
        p1=PatchCollection([polygon1],color='lime')
        fig, ax = plt.subplots()
        ax.set_yscale('log')
        ax.set_xscale('log')
        ax.set_ylim([10,1000])

        ax.add_collection(p2)
        ax.add_collection(p1)
        sigma1 = mpatches.Patch(color='lime')
        sigma2 = mpatches.Patch(color='yellow')

        plt.plot(lumis, lim_angle, color='black',linestyle='dotted', linewidth=2)
        plt.plot(lumis, lim_mtt, color='red',linestyle='dotted', linewidth=2)
        angle=mlines.Line2D([], [], color='black',linestyle='dotted', linewidth=2)
        mtt=mlines.Line2D([], [], color='red',linestyle='dotted', linewidth=2)
        tmp=mlines.Line2D([], [], color='red',linestyle=' ', linewidth=2)
        plt.legend([angle,sigma1,sigma2,mtt,tmp],(r"$\theta_l+m_{tt}$", r"$\pm 1 \sigma$", r"$\pm 2 \sigma$", "$m_{tt}$", " "),loc=3, numpoints=1, fontsize=22,frameon=False,labelspacing=0.2,handlelength=2.6,handletextpad=0.5)
        plt.text(1.4,15.,r"Semi-leptonic channel, $c=2$, $m(G^1) = 3$ TeV", fontsize = 18)
        plt.xlabel(r"$\mathcal{L} \hspace{1} [\mathrm{fb}^{-1}]$", fontsize = 24)
        plt.ylabel(r"95% C.L. limit on $\mu$", fontsize = 22)
        #
        plt.tight_layout()
        plt.savefig(dir+model+".limits.pdf", dpi=100)
        plt.close()

for dir in ["./30tev/gluon_dilep/", "./35tev/gluon_dilep/", "./40tev/gluon_dilep/"]:
    lumis = [1,10,100,1000] # in fb
    mus = [0.01,0.1,0.5,1,2,5,10,100]
    for model in ["g2.yoda", "g6.yoda"]:
        with open (dir+model+".lim", 'rb') as fp:
            result = pickle.load(fp)
        # result = [[[1, 1, 1.0, 0.9894265364564212, 1.0, 0.9966517152062669, 0.9959816644838672, 0.994828447107497, 0.9996484375, 1.0, 0.9981756061963047, 0.9981789486463518], [1, 10, 0.9921851174138456, 0.9913180741910024, 0.9911495624502784, 0.9926619507470135, 0.9933107872880091, 0.9952759534508584, 0.99328783980311, 0.989409984871407, 0.992658332350513, 0.9919774067779666], [1, 100, 0.9663266511756134, 0.9500096880449526, 0.7815533980582524, 0.9904237705284825, 0.9986095717884131, 0.968231556653724, 0.9538225713738264, 0.952, 0.9961007529580495, 0.9985497905252981], [1, 1000, 0.9088934523339389, 0.8266666666666667, 0.1, 0.9987769913988411, 1.0, 0.9589770271351957, 0.696969696969697, 0.1, 0.9999599342922393, 1.0]], [[10, 1, 0.9689962315861597, 0.9657318564072167, 0.9493115632996223, 0.9628328109665368, 0.9724309276498937, 0.9817783208740656, 0.983617427936186, 0.9851069246435845, 0.9810040705563093, 0.9843249773482332], [10, 10, 0.872822697193276, 0.825536660161695, 0.8147706968433591, 0.9083345497007737, 0.9335555555555556, 0.9423002850319698, 0.9092879773219943, 0.8885286545246496, 0.961010059048693, 0.9683411564965545], [10, 100, 0.6300808200275971, 0.38902439024390245, 0.2268041237113402, 0.8687783817640874, 0.9795482480001612, 0.8057343706804628, 0.6266354227689904, 0.3625, 0.9384784215359119, 0.9908118237320921], [10, 1000, 0.2680300968542384, 0.0, 0.0, 0.956479884481168, 1.0, 0.5482977356672555, 0.08064516129032258, 0.0, 0.9915077711905144, 1.0]], [[50, 1, 0.8240126490908466, 0.8224789519386869, 0.8024535221955229, 0.8400025467163277, 0.8524134218553742, 0.9001011268419532, 0.904168305151396, 0.892621168847777, 0.9095648752336295, 0.9265797573782365], [50, 10, 0.46582229898292077, 0.36298649722001586, 0.2872373784885544, 0.5653189997663005, 0.6579108874432307, 0.6951059730250482, 0.6234849766052202, 0.5590860056879474, 0.7629033209461452, 0.8297202797202797], [50, 100, 0.022572469507365755, 0.0008851515822084533, 0.0, 0.16217887831061828, 0.5630985915492958, 0.21241460081983213, 0.05404315386166566, 0.004366812227074236, 0.5538416893611322, 0.8829714377794787], [50, 1000, 0.0, 0.0, 0.0, 0.006755944027905858, 0.62492, 0.002750538148768237, 0.0, 0.0, 0.47978766025641023, 0.99774]], [[100, 1, 0.6848595249772297, 0.6679807103490508, 0.645726902762144, 0.6976662404092071, 0.7291053295679866, 0.8184169501133787, 0.8064897685605015, 0.796, 0.8310980584108337, 0.8408884034225378], [100, 10, 0.1660053164849559, 0.10665408211420481, 0.06542775456344843, 0.24876152073732719, 0.35106719559979555, 0.44688137288852975, 0.3636728837876614, 0.2703508597554915, 0.5424741840991804, 0.643412392897057], [100, 100, 3.970932772108168e-05, 0.0, 0.0, 0.001558452961060626, 0.032408430612203894, 0.01601083320057352, 0.0010602205258693808, 0.0, 0.13388488568632684, 0.5056263461964289], [100, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0015021932020750296, 0.4912]], [[500, 1, 0.11792689033550326, 0.1040832018129578, 0.08739282012626025, 0.13344316309719934, 0.1555226970279462, 0.3471720818291215, 0.325299306868305, 0.30202261084614024, 0.3772878401497789, 0.40369960852284614], [500, 10, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0012734429266033805, 0.0008295804693055226, 0.00022333891680625348, 0.00466315240688177, 0.011204005249598989], [500, 100, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [500, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]], [[1000, 1, 0.00947147711662208, 0.007310310057978321, 0.0059626472182526, 0.013262941024980685, 0.015009159132541512, 0.10863559717289133, 0.09160490827359981, 0.07464212678936605, 0.11893322874791647, 0.1326378093985412], [1000, 10, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [1000, 100, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [1000, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]], [[2000, 1, 0.0, 0.00012819964958762445, 5.1224259809445757e-05, 6.449948400412796e-05, 2.919281856663261e-05, 0.006314498087609151, 0.006229546977881166, 0.005082005082005082, 0.00850453575240128, 0.009976720984369804], [2000, 10, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [2000, 100, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [2000, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]]]


        x = np.array(np.log10(mus))
        # x = np.array([np.log10(result[mu][0][1]),np.log10(result[mu][1][1]),np.log10(result[mu][2][1]),np.log10(result[mu][3][1])])

        lim_angle = []
        lim_angle_m1 = []
        lim_angle_m2 = []
        lim_angle_p1 = []
        lim_angle_p2 = []

        lim_mtt = []
        lim_mtt_m1 = []
        lim_mtt_m2 = []
        lim_mtt_p1 = []
        lim_mtt_p2 = []

        for i in range(4):
            y_angle = np.array([result[0][i][2],result[1][i][2],result[2][i][2],result[3][i][2],result[4][i][2],result[5][i][2],result[6][i][2],result[7][i][2]])
            y_angle_m1 = np.array([result[0][i][3],result[1][i][3],result[2][i][3],result[3][i][3],result[4][i][3],result[5][i][3],result[6][i][3],result[7][i][3]])
            y_angle_m2 = np.array([result[0][i][4],result[1][i][4],result[2][i][4],result[3][i][4],result[4][i][4],result[5][i][4],result[6][i][4],result[7][i][4]])
            y_angle_p1 = np.array([result[0][i][5],result[1][i][5],result[2][i][5],result[3][i][5],result[4][i][5],result[5][i][5],result[6][i][5],result[7][i][5]])
            y_angle_p2 = np.array([result[0][i][6],result[1][i][6],result[2][i][6],result[3][i][6],result[4][i][6],result[5][i][6],result[6][i][6],result[7][i][6]])

            y_mtt = np.array([result[0][i][7],result[1][i][7],result[2][i][7],result[3][i][7],result[4][i][7],result[5][i][7],result[6][i][7],result[7][i][7]])
            y_mtt_m1 = np.array([result[0][i][8], result[1][i][8], result[2][i][8], result[3][i][8], result[4][i][8], result[5][i][8], result[6][i][8], result[7][i][8]])
            y_mtt_m2 = np.array([result[0][i][9], result[1][i][9], result[2][i][9], result[3][i][9], result[4][i][9], result[5][i][9], result[6][i][9], result[7][i][9]])
            y_mtt_p1 = np.array([result[0][i][10],result[1][i][10],result[2][i][10],result[3][i][10],result[4][i][10],result[5][i][10],result[6][i][10],result[7][i][10]])
            y_mtt_p2 = np.array([result[0][i][11],result[1][i][11],result[2][i][11],result[3][i][11],result[4][i][11],result[5][i][11],result[6][i][11],result[7][i][11]])

            new_y_angle = interpolate.UnivariateSpline(x, y_angle-0.05)
            new_y_angle_m1 = interpolate.UnivariateSpline(x, y_angle_m1-0.05)
            new_y_angle_m2 = interpolate.UnivariateSpline(x, y_angle_m2-0.05)
            new_y_angle_p1 = interpolate.UnivariateSpline(x, y_angle_p1-0.05)
            new_y_angle_p2 = interpolate.UnivariateSpline(x, y_angle_p2-0.05)

            new_y_mtt = interpolate.UnivariateSpline(x, y_mtt-0.05)
            new_y_mtt_m1 = interpolate.UnivariateSpline(x, y_mtt_m1-0.05)
            new_y_mtt_m2 = interpolate.UnivariateSpline(x, y_mtt_m2-0.05)
            new_y_mtt_p1 = interpolate.UnivariateSpline(x, y_mtt_p1-0.05)
            new_y_mtt_p2 = interpolate.UnivariateSpline(x, y_mtt_p2-0.05)

            # mu_angle = optimize.brentq(new_y_angle, 0.0, 3.3, args=())
            # mu_angle_m1 = optimize.brentq(new_y_angle_m1, 0.0, 3.3, args=())
            # mu_angle_m2 = optimize.brentq(new_y_angle_m2, 0.0, 3.3, args=())
            # mu_angle_p1 = optimize.brentq(new_y_angle_p1, 0.0, 3.3, args=())
            # mu_angle_p2 = optimize.brentq(new_y_angle_p2, 0.0, 3.3, args=())
            #
            # mu_mtt = optimize.brentq(new_y_mtt, 0.0, 3.3, args=())
            # mu_mtt_m1 = optimize.brentq(new_y_mtt_m1, 0.0, 3.3, args=())
            # mu_mtt_m2 = optimize.brentq(new_y_mtt_m2, 0.0, 3.3, args=())
            # mu_mtt_p1 = optimize.brentq(new_y_mtt_p1, 0.0, 3.3, args=())
            # mu_mtt_p2 = optimize.brentq(new_y_mtt_p2, 0.0, 3.3, args=())

            # print lumis[i]
            if new_y_angle.roots().size == 0:
                mu_angle = 2.
            else:
                mu_angle = min(new_y_angle.roots()  )
            if new_y_angle_m1.roots().size == 0:
                mu_angle_m1 = 2.
            else:
                mu_angle_m1 = min(new_y_angle_m1.roots() )
            if new_y_angle_m2.roots().size == 0:
                mu_angle_m2 = 2.
            else:
                mu_angle_m2 = min(new_y_angle_m2.roots() )
            if new_y_angle_p1.roots().size == 0:
                mu_angle_p1 = 2.
            else:
                mu_angle_p1 = min(new_y_angle_p1.roots() )
            if new_y_angle_p2.roots().size == 0:
                mu_angle_p2 = 2.
            else:
                mu_angle_p2 = min(new_y_angle_p2.roots() )


            if new_y_mtt.roots().size == 0:
                mu_mtt = 2.
            else:
                mu_mtt = min(new_y_mtt.roots()  )
            if new_y_mtt_m1.roots().size == 0:
                mu_mtt_m1 = 2.
            else:
                mu_mtt_m1 = min(new_y_mtt_m1.roots() )
            if new_y_mtt_m2.roots().size == 0:
                mu_mtt_m2 = 2.
            else:
                mu_mtt_m2 = min(new_y_mtt_m2.roots() )
            if new_y_mtt_p1.roots().size == 0:
                mu_mtt_p1 = 2.
            else:
                mu_mtt_p1 = min(new_y_mtt_p1.roots() )
            if new_y_mtt_p2.roots().size == 0:
                mu_mtt_p2 = 2.
            else:
                mu_mtt_p2 = min(new_y_mtt_p2.roots() )

            lim_angle.append(pow(10.,mu_angle))
            lim_angle_m1.append(pow(10.,mu_angle_m1))
            lim_angle_m2.append(pow(10.,mu_angle_m2))
            lim_angle_p1.append(pow(10.,mu_angle_p1))
            lim_angle_p2.append(pow(10.,mu_angle_p2))

            lim_mtt.append(pow(10.,mu_mtt))
            lim_mtt_m1.append(pow(10.,mu_mtt_m1))
            lim_mtt_m2.append(pow(10.,mu_mtt_m2))
            lim_mtt_p1.append(pow(10.,mu_mtt_p1))
            lim_mtt_p2.append(pow(10.,mu_mtt_p2))

        plt.yscale('log')
        plt.xscale('log')
        plt.ylim([0.01,100])
        # plt.plot(np.linspace(0,3), new_y(np.linspace(0,3)), color='black',linestyle='dotted', linewidth=2)
        polygon1 = Polygon(np.array([[lumis[0],lim_angle_p1[0]], [lumis[0],lim_angle_m1[0]], [lumis[1],lim_angle_m1[1]],[lumis[2],lim_angle_m1[2]],[lumis[3],lim_angle_m1[3]],[lumis[3],lim_angle_p1[3]],[lumis[2],lim_angle_p1[2]],[lumis[1],lim_angle_p1[1]]]), True,color='lime')
        polygon2 = Polygon(np.array([[lumis[0],lim_angle_p2[0]], [lumis[0],lim_angle_m2[0]], [lumis[1],lim_angle_m2[1]],[lumis[2],lim_angle_m2[2]],[lumis[3],lim_angle_m2[3]],[lumis[3],lim_angle_p2[3]],[lumis[2],lim_angle_p2[2]],[lumis[1],lim_angle_p2[1]]]), True,color='yellow')
        p2=PatchCollection([polygon2],color='yellow')
        p1=PatchCollection([polygon1],color='lime')
        fig, ax = plt.subplots()
        ax.set_yscale('log')
        ax.set_xscale('log')
        ax.set_ylim([0.01,100])

        ax.add_collection(p2)
        ax.add_collection(p1)
        sigma1 = mpatches.Patch(color='lime')
        sigma2 = mpatches.Patch(color='yellow')

        plt.plot(lumis, lim_angle, color='black',linestyle='dotted', linewidth=2)
        plt.plot(lumis, lim_mtt, color='red',linestyle='dotted', linewidth=2)
        angle=mlines.Line2D([], [], color='black',linestyle='dotted', linewidth=2)
        mtt=mlines.Line2D([], [], color='red',linestyle='dotted', linewidth=2)
        tmp=mlines.Line2D([], [], color='red',linestyle=' ', linewidth=2)
        plt.legend([angle,sigma1,sigma2,mtt,tmp],(r"$\theta_l+m_{tt}$", r"$\pm 1 \sigma$", r"$\pm 2 \sigma$", "$m_{tt}$", " "),loc=3, numpoints=1, fontsize=22,frameon=False,labelspacing=0.2,handlelength=2.6,handletextpad=0.5)
        plt.text(1.4,0.015,r"Di-leptonic channel, $g_{t_R}=6$, $m(g^1) = 3$ TeV", fontsize = 18)
        plt.xlabel(r"$\mathcal{L} \hspace{1} [\mathrm{fb}^{-1}]$", fontsize = 24)
        plt.ylabel(r"95% C.L. limit on $\mu$", fontsize = 22)
        #
        plt.tight_layout()
        plt.savefig(dir+model+".limits.pdf", dpi=100)
        plt.close()

for dir in ["./30tev/gluon_semil/", "./35tev/gluon_semil/", "./40tev/gluon_semil/"]:
    lumis = [1,10,100,1000] # in fb
    mus = [0.01,0.1,0.5,1,2,5,10,100]
    for model in ["g2.yoda", "g6.yoda"]:
        with open (dir+model+".lim", 'rb') as fp:
            result = pickle.load(fp)
        # result = [[[1, 1, 1.0, 0.9894265364564212, 1.0, 0.9966517152062669, 0.9959816644838672, 0.994828447107497, 0.9996484375, 1.0, 0.9981756061963047, 0.9981789486463518], [1, 10, 0.9921851174138456, 0.9913180741910024, 0.9911495624502784, 0.9926619507470135, 0.9933107872880091, 0.9952759534508584, 0.99328783980311, 0.989409984871407, 0.992658332350513, 0.9919774067779666], [1, 100, 0.9663266511756134, 0.9500096880449526, 0.7815533980582524, 0.9904237705284825, 0.9986095717884131, 0.968231556653724, 0.9538225713738264, 0.952, 0.9961007529580495, 0.9985497905252981], [1, 1000, 0.9088934523339389, 0.8266666666666667, 0.1, 0.9987769913988411, 1.0, 0.9589770271351957, 0.696969696969697, 0.1, 0.9999599342922393, 1.0]], [[10, 1, 0.9689962315861597, 0.9657318564072167, 0.9493115632996223, 0.9628328109665368, 0.9724309276498937, 0.9817783208740656, 0.983617427936186, 0.9851069246435845, 0.9810040705563093, 0.9843249773482332], [10, 10, 0.872822697193276, 0.825536660161695, 0.8147706968433591, 0.9083345497007737, 0.9335555555555556, 0.9423002850319698, 0.9092879773219943, 0.8885286545246496, 0.961010059048693, 0.9683411564965545], [10, 100, 0.6300808200275971, 0.38902439024390245, 0.2268041237113402, 0.8687783817640874, 0.9795482480001612, 0.8057343706804628, 0.6266354227689904, 0.3625, 0.9384784215359119, 0.9908118237320921], [10, 1000, 0.2680300968542384, 0.0, 0.0, 0.956479884481168, 1.0, 0.5482977356672555, 0.08064516129032258, 0.0, 0.9915077711905144, 1.0]], [[50, 1, 0.8240126490908466, 0.8224789519386869, 0.8024535221955229, 0.8400025467163277, 0.8524134218553742, 0.9001011268419532, 0.904168305151396, 0.892621168847777, 0.9095648752336295, 0.9265797573782365], [50, 10, 0.46582229898292077, 0.36298649722001586, 0.2872373784885544, 0.5653189997663005, 0.6579108874432307, 0.6951059730250482, 0.6234849766052202, 0.5590860056879474, 0.7629033209461452, 0.8297202797202797], [50, 100, 0.022572469507365755, 0.0008851515822084533, 0.0, 0.16217887831061828, 0.5630985915492958, 0.21241460081983213, 0.05404315386166566, 0.004366812227074236, 0.5538416893611322, 0.8829714377794787], [50, 1000, 0.0, 0.0, 0.0, 0.006755944027905858, 0.62492, 0.002750538148768237, 0.0, 0.0, 0.47978766025641023, 0.99774]], [[100, 1, 0.6848595249772297, 0.6679807103490508, 0.645726902762144, 0.6976662404092071, 0.7291053295679866, 0.8184169501133787, 0.8064897685605015, 0.796, 0.8310980584108337, 0.8408884034225378], [100, 10, 0.1660053164849559, 0.10665408211420481, 0.06542775456344843, 0.24876152073732719, 0.35106719559979555, 0.44688137288852975, 0.3636728837876614, 0.2703508597554915, 0.5424741840991804, 0.643412392897057], [100, 100, 3.970932772108168e-05, 0.0, 0.0, 0.001558452961060626, 0.032408430612203894, 0.01601083320057352, 0.0010602205258693808, 0.0, 0.13388488568632684, 0.5056263461964289], [100, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0015021932020750296, 0.4912]], [[500, 1, 0.11792689033550326, 0.1040832018129578, 0.08739282012626025, 0.13344316309719934, 0.1555226970279462, 0.3471720818291215, 0.325299306868305, 0.30202261084614024, 0.3772878401497789, 0.40369960852284614], [500, 10, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0012734429266033805, 0.0008295804693055226, 0.00022333891680625348, 0.00466315240688177, 0.011204005249598989], [500, 100, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [500, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]], [[1000, 1, 0.00947147711662208, 0.007310310057978321, 0.0059626472182526, 0.013262941024980685, 0.015009159132541512, 0.10863559717289133, 0.09160490827359981, 0.07464212678936605, 0.11893322874791647, 0.1326378093985412], [1000, 10, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [1000, 100, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [1000, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]], [[2000, 1, 0.0, 0.00012819964958762445, 5.1224259809445757e-05, 6.449948400412796e-05, 2.919281856663261e-05, 0.006314498087609151, 0.006229546977881166, 0.005082005082005082, 0.00850453575240128, 0.009976720984369804], [2000, 10, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [2000, 100, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [2000, 1000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]]]


        x = np.array(np.log10(mus))
        # x = np.array([np.log10(result[mu][0][1]),np.log10(result[mu][1][1]),np.log10(result[mu][2][1]),np.log10(result[mu][3][1])])

        lim_angle = []
        lim_angle_m1 = []
        lim_angle_m2 = []
        lim_angle_p1 = []
        lim_angle_p2 = []

        lim_mtt = []
        lim_mtt_m1 = []
        lim_mtt_m2 = []
        lim_mtt_p1 = []
        lim_mtt_p2 = []

        for i in range(4):
            y_angle = np.array([result[0][i][2],result[1][i][2],result[2][i][2],result[3][i][2],result[4][i][2],result[5][i][2],result[6][i][2],result[7][i][2]])
            y_angle_m1 = np.array([result[0][i][3],result[1][i][3],result[2][i][3],result[3][i][3],result[4][i][3],result[5][i][3],result[6][i][3],result[7][i][3]])
            y_angle_m2 = np.array([result[0][i][4],result[1][i][4],result[2][i][4],result[3][i][4],result[4][i][4],result[5][i][4],result[6][i][4],result[7][i][4]])
            y_angle_p1 = np.array([result[0][i][5],result[1][i][5],result[2][i][5],result[3][i][5],result[4][i][5],result[5][i][5],result[6][i][5],result[7][i][5]])
            y_angle_p2 = np.array([result[0][i][6],result[1][i][6],result[2][i][6],result[3][i][6],result[4][i][6],result[5][i][6],result[6][i][6],result[7][i][6]])

            y_mtt = np.array([result[0][i][7],result[1][i][7],result[2][i][7],result[3][i][7],result[4][i][7],result[5][i][7],result[6][i][7],result[7][i][7]])
            y_mtt_m1 = np.array([result[0][i][8], result[1][i][8], result[2][i][8], result[3][i][8], result[4][i][8], result[5][i][8], result[6][i][8], result[7][i][8]])
            y_mtt_m2 = np.array([result[0][i][9], result[1][i][9], result[2][i][9], result[3][i][9], result[4][i][9], result[5][i][9], result[6][i][9], result[7][i][9]])
            y_mtt_p1 = np.array([result[0][i][10],result[1][i][10],result[2][i][10],result[3][i][10],result[4][i][10],result[5][i][10],result[6][i][10],result[7][i][10]])
            y_mtt_p2 = np.array([result[0][i][11],result[1][i][11],result[2][i][11],result[3][i][11],result[4][i][11],result[5][i][11],result[6][i][11],result[7][i][11]])

            new_y_angle = interpolate.UnivariateSpline(x, y_angle-0.05)
            new_y_angle_m1 = interpolate.UnivariateSpline(x, y_angle_m1-0.05)
            new_y_angle_m2 = interpolate.UnivariateSpline(x, y_angle_m2-0.05)
            new_y_angle_p1 = interpolate.UnivariateSpline(x, y_angle_p1-0.05)
            new_y_angle_p2 = interpolate.UnivariateSpline(x, y_angle_p2-0.05)

            new_y_mtt = interpolate.UnivariateSpline(x, y_mtt-0.05)
            new_y_mtt_m1 = interpolate.UnivariateSpline(x, y_mtt_m1-0.05)
            new_y_mtt_m2 = interpolate.UnivariateSpline(x, y_mtt_m2-0.05)
            new_y_mtt_p1 = interpolate.UnivariateSpline(x, y_mtt_p1-0.05)
            new_y_mtt_p2 = interpolate.UnivariateSpline(x, y_mtt_p2-0.05)

            # mu_angle = optimize.brentq(new_y_angle, 0.0, 3.3, args=())
            # mu_angle_m1 = optimize.brentq(new_y_angle_m1, 0.0, 3.3, args=())
            # mu_angle_m2 = optimize.brentq(new_y_angle_m2, 0.0, 3.3, args=())
            # mu_angle_p1 = optimize.brentq(new_y_angle_p1, 0.0, 3.3, args=())
            # mu_angle_p2 = optimize.brentq(new_y_angle_p2, 0.0, 3.3, args=())
            #
            # mu_mtt = optimize.brentq(new_y_mtt, 0.0, 3.3, args=())
            # mu_mtt_m1 = optimize.brentq(new_y_mtt_m1, 0.0, 3.3, args=())
            # mu_mtt_m2 = optimize.brentq(new_y_mtt_m2, 0.0, 3.3, args=())
            # mu_mtt_p1 = optimize.brentq(new_y_mtt_p1, 0.0, 3.3, args=())
            # mu_mtt_p2 = optimize.brentq(new_y_mtt_p2, 0.0, 3.3, args=())

            # print lumis[i]
            if new_y_angle.roots().size == 0:
                mu_angle = 2.
            else:
                mu_angle = min(new_y_angle.roots()  )
            if new_y_angle_m1.roots().size == 0:
                mu_angle_m1 = 2.
            else:
                mu_angle_m1 = min(new_y_angle_m1.roots() )
            if new_y_angle_m2.roots().size == 0:
                mu_angle_m2 = 2.
                # mu_angle_m2 = -1.2
            else:
                mu_angle_m2 = min(new_y_angle_m2.roots() )
            if new_y_angle_p1.roots().size == 0:
                mu_angle_p1 = 2.
            else:
                mu_angle_p1 = min(new_y_angle_p1.roots() )
            if new_y_angle_p2.roots().size == 0:
                mu_angle_p2 = 2.
            else:
                mu_angle_p2 = min(new_y_angle_p2.roots() )


            if new_y_mtt.roots().size == 0:
                mu_mtt = 2.
            else:
                mu_mtt = min(new_y_mtt.roots()  )
            if new_y_mtt_m1.roots().size == 0:
                mu_mtt_m1 = 2.
            else:
                mu_mtt_m1 = min(new_y_mtt_m1.roots() )
            if new_y_mtt_m2.roots().size == 0:
                mu_mtt_m2 = 2.
            else:
                mu_mtt_m2 = min(new_y_mtt_m2.roots() )
            if new_y_mtt_p1.roots().size == 0:
                mu_mtt_p1 = 2.
            else:
                mu_mtt_p1 = min(new_y_mtt_p1.roots() )
            if new_y_mtt_p2.roots().size == 0:
                mu_mtt_p2 = 2.
            else:
                mu_mtt_p2 = min(new_y_mtt_p2.roots() )
            lim_angle.append(pow(10.,mu_angle))
            lim_angle_m1.append(pow(10.,mu_angle_m1))
            lim_angle_p1.append(pow(10.,mu_angle_p1))
            lim_angle_m2.append(pow(10.,mu_angle_m2))
            lim_angle_p2.append(pow(10.,mu_angle_p2))

            lim_mtt.append(pow(10.,mu_mtt))
            lim_mtt_m1.append(pow(10.,mu_mtt_m1))
            lim_mtt_m2.append(pow(10.,mu_mtt_m2))
            lim_mtt_p1.append(pow(10.,mu_mtt_p1))
            lim_mtt_p2.append(pow(10.,mu_mtt_p2))

        plt.yscale('log')
        plt.xscale('log')
        plt.ylim([0.01,10])
        # plt.plot(np.linspace(0,3), new_y(np.linspace(0,3)), color='black',linestyle='dotted', linewidth=2)
        polygon1 = Polygon(np.array([[lumis[0],lim_angle_p1[0]], [lumis[0],lim_angle_m1[0]], [lumis[1],lim_angle_m1[1]],[lumis[2],lim_angle_m1[2]],[lumis[3],lim_angle_m1[3]],[lumis[3],lim_angle_p1[3]],[lumis[2],lim_angle_p1[2]],[lumis[1],lim_angle_p1[1]]]), True,color='lime')
        polygon2 = Polygon(np.array([[lumis[0],lim_angle_p2[0]], [lumis[0],lim_angle_m2[0]], [lumis[1],lim_angle_m2[1]],[lumis[2],lim_angle_m2[2]],[lumis[3],lim_angle_m2[3]],[lumis[3],lim_angle_p2[3]],[lumis[2],lim_angle_p2[2]],[lumis[1],lim_angle_p2[1]]]), True,color='yellow')
        p2=PatchCollection([polygon2],color='yellow')
        p1=PatchCollection([polygon1],color='lime')
        fig, ax = plt.subplots()
        ax.set_yscale('log')
        ax.set_xscale('log')
        ax.set_ylim([0.01,10])

        ax.add_collection(p2)
        ax.add_collection(p1)
        sigma1 = mpatches.Patch(color='lime')
        sigma2 = mpatches.Patch(color='yellow')

        plt.plot(lumis, lim_angle, color='black',linestyle='dotted', linewidth=2)
        plt.plot(lumis, lim_mtt, color='red',linestyle='dotted', linewidth=2)
        angle=mlines.Line2D([], [], color='black',linestyle='dotted', linewidth=2)
        mtt=mlines.Line2D([], [], color='red',linestyle='dotted', linewidth=2)
        tmp=mlines.Line2D([], [], color='red',linestyle=' ', linewidth=2)
        plt.legend([angle,sigma1,sigma2,mtt,tmp],(r"$\theta_l+m_{tt}$", r"$\pm 1 \sigma$", r"$\pm 2 \sigma$", "$m_{tt}$", " "),loc=3, numpoints=1, fontsize=22,frameon=False,labelspacing=0.2,handlelength=2.6,handletextpad=0.5)
        plt.text(1.4,0.015,r"Semi-leptonic channel, $g_{t_R}=2$, $m(g^1) = 3$ TeV", fontsize = 17)
        plt.xlabel(r"$\mathcal{L} \hspace{1} [\mathrm{fb}^{-1}]$", fontsize = 24)
        plt.ylabel(r"95% C.L. limit on $\mu$", fontsize = 22)
        #
        plt.tight_layout()
        plt.savefig(dir+model+".limits.pdf", dpi=100)
        plt.close()
