import yoda
from cls import *
import math
import numpy as np
import pickle
import sys

systematics = 0.1

# if str(sys.argv[1]) == "graviton_dilep":
#     dirs = ["./30tev/graviton_dilep/", "./35tev/graviton_dilep/", "./40tev/graviton_dilep/"]
# elif str(sys.argv[1]) == "graviton_semil":
#     dirs = ["./30tev/graviton_semil/", "./35tev/graviton_semil/", "./40tev/graviton_semil/"]
# elif str(sys.argv[1]) == "gluon_dilep":
#     dirs = ["./30tev/gluon_dilep/", "./35tev/gluon_dilep/", "./40tev/gluon_dilep/"]
# elif str(sys.argv[1]) == "gluon_semil":
#     dirs = ["./30tev/gluon_semil/", "./35tev/gluon_semil/", "./40tev/gluon_semil/"]

for dir in sys.argv[1:]:
    luminosity = [1,10,100,1000] # in fb
    if "graviton" in str(dir):
        mus = [1,10,50,100,500,1000,2000,10000]
        models = ["c1.yoda", "c2.yoda"]
    elif "gluon" in str(dir):
        mus = [0.01,0.1,0.5,1,2,5,10,100]
        models = ["g2.yoda", "g6.yoda"]
    for model in models:
        signal_plots=yoda.read(str(dir)+model, asdict=False)
        background_plots=yoda.read(str(dir)+"background.yoda", asdict=False)

        solutions = []
        #test=np.full((10,1),0.)
        #test[1]=14

        #print loglikelihood(np.full((10,1),6.),np.full((10,1),0.),np.full((10,1),10.),np.full((10,1),2.),np.full((10,1),13.),10)
        #print loglikelihood(test,np.full((10,1),0.),np.full((10,1),10.),np.full((10,1),2.),np.full((10,1),13.),10)
        #print loglikelihood(np.full((1,1),60.),np.full((1,1),0.),np.full((1,1),100.),np.full((1,1),20.),np.full((1,1),130.),1)

        for mu in mus:
            sols = []
            print "Mu: " + str(mu)
            for lumi in luminosity:

                print "Lumi: " + str(lumi) + " fb"
                signal_lepton_angle=signal_plots[4]
                background_lepton_angle=background_plots[4]

                signal=np.zeros(signal_lepton_angle.numBins)
                signalErr=np.zeros(signal_lepton_angle.numBins)
                background=np.zeros(signal_lepton_angle.numBins)
                backgroundErr=np.zeros(signal_lepton_angle.numBins)

                if "dilep" in str(dir):

                    total_signal = 0.
                    total_background = 0.
                    squared_sum = 0.
                    xsec_sig = signal_plots[32].point(0).x
                    totalevents_sig = signal_plots[31].sumW()
                    passedevents_sig = int(signal_plots[28].effNumEntries(False))/2.

                    xsec_back = background_plots[32].point(0).x
                    totalevents_back = background_plots[31].sumW()
                    passedevents_back = int(background_plots[28].effNumEntries(False))/2.

                    # print passedevents_back, totalevents_back, xsec_back
                    # print passedevents_sig, totalevents_sig, xsec_sig


                    for i in range(signal_lepton_angle.numBins):
                        total_signal += signal_lepton_angle.bin(i).height
                        total_background += background_lepton_angle.bin(i).height
                        squared_sum += background_lepton_angle.bin(i).height**2

                    signal_to_fb =  mu * 1000 * xsec_sig * (passedevents_sig/totalevents_sig) / total_signal
                    background_to_fb =  1000 * xsec_back * 1.55 * (passedevents_back/totalevents_back) / total_background
                    # signal_to_fb =  mu * 40 * 1000
                    # background_to_fb =  1000 * 40
                    total_error = total_background * systematics
		    total_signal = 0.
                    total_background = 0.
                    bin_systematics = total_error/sqrt(squared_sum)
		    print total_error, sqrt(squared_sum)
		    print bin_systematics

                    for i in range(signal_lepton_angle.numBins):
                        signal[i] = signal_lepton_angle.bin(i).height * signal_to_fb * lumi
                        total_signal += signal_lepton_angle.bin(i).height * signal_to_fb
                        signalErr[i] = 0
                        background[i] = background_lepton_angle.bin(i).height * background_to_fb * lumi
                        total_background += background_lepton_angle.bin(i).height * background_to_fb
                        backgroundErr[i] = background_lepton_angle.bin(i).height * background_to_fb * lumi * bin_systematics

                    print total_signal, total_background
                    print sum(signal), sum(background)


                elif "semil" in str(dir):
                    total_signal = 0.
                    total_background = 0.
                    squared_sum = 0.
                    xsec_sig = signal_plots[15].point(0).x
                    totalevents_sig = signal_plots[14].sumW()
                    passedevents_sig = int(signal_plots[13].effNumEntries(False))

                    xsec_back = background_plots[15].point(0).x
                    totalevents_back = background_plots[14].sumW()
                    passedevents_back = int(background_plots[13].effNumEntries(False))

                    # print passedevents_back, totalevents_back, xsec_back
                    # print passedevents_sig, totalevents_sig, xsec_sig

                    for i in range(signal_lepton_angle.numBins):
                        total_signal += signal_lepton_angle.bin(i).height
                        total_background += background_lepton_angle.bin(i).height
                        squared_sum += background_lepton_angle.bin(i).height**2

                    signal_to_fb =  4 * mu * 1000 * xsec_sig * (passedevents_sig/totalevents_sig) / total_signal
                    background_to_fb =  4 * 1000 * xsec_back * 1.55 * (passedevents_back/totalevents_back) / total_background
                    total_signal = 0.
                    total_error = total_background * systematics
                    total_background = 0.
                    bin_systematics = total_error/sqrt(squared_sum)

                    for i in range(signal_lepton_angle.numBins):
                        signal[i] = signal_lepton_angle.bin(i).height * signal_to_fb * lumi
                        total_signal += signal_lepton_angle.bin(i).height * signal_to_fb
                        signalErr[i] = 0
                        background[i] = background_lepton_angle.bin(i).height * background_to_fb * lumi
                        total_background += background_lepton_angle.bin(i).height * background_to_fb
                        backgroundErr[i] = background_lepton_angle.bin(i).height * background_to_fb * lumi * bin_systematics

                    print total_signal, total_background
                    print sum(signal), sum(background)




                # cl_angle = loglikelihood(signal,signalErr,background,backgroundErr,background-0*backgroundErr,signal_lepton_angle.numBins)
                # cl_angle_m1 = loglikelihood(signal,signalErr,background,backgroundErr,background-np.sqrt(background),signal_lepton_angle.numBins)
                # cl_angle_m2 = loglikelihood(signal,signalErr,background,backgroundErr,background-2*np.sqrt(background),signal_lepton_angle.numBins)
                # cl_angle_p1 = loglikelihood(signal,signalErr,background,backgroundErr,background+np.sqrt(background),signal_lepton_angle.numBins)
                # cl_angle_p2 = loglikelihood(signal,signalErr,background,backgroundErr,background+2*np.sqrt(background),signal_lepton_angle.numBins)

                cl_angle = loglikelihood(signal,signalErr,background,backgroundErr,background-0*backgroundErr,signal_lepton_angle.numBins)
                cl_angle_m1 = loglikelihood(signal,signalErr,background,backgroundErr,background-0.5*backgroundErr,signal_lepton_angle.numBins)
                cl_angle_m2 = loglikelihood(signal,signalErr,background,backgroundErr,background-1*backgroundErr,signal_lepton_angle.numBins)
                cl_angle_p1 = loglikelihood(signal,signalErr,background,backgroundErr,background+0.5*backgroundErr,signal_lepton_angle.numBins)
                cl_angle_p2 = loglikelihood(signal,signalErr,background,backgroundErr,background+1*backgroundErr,signal_lepton_angle.numBins)

                if "dilep" in str(dir):
                    signal_mttbar=signal_plots[28]
                    background_mttbar=background_plots[28]
                elif "semil" in str(dir):
                    signal_mttbar=signal_plots[13]
                    background_mttbar=background_plots[13]

                # signal=np.zeros(signal_mttbar.numBins)
                # signalErr=np.zeros(signal_mttbar.numBins)
                # background=np.zeros(signal_mttbar.numBins)
                # backgroundErr=np.zeros(signal_mttbar.numBins)
                #
                # signal_to_fb =  200 * 1000 * mu # 200 to correct for bin size
                # background_to_fb =  200 * 1000 # 200 to correct for bin size
                # total_signal = 0.
                # total_background = 0.
                #
                # for i in range(signal_mttbar.numBins):
                #   signal[i] = signal_mttbar.bin(i).height * signal_to_fb * lumi
                #   total_signal += signal_mttbar.bin(i).height * signal_to_fb
                #   signalErr[i] = 0
                #   background[i] = background_mttbar.bin(i).height * background_to_fb * lumi
                #   total_background += background_mttbar.bin(i).height * background_to_fb
                #   backgroundErr[i] = background_mttbar.bin(i).height * background_to_fb * lumi * systematics
                #
                # print total_signal, total_background
                #
                # cl_mtt = loglikelihood(signal,signalErr,background,backgroundErr,background-0*backgroundErr,signal_mttbar.numBins)
                # signal=np.zeros(signal_mttbar.numBins*10)
                # signalErr=np.zeros(signal_mttbar.numBins*10)
                # background=np.zeros(signal_mttbar.numBins*10)
                # backgroundErr=np.zeros(signal_mttbar.numBins*10)
                signal=np.zeros(signal_mttbar.numBins)
                signalErr=np.zeros(signal_mttbar.numBins)
                background=np.zeros(signal_mttbar.numBins)
                backgroundErr=np.zeros(signal_mttbar.numBins)

                if "dilep" in str(dir):

                    total_signal = 0.
                    total_background = 0.
                    squared_sum = 0.
                    xsec_sig = signal_plots[32].point(0).x
                    totalevents_sig = signal_plots[31].sumW()
                    passedevents_sig = int(signal_plots[28].effNumEntries(False))/4.

                    xsec_back = background_plots[32].point(0).x
                    totalevents_back = background_plots[31].sumW()
                    passedevents_back = int(background_plots[28].effNumEntries(False))/4.

                    # print passedevents_back, totalevents_back, xsec_back
                    # print passedevents_sig, totalevents_sig, xsec_sig

                    for i in range(signal_mttbar.numBins):
                        total_signal += signal_mttbar.bin(i).height
                        total_background += background_mttbar.bin(i).height
                        squared_sum += background_mttbar.bin(i).height**2

                    # signal_to_fb =  mu * 100 * xsec_sig * (passedevents_sig/totalevents_sig) / total_signal
                    # background_to_fb =  100 * xsec_back * (passedevents_back/totalevents_back) / total_background
                    # # signal_to_fb =  mu * 40 * 1000
                    # # background_to_fb =  1000 * 40
                    # total_signal = 0.
                    # total_background = 0.
                    # for j in range(10):
                    #     for i in range(signal_mttbar.numBins):
                    #         signal[i+signal_mttbar.numBins*j] = signal_mttbar.bin(i).height * signal_to_fb * lumi
                    #         total_signal += signal_mttbar.bin(i).height * signal_to_fb
                    #         signalErr[i+signal_mttbar.numBins*j] = 0
                    #         background[i+signal_mttbar.numBins*j] = background_mttbar.bin(i).height * background_to_fb * lumi
                    #         total_background += background_mttbar.bin(i).height * background_to_fb
                    #         backgroundErr[i+signal_mttbar.numBins*j] = background_mttbar.bin(i).height * background_to_fb * lumi * systematics

                    signal_to_fb =  mu * 1000 * xsec_sig * (passedevents_sig/totalevents_sig) / total_signal
                    background_to_fb =  1000 * xsec_back * 1.55 * (passedevents_back/totalevents_back) / total_background
                    total_error = total_background * systematics
                    bin_systematics = total_error/sqrt(squared_sum)
                    print bin_systematics
		    # signal_to_fb =  mu * 40 * 1000
                    # background_to_fb =  1000 * 40
                    total_signal = 0.
                    total_background = 0.
                    for i in range(signal_mttbar.numBins):
                        signal[i] = signal_mttbar.bin(i).height * signal_to_fb * lumi
                        total_signal += signal_mttbar.bin(i).height * signal_to_fb
                        signalErr[i] = 0
                        background[i] = background_mttbar.bin(i).height * background_to_fb * lumi
                        total_background += background_mttbar.bin(i).height * background_to_fb
                        backgroundErr[i] = background_mttbar.bin(i).height * background_to_fb * lumi * bin_systematics

                    print total_signal, total_background
                    print sum(signal), sum(background)


                elif "semil" in str(dir):
                    total_signal = 0.
                    total_background = 0.
                    squared_sum = 0.
                    xsec_sig = signal_plots[15].point(0).x
                    totalevents_sig = signal_plots[14].sumW()
                    passedevents_sig = int(signal_plots[13].effNumEntries(False))

                    xsec_back = background_plots[15].point(0).x
                    totalevents_back = background_plots[14].sumW()
                    passedevents_back = int(background_plots[13].effNumEntries(False))

                    # print passedevents_back, totalevents_back, xsec_back
                    # print passedevents_sig, totalevents_sig, xsec_sig

                    for i in range(signal_mttbar.numBins):
                        total_signal += signal_mttbar.bin(i).height
                        total_background += background_mttbar.bin(i).height
                        squared_sum += background_mttbar.bin(i).height**2
                    # signal_to_fb =  3 * mu * 100 * xsec_sig * (passedevents_sig/totalevents_sig) / total_signal
                    # background_to_fb =  3 * 100 * xsec_back * (passedevents_back/totalevents_back) / total_background
                    # total_signal = 0.
                    # total_background = 0.
                    #
                    # for j in range(10):
                    #     for i in range(signal_mttbar.numBins):
                    #         signal[i+signal_mttbar.numBins*j] = signal_mttbar.bin(i).height * signal_to_fb * lumi
                    #         total_signal += signal_mttbar.bin(i).height * signal_to_fb
                    #         signalErr[i+signal_mttbar.numBins*j] = 0
                    #         background[i+signal_mttbar.numBins*j] = background_mttbar.bin(i).height * background_to_fb * lumi
                    #         total_background += background_mttbar.bin(i).height * background_to_fb
                    #         backgroundErr[i+signal_mttbar.numBins*j] = background_mttbar.bin(i).height * background_to_fb * lumi * systematics

                    signal_to_fb =  4 * mu * 1000 * xsec_sig * (passedevents_sig/totalevents_sig) / total_signal
                    background_to_fb =  4 * 1000 * xsec_back * 1.55 * (passedevents_back/totalevents_back) / total_background
                    total_signal = 0.
                    total_error = total_background * systematics
                    total_background = 0.
                    bin_systematics = total_error/sqrt(squared_sum)

                    for i in range(signal_mttbar.numBins):
                        signal[i] = signal_mttbar.bin(i).height * signal_to_fb * lumi
                        total_signal += signal_mttbar.bin(i).height * signal_to_fb
                        signalErr[i] = 0
                        background[i] = background_mttbar.bin(i).height * background_to_fb * lumi
                        total_background += background_mttbar.bin(i).height * background_to_fb
                        backgroundErr[i] = background_mttbar.bin(i).height * background_to_fb * lumi * bin_systematics

                    print total_signal, total_background
                    print sum(signal), sum(background)


                # signal_to_fb =  20 * 1000 * mu # 200 to correct for bin size
                # background_to_fb =  20 * 1000 # 200 to correct for bin size
                # total_signal = 0.
                # total_background = 0.
                #
                # for j in range(10):
                #     for i in range(signal_mttbar.numBins):
                #         signal[i+signal_mttbar.numBins*j] = signal_mttbar.bin(i).height * signal_to_fb * lumi
                #         total_signal += signal_mttbar.bin(i).height * signal_to_fb
                #         signalErr[i+signal_mttbar.numBins*j] = 0
                #         background[i+signal_mttbar.numBins*j] = background_mttbar.bin(i).height * background_to_fb * lumi
                #         total_background += background_mttbar.bin(i).height * background_to_fb
                #         backgroundErr[i+signal_mttbar.numBins*j] = background_mttbar.bin(i).height * background_to_fb * lumi * systematics

                        #   print total_signal, total_background

                # cl_mtt = loglikelihood(signal,signalErr,background,backgroundErr,background-0*backgroundErr,signal_mttbar.numBins*10)
                # cl_mtt_m1 = loglikelihood(signal,signalErr,background,backgroundErr,background-np.sqrt(background),signal_mttbar.numBins*10)
                # cl_mtt_m2 = loglikelihood(signal,signalErr,background,backgroundErr,background-2*np.sqrt(background),signal_mttbar.numBins*10)
                # cl_mtt_p1 = loglikelihood(signal,signalErr,background,backgroundErr,background+np.sqrt(background),signal_mttbar.numBins*10)
                # cl_mtt_p2 = loglikelihood(signal,signalErr,background,backgroundErr,background+2*np.sqrt(background),signal_mttbar.numBins*10)

                # cl_mtt = loglikelihood(signal,signalErr,background,backgroundErr,background-0*backgroundErr,signal_mttbar.numBins)
                # cl_mtt_m1 = loglikelihood(signal,signalErr,background,backgroundErr,background-np.sqrt(background),signal_mttbar.numBins)
                # cl_mtt_m2 = loglikelihood(signal,signalErr,background,backgroundErr,background-2*np.sqrt(background),signal_mttbar.numBins)
                # cl_mtt_p1 = loglikelihood(signal,signalErr,background,backgroundErr,background+np.sqrt(background),signal_mttbar.numBins)
                # cl_mtt_p2 = loglikelihood(signal,signalErr,background,backgroundErr,background+2*np.sqrt(background),signal_mttbar.numBins)

                cl_mtt = loglikelihood(signal,signalErr,background,backgroundErr,background-0*backgroundErr,signal_mttbar.numBins)
                cl_mtt_m1 = loglikelihood(signal,signalErr,background,backgroundErr,background-0.5*backgroundErr,signal_mttbar.numBins)
                cl_mtt_m2 = loglikelihood(signal,signalErr,background,backgroundErr,background-1*backgroundErr,signal_mttbar.numBins)
                cl_mtt_p1 = loglikelihood(signal,signalErr,background,backgroundErr,background+0.5*backgroundErr,signal_mttbar.numBins)
                cl_mtt_p2 = loglikelihood(signal,signalErr,background,backgroundErr,background+1*backgroundErr,signal_mttbar.numBins)

                print "CLs from angle: " + str(cl_angle) + "   CLs from mTT: " + str(cl_mtt)
                sols.append([mu,lumi,cl_angle,cl_angle_m1,cl_angle_m2,cl_angle_p1,cl_angle_p2,cl_mtt,cl_mtt_m1,cl_mtt_m2,cl_mtt_p1,cl_mtt_p2])

            solutions.append(sols)

        print solutions
        with open(str(dir)+model+".lim", 'wb') as fp:
            pickle.dump(solutions, fp)
