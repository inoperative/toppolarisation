#! /usr/bin/env python

import yoda
from math import *
from scipy.stats import poisson
import sys
try:
    import numpy as np
except Exception:
    print ''' WARNING:
    Could not load numpy. CLs value cannot be calculated!
    '''

#def cls_bins(Nsig, errNsig, Nbkg, errNbkg, Nobs, nbins):
#  ntot = 1000
#  histbg=yoda.Histo1D(1000,-49.95,49.05)
#  histsigbg=yoda.Histo1D(1000,-49.95,49.05)
#  CLs=yoda.Histo1D(1000,-49.95,49.05)
#  for i in range(ntot):
#    qsigbg=1.0
#    qbg=1.0
#    bglambda=np.zeros(nbins)
#    siglambda=np.zeros(nbins)
#    bg=np.zeros(nbins)
#    sigbg=np.zeros(nbins)
#    for j in range(nbins):
#      bglambda[j] = abs(np.random.normal(Nbkg[j] ,0.00000001+errNbkg[j]))
#      siglambda[j] = abs(np.random.normal(Nsig[j] ,0.00000001+errNsig[j]))
#      bg[j] = np.random.poisson(bglambda[j])
#      sigbg[j] = np.random.poisson(siglambda[j]+bglambda[j])
#
#      if poisson.cdf(bg[j], Nbkg[j]) < 0.000001:
#        qbg *= 1
#      else:
#        qbg *= poisson.cdf(bg[j], Nbkg[j]+Nsig[j]) / poisson.cdf(bg[j], Nbkg[j])
#
#      if poisson.cdf(sigbg[j], Nbkg[j]) < 0.000001:
#        qsigbg *= 1
#      else:
#        qsigbg *= poisson.cdf(sigbg[j], Nbkg[j]+Nsig[j]) / poisson.cdf(sigbg[j], Nbkg[j])
#
#    histbg.fill(-2.0 * log(qbg))
#    histsigbg.fill(-2.0 * log(qsigbg))
#
#  qbgd=1.0
#
#  for j in range(nbins):
#    qbgd *= poisson.cdf(Nobs[j], Nbkg[j]+Nsig[j]) / poisson.cdf(Nobs[j], Nbkg[j]);
#
#
#  qbgd = -2.0 * log(qbgd)
#
#  print "-2*ln(Q) = " + str(qbgd)
#
#
#  runningsumbg=histbg.sumW(False)
#  runningsumsigbg=histsigbg.sumW(False)
#
#  for i in range(1000):
#    if runningsumbg > 0.0:
#      CLs.fill(-49.95 + i * (99./1000.),runningsumsigbg / runningsumbg)
#
#    runningsumbg -= histbg[i].height
#    runningsumsigbg -= histsigbg[i].height
#
#  print "CLs = " + str(CLs.binAt(qbgd).height)

def cls_bins(Nsig, errNsig, Nbkg, errNbkg, Nobs, nbins):
  cl = 1.
  for i in range(nbins):
    cls1 = cls(Nsig[i],errNsig[i],Nbkg[i],[errNbkg[i]],Nobs[i])
    cl = cl * cls1
#    print cls1,Nsig[i],errNsig[i],Nbkg[i],[errNbkg[i]],Nobs[i]
  return cl


def cls(Nsig, errNsig, Nbkg, errNbkg, Nobs):
    if Nsig == 0:
      return 1
    distListSig = []
    distList = []
    nb = 0
    nsb = 0
    ntot = 100000
    sigList = np.random.poisson(Nsig,ntot)
    #sigList = np.random.normal(Nsig,errNsig,ntot)
    errList = []
    #lumiError = np.random.normal(1, 0.036, ntot)
    lumiError = np.ones(ntot)
    for err in errNbkg:
        errList.append(np.random.normal(0,0.000001+err,ntot))

    for i in range(0,ntot):
        totErr = 0
        for j in range(0, len(errNbkg)):
            totErr += errList[j][i]
        distListSig.append(sigList[i]+np.random.poisson(max(0.000001,(Nbkg+totErr)*lumiError[i])))
        distList.append(np.random.poisson(max(0.000001,(Nbkg+totErr)*lumiError[i])))
        if distList[i]<Nobs:
            nb=nb+1
        if distListSig[i]<Nobs:
            nsb=nsb+1
    #print "nsb, nb", nsb, nb, Nsig, errNsig, Nbkg, errNbkg, Nobs
#    if nsb>nb:
#        cls=1
#    else:
#        cls=float(nsb)/float(nb)
    cls=float(nsb)/float(nb)
    return cls

def altCLs(Nbkg, Nobs, CL):
    Nsig=0
    while (True):
        clb = poisson.cdf(Nobs, Nbkg)
        clbs = poisson.cdf(Nobs, Nbkg+Nsig)
        cls = clbs/clb
        if (cls < (1 - CL)):
            break
        Nsig += 1
    return Nsig

def calcCLs(Nbkg, errNbkg, Nobs, confLvl):
    alpha = 1
    sig = Nbkg / 2
    step = Nbkg/10 + 1
    while ( abs(confLvl - 1 + alpha) > 0.001 ):
        sig, alpha = calcStep(Nbkg, errNbkg, Nobs, confLvl, sig, step)
        step /= -2.
    return sig

def calcStep(Nbkg, errNbkg, Nobs, confLvl, start, stepsize):
    sig = start
    sign = 1
    sign = copysign(sign, stepsize)
    if(sign < 0):
        alpha = 0
    else:
        alpha = 1
    while(sign*alpha > sign*(1 - confLvl)):
        if sig < 0:
            sig = 0
        alpha = cls(sig, 0, Nbkg, errNbkg, Nobs)
        sig += stepsize
    return sig, alpha

def loglikelihood(Nsig, errNsig, Nbkg, errNbkg, Nobs, nbins):
  clb = 0.
  clsb = 0.
  for j in range(500):
    qobs = 0
    used = 0
    used_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 120, 121, 122, 123, 124, 125, 126, 127, 128, 130, 131, 132, 133, 137, 138, 139, 140, 141, 142, 145, 146, 156, 157, 165, 168]
    for i in range(nbins):
      if Nbkg[i] == 0:
        continue
      if Nobs[i] <= 0:
        continue
      if i not in used_list:
	continue
    #   if Nbkg[i] <= 0.1:
    #     continue
    #   if Nsig[i] <= 0.1:
    #     continue
      qobs = qobs + (Nsig[i] - Nobs[i] * log(1+Nsig[i]/(Nbkg[i])))

    qobsh0 = 0
    qobsh1 = 0
    used = 0

    for i in range(nbins):
      if Nbkg[i] == 0:
        continue
      if Nobs[i] <= 0:
        continue
      #if (used >= 144 and nbins == 200) or (used >= 17 and nbins == 20):
      if i not in used_list:
	continue
      used = used + 1
      used_list.append(i)
    #   if Nbkg[i] <= 0.1:
    #     continue
    #   if Nsig[i] <= 0.1:
    #     continue
      nh0 = np.random.poisson(max(0.0000000001,(Nbkg[i]+np.random.normal(0,errNbkg[i])) ) )
      nh1 = np.random.poisson(max(0.0000000001,Nsig[i]+(Nbkg[i]+np.random.normal(0,errNbkg[i])) ) )
      qobsh0 = qobsh0 + (Nsig[i] - nh0 * log(1+Nsig[i]/(Nbkg[i])))
      qobsh1 = qobsh1 + (Nsig[i] - nh1 * log(1+Nsig[i]/(Nbkg[i])))
    #print used_list
    if qobsh0 > qobs:
      clb = clb + 1.
    if qobsh1 > qobs:
      clsb = clsb + 1.
  # print clsb, qobsh1, clb, qobsh0
  print used
  if clb == 0:
    return 0.
  elif clb < clsb:
    return 1.
  else:
    return clsb/clb
