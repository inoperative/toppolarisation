#!/bin/bash

for i in ./30tev/graviton_dilep/ ./35tev/graviton_dilep/ ./40tev/graviton_dilep/ ./30tev/graviton_semil/ ./35tev/graviton_semil/ ./40tev/graviton_semil/ ./30tev/gluon_dilep/ ./35tev/gluon_dilep/ ./40tev/gluon_dilep/ ./30tev/gluon_semil/ ./35tev/gluon_semil/ ./40tev/gluon_semil/
do
	nohup python calculate_limit.py $i &
done
exit 0
