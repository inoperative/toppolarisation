import yoda
import matplotlib.pyplot as plt
import matplotlib
from scipy.interpolate import griddata
import numpy as np

matplotlib.rcParams.update({'font.size': 26})
matplotlib.rcParams['xtick.major.size'] = 6
matplotlib.rcParams['xtick.major.width'] = 1
matplotlib.rcParams['xtick.minor.size'] = 4
matplotlib.rcParams['xtick.minor.width'] = 1
matplotlib.rcParams['ytick.major.size'] = 6
matplotlib.rcParams['ytick.major.width'] = 1
matplotlib.rcParams['ytick.minor.size'] = 4
matplotlib.rcParams['ytick.minor.width'] = 1
matplotlib.rcParams['xtick.major.pad']='8'
matplotlib.rcParams['ytick.major.pad']='10'


signal_plots=yoda.read("c1.yoda", asdict=False)
background_plots=yoda.read("background.yoda", asdict=False)

signal_lepton_angle=signal_plots[4]
background_lepton_angle=background_plots[4]

# f_sig = open('lepton_angle_signal.dat', 'w')
# f_back = open('lepton_angle_background.dat', 'w')
#
# for b in signal_lepton_angle.bins:
#     f_sig.write(str(b.xMid)+ ' ' + str(b.yMid) + ' ' + str(b.height) + '\n')
#
# f_sig.close()
# f_back.close()

dat = np.genfromtxt('lepton_angle_signal.dat', delimiter=' ',skip_header=0)
X_dat = dat[:,0]
Y_dat = dat[:,1]
Z_dat = dat[:,2]

# Convert from pandas dataframes to numpy arrays
X, Y, Z, = np.array([]), np.array([]), np.array([])
for i in range(len(X_dat)):
        X = np.append(X,X_dat[i])
        Y = np.append(Y,Y_dat[i])
        Z = np.append(Z,Z_dat[i])

# create x-y points to be used in heatmap
xi = np.linspace(X.min(),X.max(),20)
yi = np.linspace(Y.min(),Y.max(),10)

# Z is a matrix of x-y values
zi = griddata((X, Y), Z, (xi[None,:], yi[:,None]), method='linear')

# I control the range of my colorbar by removing data
# outside of my range of interest

# Create the contour plot
# CS = plt.contourf(xi, yi, zi, 15, cmap=plt.cm.YlGnBu)
CS = plt.imshow(zi, cmap=plt.cm.YlGnBu)
plt.colorbar()
plt.savefig("test.pdf",dpi=100)
