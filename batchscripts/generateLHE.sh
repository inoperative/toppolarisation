#! /bin/bash

d=$1
mgdir=$2
nev=$3
source /afs/phas.gla.ac.uk/user/k/knordstrom/madgraph_top/setup
cd $mgdir

#    if [[ -f ${d}_dir/unweighted_events.lhe.gz ]] ; then
#	exit
#    fi
    mkdir -p ${d}_dir
    mkdir -p /tmp/knordstrom/${d}_mg/
    cp -r MadGraph5_v1_5_14_2hdm/ /tmp/knordstrom/${d}_mg/
    cp -f $d /tmp/knordstrom/${d}_mg/MadGraph5_v1_5_14_2hdm/2hdmc_run/Cards/param_card.dat
    cd /tmp/knordstrom/${d}_mg/MadGraph5_v1_5_14_2hdm/2hdmc_run
    sed -i 's/10000 = nevents/'$nev' = nevents/g' Cards/run_card.dat
    ./bin/generate_events -f
    cp -f Events/run_01/unweighted_events.lhe.gz $mgdir/${d}_dir/
    cd $mgdir
    rm -rf /tmp/knordstrom/${d}_mg/

exit
