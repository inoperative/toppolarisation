#! /bin/bash

mgdir=/afs/phas.gla.ac.uk/user/k/knordstrom/madgraph_top/
atomdir=/afs/phas.gla.ac.uk/user/k/knordstrom/atom/
nev=$1

#source /afs/phas.gla.ac.uk/user/k/knordstrom/madgraph_top/setup
cd $mgdir

for d in paramcards/*
do
if [[ ! -d $d ]]; then
     #echo $d
     qsub -v d=${d},mgdir=${mgdir},nev=${nev} ${atomdir}/rundir/submit.pbs
#    sh $atomdir/rundir/generateLHE.sh $d $mgdir $nev
#    sh $atomdir/rundir/showerLHE.sh $d $mgdir $nev
#    env -i sh $atomdir/rundir/atomRun.sh $d $mgdir
fi
done

