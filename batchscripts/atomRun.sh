#! /bin/bash

d=$1
mgdir=$2

HOME=/afs/phas.gla.ac.uk/user/k/knordstrom/
#source /afs/phas.gla.ac.uk/user/k/knordstrom/.bashrc
source /afs/phas.gla.ac.uk/user/k/knordstrom/atom/setup
ATOM_ANALYSIS_PATH=/tmp/knordstrom/${d}_atom

cd /tmp/knordstrom/${d}_atom/
cp -r /afs/phas.gla.ac.uk/user/k/knordstrom/atom/toppolarisation .
cd toppolarisation
make clean
make atom
cd ..

/bin/cat <<EOM > Atom.batch
add Analysis TopPolarisation
add InputFile ./herwig.hepmc
set IgnoreBeams on
set SaveHistos on
set DoSmearing on
set DoEfficiencies on
set DoTagging on
set DoFlagging on
launch
EOM

ls
	
atom -b Atom.batch

cp -f atom_results.* $mgdir/${d}_dir
cd $mgdir
rm -rf /tmp/knordstrom/${d}_atom

exit
