#! /bin/bash

d=$1
mgdir=$2
nev=$3
source /afs/phas.gla.ac.uk/user/k/knordstrom/atom/setup

cd $mgdir

    mkdir -p /tmp/knordstrom/${d}_shower/
    mkdir -p /tmp/knordstrom/${d}_atom/
    cp -f ${d}_dir/unweighted_events.lhe.gz /tmp/knordstrom/${d}_shower/
    cp -f herwig.in /tmp/knordstrom/${d}_shower/
    cd /tmp/knordstrom/${d}_shower/
    gunzip unweighted_events.lhe.gz
    sed -i 's/set theGenerator:NumberOfEvents 1000000/set theGenerator:NumberOfEvents '$nev'/g' herwig.in
    Herwig++ read herwig.in
    Herwig++ run herwig.run
    mv -f herwig.hepmc /tmp/knordstrom/${d}_atom/
    cd $mgdir
    rm -rf /tmp/knordstrom/${d}_shower/
