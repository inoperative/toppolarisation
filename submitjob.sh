#!/bin/bash

dir=/afs/phas.gla.ac.uk/user/m/mrussell/minimization
cd $dir


for d in grid/*
do
    
    cd $d 
    
    qsub -q medium6 prof2ipol-chi2 -F "/afs/phas.gla.ac.uk/user/m/mrussell/minimization/yoda_inputs/all_totxsecs --ifile /afs/phas.gla.ac.uk/user/m/mrussell/minimization/yoda_inputs/all_totxsecs/ipol-5.dat --cpair 0,2 --npix=21  --nscan=11"

    cd ../../
    
done