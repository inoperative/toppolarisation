#include "SonnenscheinEngine.h"

#include <iostream>
#include <vector>
#include <utility>

#include "TLorentzVector.h"

int main() {
    std::cout << "hello world\n";

    top::SonnenscheinEngine sonn;

    TLorentzVector b(27.9652, 94.1283, -37.7801, 105.321);
    TLorentzVector lp(14.7347, 21.9598, -8.8204, 27.8775);
    TLorentzVector nu(-28.2752, -54.4052, -20.9865, 64.8062);

    TLorentzVector bbar(33.7778, 35.977, 58.007, 76.3095);
    TLorentzVector lm(-15.1858, -97.1134, 137.118, 168.71);
    TLorentzVector nubar(-33.7231, 0.273651, 82.1863, 88.8364);

    const double met_ex = nu.Px() + nubar.Px();
    const double met_ey = nu.Py() + nubar.Py();

    //solve
    std::vector<std::pair<TLorentzVector, TLorentzVector> > allSolutions = sonn.solve(lp, b, 175., 80.41, lm, bbar, 175., 80.41, met_ex, met_ey);

    std::cout << "Solutions\n";
    unsigned int counter = 0;
    for (const std::pair<TLorentzVector, TLorentzVector>& possible : allSolutions) {
        std::cout << "Possible solution number " << counter << "\n";
        std::cout << "    first neutrino : " << possible.first << "\n";
        std::cout << "    second neutrino: " << possible.second << "\n";
        ++counter;
    }

    return 0;
}
