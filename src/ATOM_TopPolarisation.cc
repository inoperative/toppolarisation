// -*- C++ -*-
#include "Atom/Analysis.hh"
#include "Atom/Tools/CommonProjections.hh"

#include "TLorentzVector.h"
#include "TVector3.h"
#include "TVector2.h"
#include "MatrixElementAnalysis.h"
#include "lester_mt2_bisect.h"
#include <random>

namespace Atom {

  bool isBMeson(int pdgId){
    if( fabs(pdgId)==511 || fabs(pdgId)==531 || fabs(pdgId)==521
             || fabs(pdgId)==5122 || fabs(pdgId)==5232 ||  fabs(pdgId)==5132 || fabs(pdgId)==5332
             || fabs(pdgId)==5322 || fabs(pdgId)==5312 || fabs(pdgId)==533 || fabs(pdgId)==513
             || fabs(pdgId)==523  || fabs(pdgId)==541 || fabs(pdgId)==543
             || fabs(pdgId)==5114 || fabs(pdgId)==5212 || fabs(pdgId)==5224 || fabs(pdgId)==5222
             || fabs(pdgId)==5214 || fabs(pdgId)==5112 || fabs(pdgId)==5324 || fabs(pdgId)==5314
             || fabs(pdgId)==5334 )
      return true;
    return false;
  }

  typedef std::map<std::string, Histo1DPtr> HistoMap;

  double _transMass(double ptLep, double phiLep, double met, double phiMet) {
    return sqrt(2.0*ptLep*met*(1 - cos(phiLep-phiMet)));
  }

  HepMC::GenVertex::particle_iterator findLepton(HepMC::GenVertex::particle_iterator p, int id, FourMomentum *ret) {
    if( !(*p)->end_vertex() ) return p;
    for ( HepMC::GenVertex::particle_iterator mother = (*p)->end_vertex()-> particles_begin(HepMC::family);
                      mother != (*p)->end_vertex()-> particles_end(HepMC::family); ++mother ) {
//      cout << (*mother)->pdg_id() << endl;
      if((*mother)->pdg_id() == id) {
        FourMomentum tmp = FourMomentum((*mother)->momentum().e(),(*mother)->momentum().px(), (*mother)->momentum().py(), (*mother)->momentum().pz());
        *ret = tmp;
        return mother;
      }
      else if ((*mother) != (*p)) findLepton(mother, id, ret);
    }
    return p;
  }

  double KZ1(double Ep, double kxp, double kyp, double kzp, double mt, double kx, double ky) {
//    cout << "in KZ1" << endl;
    return (4*pow(Ep,2)*kzp - 8*kx*kxp*kzp -
      4*pow(kxp,2)*kzp - 8*ky*kyp*kzp - 4*pow(kyp,2)*kzp -
        4*pow(kzp,3) - 4*kzp*pow(mt,2) -
        sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
            4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
            4*pow(kyp,2)*kzp + 4*pow(kzp,3) + 4*kzp*pow(mt,2),
           2) - 4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
           (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
             4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
             4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
             pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
             4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
             4*pow(kxp,2)*ky*kyp - 2*pow(Ep,2)*pow(kyp,2) +
             4*kx*kxp*pow(kyp,2) + 2*pow(kxp,2)*pow(kyp,2) +
             4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
             pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
             4*kx*kxp*pow(kzp,2) + 2*pow(kxp,2)*pow(kzp,2) +
             4*ky*kyp*pow(kzp,2) + 2*pow(kyp,2)*pow(kzp,2) +
             pow(kzp,4) - 2*pow(Ep,2)*pow(mt,2) +
             4*kx*kxp*pow(mt,2) + 2*pow(kxp,2)*pow(mt,2) +
             4*ky*kyp*pow(mt,2) + 2*pow(kyp,2)*pow(mt,2) +
             2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
      (2.*(-4*pow(Ep,2) + 4*pow(kzp,2)));
  }

  double KZ2(double Ep, double kxp, double kyp, double kzp, double mt, double kx, double ky) {
//    cout << "in KZ2" << endl;
    return (4*pow(Ep,2)*kzp - 8*kx*kxp*kzp -
        4*pow(kxp,2)*kzp - 8*ky*kyp*kzp - 4*pow(kyp,2)*kzp -
        4*pow(kzp,3) - 4*kzp*pow(mt,2) +
        sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
            4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
            4*pow(kyp,2)*kzp + 4*pow(kzp,3) + 4*kzp*pow(mt,2),
           2) - 4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
           (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
             4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
             4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
             pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
             4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
             4*pow(kxp,2)*ky*kyp - 2*pow(Ep,2)*pow(kyp,2) +
             4*kx*kxp*pow(kyp,2) + 2*pow(kxp,2)*pow(kyp,2) +
             4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
             pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
             4*kx*kxp*pow(kzp,2) + 2*pow(kxp,2)*pow(kzp,2) +
             4*ky*kyp*pow(kzp,2) + 2*pow(kyp,2)*pow(kzp,2) +
             pow(kzp,4) - 2*pow(Ep,2)*pow(mt,2) +
             4*kx*kxp*pow(mt,2) + 2*pow(kxp,2)*pow(mt,2) +
             4*ky*kyp*pow(mt,2) + 2*pow(kyp,2)*pow(mt,2) +
             2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
      (2.*(-4*pow(Ep,2) + 4*pow(kzp,2)));
  }

  double ee1(double Ep, double kxp, double kyp, double kzp, double mt, double kx, double ky) {
//    cout << "in ee1" << endl;
    return (-pow(Ep,2) + 2*kx*kxp + pow(kxp,2) + 2*ky*kyp +
        pow(kyp,2) + pow(kzp,2) +
        (4*pow(Ep,2)*pow(kzp,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (8*kx*kxp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (4*pow(kxp,2)*pow(kzp,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (8*ky*kyp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (4*pow(kyp,2)*pow(kzp,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (4*pow(kzp,4))/(-4*pow(Ep,2) + 4*pow(kzp,2)) +
        pow(mt,2) - (4*pow(kzp,2)*pow(mt,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (kzp*sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
               4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
               4*pow(kyp,2)*kzp + 4*pow(kzp,3) +
               4*kzp*pow(mt,2),2) -
             4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
              (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
                4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
                4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
                pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
                4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
                4*pow(kxp,2)*ky*kyp -
                2*pow(Ep,2)*pow(kyp,2) +
                4*kx*kxp*pow(kyp,2) +
                2*pow(kxp,2)*pow(kyp,2) +
                4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
                pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
                4*kx*kxp*pow(kzp,2) +
                2*pow(kxp,2)*pow(kzp,2) +
                4*ky*kyp*pow(kzp,2) +
                2*pow(kyp,2)*pow(kzp,2) + pow(kzp,4) -
                2*pow(Ep,2)*pow(mt,2) + 4*kx*kxp*pow(mt,2) +
                2*pow(kxp,2)*pow(mt,2) + 4*ky*kyp*pow(mt,2) +
                2*pow(kyp,2)*pow(mt,2) +
                2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)))/(2.*Ep);
  }

  double ee2(double Ep, double kxp, double kyp, double kzp, double mt, double kx, double ky) {
//    cout << "in ee2" << endl;
    return (-pow(Ep,2) + 2*kx*kxp + pow(kxp,2) + 2*ky*kyp +
        pow(kyp,2) + pow(kzp,2) +
        (4*pow(Ep,2)*pow(kzp,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (8*kx*kxp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (4*pow(kxp,2)*pow(kzp,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (8*ky*kyp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (4*pow(kyp,2)*pow(kzp,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (4*pow(kzp,4))/(-4*pow(Ep,2) + 4*pow(kzp,2)) +
        pow(mt,2) - (4*pow(kzp,2)*pow(mt,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) +
        (kzp*sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
               4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
               4*pow(kyp,2)*kzp + 4*pow(kzp,3) +
               4*kzp*pow(mt,2),2) -
             4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
              (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
                4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
                4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
                pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
                4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
                4*pow(kxp,2)*ky*kyp -
                2*pow(Ep,2)*pow(kyp,2) +
                4*kx*kxp*pow(kyp,2) +
                2*pow(kxp,2)*pow(kyp,2) +
                4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
                pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
                4*kx*kxp*pow(kzp,2) +
                2*pow(kxp,2)*pow(kzp,2) +
                4*ky*kyp*pow(kzp,2) +
                2*pow(kyp,2)*pow(kzp,2) + pow(kzp,4) -
                2*pow(Ep,2)*pow(mt,2) + 4*kx*kxp*pow(mt,2) +
                2*pow(kxp,2)*pow(mt,2) + 4*ky*kyp*pow(mt,2) +
                2*pow(kyp,2)*pow(mt,2) +
                2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)))/(2.*Ep);
  }

  vector<double> BaumgartTweedie(TLorentzVector t, TLorentzVector tbar, TLorentzVector lp, TLorentzVector lm) {
      const double pt = t.Pt() + tbar.Pt();

      //boost to com
      TLorentzVector ttbar = t + tbar;
      TVector3 boost_to_ttbar = -1. * ttbar.BoostVector();

      t.Boost(boost_to_ttbar);
      tbar.Boost(boost_to_ttbar);
      lp.Boost(boost_to_ttbar);
      lm.Boost(boost_to_ttbar);

      //boost from ZMF to top frame
      TVector3 boost_to_top = -1. * t.BoostVector();
      lp.Boost(boost_to_top);

      //boost from ZMF to tbar frame
      TVector3 boost_to_tbar = -1. * tbar.BoostVector();
      lm.Boost(boost_to_tbar);

      TVector3 beamZ(0, 0, 1);

      TVector3 newZ = t.Vect().Unit();
      TVector3 newY = t.Vect().Unit().Cross(beamZ).Unit();
      TVector3 newX = newY.Cross(newZ).Unit();

      TRotation m;
      m.RotateAxes(newX, newY, newZ);
      m = m.Inverse();
      lp.Transform(m);
      lm.Transform(m);

      vector<double> ret;

      ret.push_back(TVector2::Phi_mpi_pi(lp.Phi() - lm.Phi()));
      ret.push_back(TVector2::Phi_mpi_pi(lp.Phi() + lm.Phi()));

      return ret;
  }


    class ATOM_TopPolarisation : public Analysis {
    public:

        /// @name Constructors etc.
        //@{

        /// Constructor
        ATOM_TopPolarisation()
            : Analysis("ATOM_TopPolarisation") {
            /// @todo Set whether your finalize method needs the generator cross section
            setNeedsCrossSection(true);
        }

        //@}

    public:

        /// @name Analysis methods
        //@{

        /// Book histograms and initialise projections before the run
        void init() {
            evno = 0;
            useDetector( "ATLAS_CMS_all" );

            FinalState fsbase( getRange( "Full_Range_ATLAS" ) );

            IsoElectron base_ele( Range(PT, 25., 14000.) & Range(ETA, -2.47, 2.47) );
            base_ele.addIso(TRACK_ISO_PT, 0.01,  1.0,  0.0, 0.01, CALO_ALL);
            base_ele.setSmearingParams  ( getElectronSim( "Electron_Smear_run1_ATLAS" ) );
            base_ele.setEfficiencyParams( getElectronEff( "Electron_Ident_Loose_2012_ATLAS" ) );

            IsoElectron ele( Range(PT, 25., 14000.) & Range(ETA, -2.47, 2.47) );
            //                         cone  frac  abs  inner
            ele.addIso(TRACK_ISO_PT, 0.3,  0.16,  0.0, 0.01, CALO_ALL);
            ele.addIso(CALO_ISO_ET,  0.3,  0.18,  0.0, 0.01, CALO_ALL);
            ele.setVariableThreshold(0.0);
            ele.setSmearingParams  ( getElectronSim( "Electron_Smear_run1_ATLAS" ) );
            ele.setEfficiencyParams( getElectronEff( "Electron_Ident_Tight_2012_ATLAS" ) );

            IsoMuon base_mu(Range(PT, 25., 14000.) & Range(ETA, -2.4, 2.4));
            base_mu.addIso(TRACK_ISO_PT, 0.01,  1.0,  0.0, 0.01, CALO_ALL);
            base_mu.setSmearingParams  ( getMuonSim( "Muon_Smear_ID-MS_ATLAS" ) );
            base_mu.setEfficiencyParams( getMuonEff( "Muon_Ident_CB-ST_ATLAS" ) );

            IsoMuon mu( Range(PT, 25., 14000.) & Range(ETA, -2.4, 2.4) );
            //                         cone  frac  abs  inner
            mu.addIso(TRACK_ISO_PT, 0.3,  0.12,  0.0, 0.01, CALO_ALL);
            mu.addIso(CALO_ISO_ET,  0.3,  0.12,  0.0, 0.01, CALO_ALL);
            mu.setVariableThreshold(0.0);
            mu.setSmearingParams  ( getMuonSim( "Muon_Smear_ID-MS_ATLAS" ) );
            mu.setEfficiencyParams( getMuonEff( "Muon_Ident_CB-ST_ATLAS" ) );

            Range muDetRange = getRange( "Muon_Range_Detector_ATLAS" );
            Range hadRange   = getRange( "HCal_Range_ATLAS" );
            FastJets base_jets(fsbase,
                            hadRange & Range(PT, 20., 14000.) & Range(ETA, -4.5, 4.5),
                            muDetRange, FastJets::ANTIKT, 0.4 );
            base_jets.setSmearingParams( getJetSim( "Jet_Smear_Topo_ATLAS" ) );
            base_jets.setEfficiencyParams( getJetEff( "Jet_Ident_PlaceHolder" ) );

            // Overlap removal
            NearIsoParticle base_jets_clean(base_jets);
            base_jets_clean.addFilter(base_ele, 0.2);

            MergedFinalState base_leptons(base_ele, base_mu);
            NearIsoParticle base_leptons_clean(base_leptons);
            base_leptons_clean.addFilter(base_jets_clean, 0.4);
            addProjection(base_leptons_clean, "Leptons");

            MergedFinalState leptons(ele, mu);
            NearIsoParticle leptons_clean(leptons);
            leptons_clean.addFilter(base_jets_clean, 0.4);
            addProjection(leptons_clean, "CR_Leptons");

            NearIsoParticle jets_clean(base_jets_clean, Range(PT, 25., 14000.) & Range(ETA, -2.8, 2.8));
            addProjection(jets_clean, "Jets");

            Range bjrange = Range(PT, 25.0, 14000.0) & Range(ETA, -2.5, 2.5);
            HeavyFlavorJets bjets(jets_clean, bjrange);
            bjets.setTaggingEfficiency( *getBJetEff("BJet_Ident_MV1_ATLAS") );
            bjets.setCurrentWorkingPoint( 0.7 );
            addProjection(bjets, "BJets");

            // SmearingParams& metsmear = metSim("Smear_MissingET_ATLAS");
            // FastSimParameterization metsim = createFastSimParam(metsmear);
//            MergedFinalState met_seed(jets_clean, base_leptons_clean);
//            MissingMomentum met( fsbase, met_seed );
            MissingMomentum met( fsbase );
            //met.setSmearingParams( FinalState::SELECTED, &dp.metEff( "Jet_PlaceHolder" ) );
            addProjection(met, "MissingEt");

            IdentifiedFinalState nu_id;
            nu_id.acceptNeutrinos();
            addProjection(nu_id, "neutrinos");


            ChargedFinalState tracks = ChargedFinalState(Range(PT, 0.5, 14000.) & Range(ETA, -2.5, 2.5));
            addProjection(tracks, "Tracks");

            /// @todo Book histograms here, e.g.:
            _histos["DPHI_LPLM"]   = bookHisto1D("dphi_lplm", 20, 0, M_PI, "", "dphi(lp lm)", "Relative Occurence");
            _histos["COSTHETACOSTHETA"]   = bookHisto1D("costhetacostheta", 20, -1, 1, "", "cos theta+ cos theta-", "Relative Occurence");
            _histos["COSTHETA1"]   = bookHisto1D("costheta1", 20, -1, 1, "", "cos theta+", "Relative Occurence");
            _histos["COSTHETA2"]   = bookHisto1D("costheta2", 20, -1, 1, "", "cos theta-", "Relative Occurence");
            _histos["XL1"]   = bookHisto1D("xl1", 20, 0, 2, "", "xl+", "Relative Occurence");
            _histos["XL2"]   = bookHisto1D("xl2", 20, 0, 2, "", "xl-", "Relative Occurence");
            _histos["UL1"]   = bookHisto1D("ul1", 20, 0, 1, "", "ul+", "Relative Occurence");
            _histos["UL2"]   = bookHisto1D("ul2", 20, 0, 1, "", "ul-", "Relative Occurence");
            _histos["COSTHETACOSTHETA_UWER"]   = bookHisto1D("costhetacostheta_uwer", 20, -1, 1, "", "cos theta+ cos theta- Uwer basis", "Relative Occurence");
            _histos["BT_DIFF"]   = bookHisto1D("bt_diff", 20, -M_PI, M_PI, "", "dphi BT", "Relative Occurence");
            _histos["BT_SUM"]   = bookHisto1D("bt_sum", 20, -M_PI, M_PI, "", "sum phi BT", "Relative Occurence");
            _histos["LEADINGJ_PT"]   = bookHisto1D("leadingjetpt", 20, 0, 300, "", "leading jet pt", "Relative Occurence");
            _histos["LEADINGT_MASS"]   = bookHisto1D("top1mass", 20, 0, 300, "", "top1 mass", "Relative Occurence");
            _histos["SUBLEADINGT_MASS"]   = bookHisto1D("top2mass", 20, 0, 300, "", "top2 mass", "Relative Occurence");
            _histos["WP_MASS"]   = bookHisto1D("w+mass", 20, 0, 200, "", "w+ mass", "Relative Occurence");
            _histos["WM_MASS"]   = bookHisto1D("w-mass", 20, 0, 200, "", "w- mass", "Relative Occurence");
            _histos["MT2bef"]   = bookHisto1D("mt2_bef", 20, 0, 200, "", "mt2 before cut", "Relative Occurence");
            _histos["b3l+_dphi"]   = bookHisto1D("b3l+_dphi", 20, 0, M_PI, "", "dphi(l+,b3)", "Relative Occurence");
            _histos["b3l-_dphi"]   = bookHisto1D("b3l-_dphi", 20, 0, M_PI, "", "dphi(l-,b3)", "Relative Occurence");

            outFile.open("atom_results.data");

            totalweight = 0.;
            totalweightuptobj = 0.;
            passweight = 0.;
            correct_pair = 0.;
            pass_to_pair = 0.;

        }


        /// Perform the per-event analysis
        void analyze(const Event& event) {

            evno++;

            const HepMC::GenEvent *evt = event.genEvent();

            vector<FourMomentum> blp, blm;
            FourMomentum hard1, hard2;
            vector<FourMomentum> bMesons,bMestmp;


            HepMC::GenEvent::particle_const_iterator e = evt->particles_end();
            for ( HepMC::GenEvent::particle_const_iterator p = evt->particles_begin(); p != e; ++p )
            {
              if ( isBMeson( (*p)->pdg_id() ) && (*p)->status() == 1  )
                bMestmp.push_back(FourMomentum((*p)->momentum().e(),(*p)->momentum().px(), (*p)->momentum().py(), (*p)->momentum().pz()));
              if ( (*p)->pdg_id()==6) {
                if ( (*p)->end_vertex() )
                {
                  for ( HepMC::GenVertex::particle_iterator mother = (*p)->end_vertex()-> particles_begin(HepMC::family);
                        mother != (*p)->end_vertex()-> particles_end(HepMC::family); ++mother )
                  {
                    if ((*mother)->pdg_id() == 5) { blp.push_back(FourMomentum((*mother)->momentum().e(),(*mother)->momentum().px(), (*mother)->momentum().py(), (*mother)->momentum().pz()));  break;}
                  }
                  for ( HepMC::GenVertex::particle_iterator mother = (*p)->end_vertex()-> particles_begin(HepMC::family);
                        mother != (*p)->end_vertex()-> particles_end(HepMC::family); ++mother )
                  {
                    if ( (*mother)->pdg_id() == 24 && (*mother)->end_vertex() ) {
                      FourMomentum lepton1(0.,0.,0.,0.);
                      FourMomentum lepton2(0.,0.,0.,0.);
                      HepMC::GenVertex::particle_iterator lep1, lep2;
                      lep1 = findLepton(mother, -11, &lepton1);
                      lep2 = findLepton(mother, 12, &lepton2);
                      if(lepton1.pT() < 0.0001 && lepton2.pT() < 0.0001) {
                        lep1 = findLepton(mother, -13, &lepton1);
                        lep2 = findLepton(mother, 14, &lepton2);
                      }
                      if(lepton1.pT() > 0.0001 && lepton2.pT() > 0.0001) {
                        blp.push_back(lepton1);
                        blp.push_back(lepton2);
                      }
                      break;
                    }
                  }
                }
              }
              if ( (*p)->pdg_id()==-6) {
                if ( (*p)->end_vertex() )
                {
                  for ( HepMC::GenVertex::particle_iterator mother = (*p)->end_vertex()-> particles_begin(HepMC::family);
                        mother != (*p)->end_vertex()-> particles_end(HepMC::family); ++mother )
                  {
                    if ((*mother)->pdg_id() == -5) { blm.push_back(FourMomentum((*mother)->momentum().e(),(*mother)->momentum().px(), (*mother)->momentum().py(), (*mother)->momentum().pz()));  break;}
                  }
                  for ( HepMC::GenVertex::particle_iterator mother = (*p)->end_vertex()-> particles_begin(HepMC::family);
                        mother != (*p)->end_vertex()-> particles_end(HepMC::family); ++mother )
                  {
                    if ( (*mother)->pdg_id() == -24 && (*mother)->end_vertex() ) {
                      FourMomentum lepton1(0.,0.,0.,0.);
                      FourMomentum lepton2(0.,0.,0.,0.);
                      HepMC::GenVertex::particle_iterator lep1, lep2;
                      lep1 = findLepton(mother, 11, &lepton1);
                      lep2 = findLepton(mother, -12, &lepton2);
                      if(lepton1.pT() < 0.0001 && lepton2.pT() < 0.0001) {
                        lep1 = findLepton(mother, 13, &lepton1);
                        lep2 = findLepton(mother, -14, &lepton2);
                      }
                      if(lepton1.pT() > 0.0001 && lepton2.pT() > 0.0001) {
                        blm.push_back(lepton1);
                        blm.push_back(lepton2);
                      }
                      break;
                    }
                  }
                }
              }
            }

            for(int i = 0; i < bMestmp.size(); ++i) {
              if ( bMestmp[i].pT() > 10. ) bMesons.push_back(bMestmp[i]);
            }

//            cout << bMesons.size() << endl;

            const Particles jets = applyProjection<NearIsoParticle>(event, "Jets").particlesByPt();
            const Particles lepts = applyProjection<NearIsoParticle>(event, "Leptons").particlesByPt();
//            const HeavyFlavorJets& bjproj = applyProjection<HeavyFlavorJets>(event, "BJets");
//            const Particles& bjets = bjproj.getTaggedJets(&event);
            const Particles neutrinos = applyProjection<IdentifiedFinalState>(event, "neutrinos").particlesByPt();
            const MissingMomentum& pmet = applyProjection<MissingMomentum>(event, "MissingEt");
            FourMomentum met = pmet.missingEt();
            FourMomentum metfm = -pmet.visibleMomentum();

            Particles jets_25;
            for (unsigned int i = 0; i < jets.size(); i++) {
              if ( fabs(jets[i].momentum().eta()) < 2.5 )
                jets_25.push_back(jets[i]);
            }

            std::random_device rd;
            std::mt19937 generator (rd());
            normal_distribution<double> distribution(met.pT(),(2.92/met.pT() - 0.07) * met.pT());
            double scale = distribution(generator)/met.pT();
            met *= scale;

            std::uniform_real_distribution<double> dis(0.0, 1.0);

            for(unsigned int ev = 0; ev < 100; ++ev) {

              int num_bjets = 3;

              totalweight += event.weight();
              double weight = event.weight();
              _top1c.clear();
              _top2c.clear();
              Particles bjets;

              if ( jets_25.size() < num_bjets ) continue;

              if (lepts.size() != 2)
                  continue;

              // two OS leptons
              if (lepts[0].pdgId()*lepts[1].pdgId() > 0)
                  continue;

              // veto on Z mass
              if ( abs( (lepts[0].momentum()+lepts[1].momentum()).mass() - 91.19 ) < 10.)
                  continue;

              for (unsigned int i = 0; i < lepts.size(); ++i) {
                if ( abs(lepts[i].pdgId()) == 11 ) {
                  weight *= 0.85;
                  _ev_weight *= 0.85;
                }
              }

              bool met_overlap = false;

              for (unsigned int t = 0; t < jets.size(); ++t)
                if (deltaR(jets[t].momentum(),metfm) < 0.4)
                  met_overlap = true;

              if (met_overlap) continue;

              if (met.pT() < 60.)
                  continue;

//              for (unsigned int i = 0; i < jets_25.size(); i++) {
//                const Particle& jet = jets_25[i];
                // Count the number of b-tags

//                double randomRealBetweenZeroAndOne = dis(generator);
//                bool btag = false;
//                if ( deltaR(blp[0], jet) < 0.4 || deltaR(blm[0], jet) ) btag = true;
//                if (jet.fromBottom() && randomRealBetweenZeroAndOne < 0.7)
//                  bjets.push_back(jet);
//                if (!jet.fromBottom() && randomRealBetweenZeroAndOne < 0.01)
//                  bjets.push_back(jet);
//                if (btag && randomRealBetweenZeroAndOne < 0.7)
//                  bjets.push_back(jet);
//                if (!btag && randomRealBetweenZeroAndOne < 0.01)
//                  bjets.push_back(jet);
  //              if (jet.fromBottom())
  //                bjets.push_back(jet);
//              }
              vector<int> used;

              for(int j = 0; j < bMesons.size(); ++j) {
                double mindR = 1000.;
                unsigned int ind = -1;
                for (unsigned int i = 0; i < jets_25.size(); i++) {
                  if( deltaR(jets_25[i], bMesons[j]) < mindR ) {
                    mindR = deltaR(jets_25[i], bMesons[j]);
                    ind = i;
                  }
                }
                if (mindR < 0.4) {
                  if(std::find(used.begin(), used.end(), ind) != used.end()) {
                    continue;
                  } else {
                    used.push_back(ind);
                  }
                }
              }

              for (unsigned int i = 0; i < jets_25.size(); i++) {
                double rand = dis(generator);
                if(std::find(used.begin(), used.end(), i) != used.end()) {
                  if(rand < 0.7) bjets.push_back(jets_25[i]);
                }
                else if (rand < 0.01) bjets.push_back(jets_25[i]);
              }

              totalweightuptobj += 1;
              nbj += bjets.size();

              if (bjets.size() < num_bjets)
                  continue;

//              if ( bjets[2].momentum().pT() < 50. )
//                continue;

              bool truth = false;
              double desiredPrecisionOnMt2 = 0.0001; // Must be >=0.  If 0 alg aims for machine precision.  if >0, MT2 computed to supplied absolute precision.
              Particle bjet3;

              if (num_bjets == 3) {

                double mVisA1 = (lepts[0].momentum()+bjets[0].momentum()).mass(); // mass of visible object on side A.  Must be >=0.
                double pxA1 = (lepts[0].momentum()+bjets[0].momentum()).px(); // x momentum of visible object on side A.
                double pyA1 = (lepts[0].momentum()+bjets[0].momentum()).py(); // y momentum of visible object on side A.

                double mVisB1 = (lepts[1].momentum()+bjets[1].momentum()).mass(); // mass of visible object on side B.  Must be >=0.
                double pxB1 = (lepts[1].momentum()+bjets[1].momentum()).px(); // x momentum of visible object on side B.
                double pyB1 = (lepts[1].momentum()+bjets[1].momentum()).py(); // y momentum of visible object on side B.

                double pxMiss1 = met.px(); // x component of missing transverse momentum.
                double pyMiss1 = met.py(); // y component of missing transverse momentum.

                double chiA = 0.; // hypothesised mass of invisible on side A.  Must be >=0.
                double chiB = 0.; // hypothesised mass of invisible on side B.  Must be >=0.

                 asymm_mt2_lester_bisect::disableCopyrightMessage();

                double MT2_1 =  asymm_mt2_lester_bisect::get_mT2(
                           mVisA1, pxA1, pyA1,
                           mVisB1, pxB1, pyB1,
                           pxMiss1, pyMiss1,
                           chiA, chiB,
                           desiredPrecisionOnMt2);

                double mVisA2 = (lepts[0].momentum()+bjets[1].momentum()).mass(); // mass of visible object on side A.  Must be >=0.
                double pxA2 = (lepts[0].momentum()+bjets[1].momentum()).px(); // x momentum of visible object on side A.
                double pyA2 = (lepts[0].momentum()+bjets[1].momentum()).py(); // y momentum of visible object on side A.

                double mVisB2 = (lepts[1].momentum()+bjets[0].momentum()).mass(); // mass of visible object on side B.  Must be >=0.
                double pxB2 = (lepts[1].momentum()+bjets[0].momentum()).px(); // x momentum of visible object on side B.
                double pyB2 = (lepts[1].momentum()+bjets[0].momentum()).py(); // y momentum of visible object on side B.

                double pxMiss2 = met.px(); // x component of missing transverse momentum.
                double pyMiss2 = met.py(); // y component of missing transverse momentum.

                double MT2_2 =  asymm_mt2_lester_bisect::get_mT2(
                           mVisA2, pxA2, pyA2,
                           mVisB2, pxB2, pyB2,
                           pxMiss2, pyMiss2,
                           chiA, chiB,
                           desiredPrecisionOnMt2);

                double mVisA3 = (lepts[0].momentum()+bjets[2].momentum()).mass(); // mass of visible object on side A.  Must be >=0.
                double pxA3 = (lepts[0].momentum()+bjets[2].momentum()).px(); // x momentum of visible object on side A.
                double pyA3 = (lepts[0].momentum()+bjets[2].momentum()).py(); // y momentum of visible object on side A.

                double mVisB3 = (lepts[1].momentum()+bjets[0].momentum()).mass(); // mass of visible object on side B.  Must be >=0.
                double pxB3 = (lepts[1].momentum()+bjets[0].momentum()).px(); // x momentum of visible object on side B.
                double pyB3 = (lepts[1].momentum()+bjets[0].momentum()).py(); // y momentum of visible object on side B.

                double pxMiss3 = met.px(); // x component of missing transverse momentum.
                double pyMiss3 = met.py(); // y component of missing transverse momentum.

                double MT2_3 =  asymm_mt2_lester_bisect::get_mT2(
                           mVisA2, pxA2, pyA2,
                           mVisB2, pxB2, pyB2,
                           pxMiss2, pyMiss2,
                           chiA, chiB,
                           desiredPrecisionOnMt2);

                double mVisA4 = (lepts[0].momentum()+bjets[2].momentum()).mass(); // mass of visible object on side A.  Must be >=0.
                double pxA4 = (lepts[0].momentum()+bjets[2].momentum()).px(); // x momentum of visible object on side A.
                double pyA4 = (lepts[0].momentum()+bjets[2].momentum()).py(); // y momentum of visible object on side A.

                double mVisB4 = (lepts[1].momentum()+bjets[1].momentum()).mass(); // mass of visible object on side B.  Must be >=0.
                double pxB4 = (lepts[1].momentum()+bjets[1].momentum()).px(); // x momentum of visible object on side B.
                double pyB4 = (lepts[1].momentum()+bjets[1].momentum()).py(); // y momentum of visible object on side B.

                double pxMiss4 = met.px(); // x component of missing transverse momentum.
                double pyMiss4 = met.py(); // y component of missing transverse momentum.

                double MT2_4 =  asymm_mt2_lester_bisect::get_mT2(
                           mVisA2, pxA2, pyA2,
                           mVisB2, pxB2, pyB2,
                           pxMiss2, pyMiss2,
                           chiA, chiB,
                           desiredPrecisionOnMt2);

                double mVisA5 = (lepts[0].momentum()+bjets[0].momentum()).mass(); // mass of visible object on side A.  Must be >=0.
                double pxA5 = (lepts[0].momentum()+bjets[0].momentum()).px(); // x momentum of visible object on side A.
                double pyA5 = (lepts[0].momentum()+bjets[0].momentum()).py(); // y momentum of visible object on side A.

                double mVisB5 = (lepts[1].momentum()+bjets[2].momentum()).mass(); // mass of visible object on side B.  Must be >=0.
                double pxB5 = (lepts[1].momentum()+bjets[2].momentum()).px(); // x momentum of visible object on side B.
                double pyB5 = (lepts[1].momentum()+bjets[2].momentum()).py(); // y momentum of visible object on side B.

                double pxMiss5 = met.px(); // x component of missing transverse momentum.
                double pyMiss5 = met.py(); // y component of missing transverse momentum.

                double MT2_5 =  asymm_mt2_lester_bisect::get_mT2(
                           mVisA2, pxA2, pyA2,
                           mVisB2, pxB2, pyB2,
                           pxMiss2, pyMiss2,
                           chiA, chiB,
                           desiredPrecisionOnMt2);

                double mVisA6 = (lepts[0].momentum()+bjets[1].momentum()).mass(); // mass of visible object on side A.  Must be >=0.
                double pxA6 = (lepts[0].momentum()+bjets[1].momentum()).px(); // x momentum of visible object on side A.
                double pyA6 = (lepts[0].momentum()+bjets[1].momentum()).py(); // y momentum of visible object on side A.

                double mVisB6 = (lepts[1].momentum()+bjets[2].momentum()).mass(); // mass of visible object on side B.  Must be >=0.
                double pxB6 = (lepts[1].momentum()+bjets[2].momentum()).px(); // x momentum of visible object on side B.
                double pyB6 = (lepts[1].momentum()+bjets[2].momentum()).py(); // y momentum of visible object on side B.

                double pxMiss6 = met.px(); // x component of missing transverse momentum.
                double pyMiss6 = met.py(); // y component of missing transverse momentum.

                double MT2_6 =  asymm_mt2_lester_bisect::get_mT2(
                           mVisA2, pxA2, pyA2,
                           mVisB2, pxB2, pyB2,
                           pxMiss2, pyMiss2,
                           chiA, chiB,
                           desiredPrecisionOnMt2);


                vector<double> tmp;
                tmp.push_back(MT2_1);
                tmp.push_back(MT2_2);
                tmp.push_back(MT2_3);
                tmp.push_back(MT2_4);
                tmp.push_back(MT2_5);
                tmp.push_back(MT2_6);
                sort(tmp.begin(),tmp.end());

                bool usefirst = false;
                bool usesecond = false;
                bool usethird = false;
                bool usefourth = false;
                bool usefifth = false;
                bool usesixth = false;

                if ( tmp[0] == MT2_1 )
                  usefirst = true;
                else if( tmp[0] == MT2_2 )
                  usesecond = true;
                else if( tmp[0] == MT2_3 )
                  usethird = true;
                else if( tmp[0] == MT2_4 )
                  usefourth = true;
                else if( tmp[0] == MT2_5 )
                  usefifth = true;
                else if( tmp[0] == MT2_6 )
                  usesixth = true;

                if ( (lepts[0].momentum() + bjets[0].momentum()).mass() > 153.2 || (lepts[1].momentum() + bjets[1].momentum()).mass() > 153.2 || MT2_1 > 172.5 )
                  usefirst = false;

                if ( (lepts[0].momentum() + bjets[1].momentum()).mass() > 153.2 || (lepts[1].momentum() + bjets[0].momentum()).mass() > 153.2 || MT2_2 > 172.5 )
                  usesecond = false;

                if ( (lepts[0].momentum() + bjets[2].momentum()).mass() > 153.2 || (lepts[1].momentum() + bjets[0].momentum()).mass() > 153.2 || MT2_3 > 172.5 )
                  usethird = false;

                if ( (lepts[0].momentum() + bjets[2].momentum()).mass() > 153.2 || (lepts[1].momentum() + bjets[1].momentum()).mass() > 153.2 || MT2_4 > 172.5 )
                  usefourth = false;

                if ( (lepts[0].momentum() + bjets[0].momentum()).mass() > 153.2 || (lepts[1].momentum() + bjets[2].momentum()).mass() > 153.2 || MT2_5 > 172.5 )
                  usefifth = false;

                if ( (lepts[0].momentum() + bjets[1].momentum()).mass() > 153.2 || (lepts[1].momentum() + bjets[2].momentum()).mass() > 153.2 || MT2_6 > 172.5 )
                  usesixth = false;

                if ( !usefirst && !usesecond && !usethird && !usefourth && !usefifth && !usesixth )
                  continue;

                if( usefirst ) {
                  bjet3 = bjets[2];
                  if(lepts[0].pdgId() < 0) {
                    _top1 = lepts[0].momentum() + bjets[0].momentum();
                    _top1c.push_back(lepts[0].momentum()); _top1c.push_back(bjets[0].momentum());
                    _top2 = lepts[1].momentum() + bjets[1].momentum();
                    _top2c.push_back(lepts[1].momentum()); _top2c.push_back(bjets[1].momentum());
                  }
                  else {
                    _top2 = lepts[0].momentum() + bjets[0].momentum();
                    _top2c.push_back(lepts[0].momentum()); _top2c.push_back(bjets[0].momentum());
                    _top1 = lepts[1].momentum() + bjets[1].momentum();
                    _top1c.push_back(lepts[1].momentum()); _top1c.push_back(bjets[1].momentum());
                  }
                }
                else if ( usesecond ) {
                  bjet3 = bjets[2];
                  if(lepts[0].pdgId() < 0) {
                    _top1 = lepts[0].momentum() + bjets[1].momentum();
                    _top1c.push_back(lepts[0].momentum()); _top1c.push_back(bjets[1].momentum());
                    _top2 = lepts[1].momentum() + bjets[0].momentum();
                    _top2c.push_back(lepts[1].momentum()); _top2c.push_back(bjets[0].momentum());
                  }
                  else {
                    _top2 = lepts[0].momentum() + bjets[1].momentum();
                    _top2c.push_back(lepts[0].momentum()); _top2c.push_back(bjets[1].momentum());
                    _top1 = lepts[1].momentum() + bjets[0].momentum();
                    _top1c.push_back(lepts[1].momentum()); _top1c.push_back(bjets[0].momentum());
                  }
                }
                else if ( usethird ) {
                  bjet3 = bjets[1];
                  if(lepts[0].pdgId() < 0) {
                    _top1 = lepts[0].momentum() + bjets[2].momentum();
                    _top1c.push_back(lepts[0].momentum()); _top1c.push_back(bjets[1].momentum());
                    _top2 = lepts[1].momentum() + bjets[0].momentum();
                    _top2c.push_back(lepts[1].momentum()); _top2c.push_back(bjets[0].momentum());
                  }
                  else {
                    _top2 = lepts[0].momentum() + bjets[2].momentum();
                    _top2c.push_back(lepts[0].momentum()); _top2c.push_back(bjets[1].momentum());
                    _top1 = lepts[1].momentum() + bjets[0].momentum();
                    _top1c.push_back(lepts[1].momentum()); _top1c.push_back(bjets[0].momentum());
                  }
                }
                else if ( usefourth ) {
                  bjet3 = bjets[0];
                  if(lepts[0].pdgId() < 0) {
                    _top1 = lepts[0].momentum() + bjets[2].momentum();
                    _top1c.push_back(lepts[0].momentum()); _top1c.push_back(bjets[1].momentum());
                    _top2 = lepts[1].momentum() + bjets[1].momentum();
                    _top2c.push_back(lepts[1].momentum()); _top2c.push_back(bjets[0].momentum());
                  }
                  else {
                    _top2 = lepts[0].momentum() + bjets[2].momentum();
                    _top2c.push_back(lepts[0].momentum()); _top2c.push_back(bjets[1].momentum());
                    _top1 = lepts[1].momentum() + bjets[1].momentum();
                    _top1c.push_back(lepts[1].momentum()); _top1c.push_back(bjets[0].momentum());
                  }
                }
                else if ( usefifth ) {
                  bjet3 = bjets[1];
                  if(lepts[0].pdgId() < 0) {
                    _top1 = lepts[0].momentum() + bjets[0].momentum();
                    _top1c.push_back(lepts[0].momentum()); _top1c.push_back(bjets[1].momentum());
                    _top2 = lepts[1].momentum() + bjets[2].momentum();
                    _top2c.push_back(lepts[1].momentum()); _top2c.push_back(bjets[0].momentum());
                  }
                  else {
                    _top2 = lepts[0].momentum() + bjets[0].momentum();
                    _top2c.push_back(lepts[0].momentum()); _top2c.push_back(bjets[1].momentum());
                    _top1 = lepts[1].momentum() + bjets[2].momentum();
                    _top1c.push_back(lepts[1].momentum()); _top1c.push_back(bjets[0].momentum());
                  }
                }
                else if ( usesixth ) {
                  bjet3 = bjets[0];
                  if(lepts[0].pdgId() < 0) {
                    _top1 = lepts[0].momentum() + bjets[1].momentum();
                    _top1c.push_back(lepts[0].momentum()); _top1c.push_back(bjets[1].momentum());
                    _top2 = lepts[1].momentum() + bjets[2].momentum();
                    _top2c.push_back(lepts[1].momentum()); _top2c.push_back(bjets[0].momentum());
                  }
                  else {
                    _top2 = lepts[0].momentum() + bjets[1].momentum();
                    _top2c.push_back(lepts[0].momentum()); _top2c.push_back(bjets[1].momentum());
                    _top1 = lepts[1].momentum() + bjets[2].momentum();
                    _top1c.push_back(lepts[1].momentum()); _top1c.push_back(bjets[0].momentum());
                  }
                }
              }

              else {

                double mVisA1 = (lepts[0].momentum()+bjets[0].momentum()).mass(); // mass of visible object on side A.  Must be >=0.
                double pxA1 = (lepts[0].momentum()+bjets[0].momentum()).px(); // x momentum of visible object on side A.
                double pyA1 = (lepts[0].momentum()+bjets[0].momentum()).py(); // y momentum of visible object on side A.

                double mVisB1 = (lepts[1].momentum()+bjets[1].momentum()).mass(); // mass of visible object on side B.  Must be >=0.
                double pxB1 = (lepts[1].momentum()+bjets[1].momentum()).px(); // x momentum of visible object on side B.
                double pyB1 = (lepts[1].momentum()+bjets[1].momentum()).py(); // y momentum of visible object on side B.

                double pxMiss1 = met.px(); // x component of missing transverse momentum.
                double pyMiss1 = met.py(); // y component of missing transverse momentum.

                double chiA = 0.; // hypothesised mass of invisible on side A.  Must be >=0.
                double chiB = 0.; // hypothesised mass of invisible on side B.  Must be >=0.

                 asymm_mt2_lester_bisect::disableCopyrightMessage();

                double MT2_1 =  asymm_mt2_lester_bisect::get_mT2(
                           mVisA1, pxA1, pyA1,
                           mVisB1, pxB1, pyB1,
                           pxMiss1, pyMiss1,
                           chiA, chiB,
                           desiredPrecisionOnMt2);

                double mVisA2 = (lepts[0].momentum()+bjets[1].momentum()).mass(); // mass of visible object on side A.  Must be >=0.
                double pxA2 = (lepts[0].momentum()+bjets[1].momentum()).px(); // x momentum of visible object on side A.
                double pyA2 = (lepts[0].momentum()+bjets[1].momentum()).py(); // y momentum of visible object on side A.

                double mVisB2 = (lepts[1].momentum()+bjets[0].momentum()).mass(); // mass of visible object on side B.  Must be >=0.
                double pxB2 = (lepts[1].momentum()+bjets[0].momentum()).px(); // x momentum of visible object on side B.
                double pyB2 = (lepts[1].momentum()+bjets[0].momentum()).py(); // y momentum of visible object on side B.

                double pxMiss2 = met.px(); // x component of missing transverse momentum.
                double pyMiss2 = met.py(); // y component of missing transverse momentum.

                double MT2_2 =  asymm_mt2_lester_bisect::get_mT2(
                           mVisA2, pxA2, pyA2,
                           mVisB2, pxB2, pyB2,
                           pxMiss2, pyMiss2,
                           chiA, chiB,
                           desiredPrecisionOnMt2);


                bool usefirst = false;
                bool usesecond = false;

                if ( MT2_1 < MT2_2 )
                  usefirst = true;
                else
                  usesecond = true;

                if ( (lepts[0].momentum() + bjets[0].momentum()).mass() > 153.2 || (lepts[1].momentum() + bjets[1].momentum()).mass() > 153.2 || MT2_1 > 172.5 )
                  usefirst = false;

                if ( (lepts[0].momentum() + bjets[1].momentum()).mass() > 153.2 || (lepts[1].momentum() + bjets[0].momentum()).mass() > 153.2 || MT2_2 > 172.5 )
                  usesecond = false;


                if(!usefirst && !usesecond)
                  continue;

                if( usefirst) {
  //                bjet3 = bjets[2];
                  if(lepts[0].pdgId() < 0) {
                    _top1 = lepts[0].momentum() + bjets[0].momentum();
                    _top1c.push_back(lepts[0].momentum()); _top1c.push_back(bjets[0].momentum());
                    _top2 = lepts[1].momentum() + bjets[1].momentum();
                    _top2c.push_back(lepts[1].momentum()); _top2c.push_back(bjets[1].momentum());
                  }
                  else {
                    _top2 = lepts[0].momentum() + bjets[0].momentum();
                    _top2c.push_back(lepts[0].momentum()); _top2c.push_back(bjets[0].momentum());
                    _top1 = lepts[1].momentum() + bjets[1].momentum();
                    _top1c.push_back(lepts[1].momentum()); _top1c.push_back(bjets[1].momentum());
                  }
                }
                else {
  //                bjet3 = bjets[2];
                  if(lepts[0].pdgId() < 0) {
                    _top1 = lepts[0].momentum() + bjets[1].momentum();
                    _top1c.push_back(lepts[0].momentum()); _top1c.push_back(bjets[1].momentum());
                    _top2 = lepts[1].momentum() + bjets[0].momentum();
                    _top2c.push_back(lepts[1].momentum()); _top2c.push_back(bjets[0].momentum());
                  }
                  else {
                    _top2 = lepts[0].momentum() + bjets[1].momentum();
                    _top2c.push_back(lepts[0].momentum()); _top2c.push_back(bjets[1].momentum());
                    _top1 = lepts[1].momentum() + bjets[0].momentum();
                    _top1c.push_back(lepts[1].momentum()); _top1c.push_back(bjets[0].momentum());
                  }
                }
              }

              // mtt_T2 cut, see 1109.2201v3
              FourMomentum visible = lepts[0].momentum() + lepts[1].momentum() + _top1c[1] + _top2c[1];
              double mtt_T2 = visible.mass2() + 2 * (sqrt(visible.pT() * visible.pT() + visible.mass2()) * met.pT() -  visible.dot(met));
              if ( sqrt(mtt_T2) < 277 ) continue;

              if(num_bjets == 3) {
                _histos["b3l+_dphi"]->fill(deltaPhi(bjet3,_top2c[0]),weight);
                _histos["b3l-_dphi"]->fill(deltaPhi(bjet3,_top1c[0]),weight);
              }

              pass_to_pair += weight;

  //            cout << deltaR(blp[0],_top1c[1]) << "         " << deltaR(blm[0],_top1c[1]) << endl;

              if ( deltaR(blp[0],_top1c[1]) + deltaR(blm[0],_top2c[1]) < deltaR(blm[0],_top1c[1]) + deltaR(blp[0],_top2c[1]) )
                correct_pair += weight;
  //            else if ( deltaR(blm[0],_top1c[1]) + deltaR(blp[0],_top2c[1]) > 1. )
  //              pass_to_pair -= weight;

              /// At this point we have tops before adding neutrinos so still "realistic"

              double mVisA = _top1.mass(); // mass of visible object on side A.  Must be >=0.
              double pxA = _top1.px(); // x momentum of visible object on side A.
              double pyA = _top1.py(); // y momentum of visible object on side A.

              double mVisB = _top2.mass(); // mass of visible object on side B.  Must be >=0.
              double pxB = _top2.px(); // x momentum of visible object on side B.
              double pyB = _top2.py(); // y momentum of visible object on side B.

              double pxMiss = met.px(); // x component of missing transverse momentum.
              double pyMiss = met.py(); // y component of missing transverse momentum.

              double chiA = 0.; // hypothesised mass of invisible on side A.  Must be >=0.
              double chiB = 0.; // hypothesised mass of invisible on side B.  Must be >=0.

              double MT2 =  asymm_mt2_lester_bisect::get_mT2(
                         mVisA, pxA, pyA,
                         mVisB, pxB, pyB,
                         pxMiss, pyMiss,
                         chiA, chiB,
                         desiredPrecisionOnMt2);

              _histos["MT2bef"]->fill(MT2, weight);

              std::pair <double,double> bestsol = ben_findsols(MT2, pxA, pyA, mVisA, chiA, pxB, pyB, pxMiss, pyMiss, mVisB, chiB);

              double Ep = _top1.E();
              double kxp = _top1.px();
              double kyp = _top1.py();
              double kzp = _top1.pz();
              double mt = 172.5;
              double kx = bestsol.first;
              double ky = bestsol.second;

              double kz1 = KZ1(Ep,kxp,kyp,kzp,mt,kx,ky);
              double EE1 = ee1(Ep,kxp,kyp,kzp,mt,kx,ky);

              double kz2 = KZ2(Ep,kxp,kyp,kzp,mt,kx,ky);
              double EE2 = ee2(Ep,kxp,kyp,kzp,mt,kx,ky);


              if(EE1 != EE1 || EE2 != EE2 || kz1 != kz1 || kz2 != kz2) { cout << "Numerical problems with MT2 solving... should not happen often!" << endl << kx << "      " << ky << "      " << Ep << "       " << kzp << endl; continue; }

              vector<FourMomentum> MAOS1;
              MAOS1.push_back(FourMomentum(EE1, kx, ky, kz1));
              MAOS1.push_back(FourMomentum(EE2, kx, ky, kz2));

              Ep = _top2.E();
              kxp = _top2.px();
              kyp = _top2.py();
              kzp = _top2.pz();
              mt = 172.5;
              kx = met.px() - bestsol.first;
              ky = met.py() - bestsol.second;


              kz1 = KZ1(Ep,kxp,kyp,kzp,mt,kx,ky);
              EE1 = ee1(Ep,kxp,kyp,kzp,mt,kx,ky);

              kz2 = KZ2(Ep,kxp,kyp,kzp,mt,kx,ky);
              EE2 = ee2(Ep,kxp,kyp,kzp,mt,kx,ky);

              if(EE1 != EE1 || EE2 != EE2 || kz1 != kz1 || kz2 != kz2) { cout << "Numerical problems with MT2 solving... should not happen often! Second" << endl << kx << "      " << ky << "      " << Ep << "       " << kzp << endl; continue; }

              vector<FourMomentum> MAOS2;
              MAOS2.push_back(FourMomentum(EE1, kx, ky, kz1));
              MAOS2.push_back(FourMomentum(EE2, kx, ky, kz2));

              FourMomentum top1bef = _top1;
              FourMomentum top2bef = _top2;
              vector<FourMomentum> top1cbef = _top1c;
              vector<FourMomentum> top2cbef = _top2c;

  //            cout << _ev_weight << endl;
              passweight += weight;

              for(size_t i = 0; i < MAOS1.size(); ++i) {
                for(size_t j = 0; j < MAOS2.size(); ++j) {

                  _top1 = top1bef;
                  _top2 = top2bef;
                  _top1c = top1cbef;
                  _top2c = top2cbef;

                  _top1 = _top1 + MAOS1[i];
                  _top2 = _top2 + MAOS2[j];
                  _top1c.push_back(MAOS1[i]);
                  _top2c.push_back(MAOS2[j]);

                  _histos["UL1"]->fill(_top1c[0].E()/(_top1c[0].E()+_top1c[1].E()), weight);
                  _histos["UL2"]->fill(_top2c[0].E()/(_top2c[0].E()+_top2c[1].E()), weight);
                  _histos["XL1"]->fill(2*_top1c[0].E()/172.5, weight);
                  _histos["XL2"]->fill(2*_top2c[0].E()/172.5, weight);

                  MatrixElementAnalysis mea;
                  TLorentzVector t(_top1.px(), _top1.py(), _top1.pz(), _top1.E());
                  TLorentzVector lp(_top1c[0].px(), _top1c[0].py(), _top1c[0].pz(), _top1c[0].E());
                  TLorentzVector tbar(_top2.px(), _top2.py(), _top2.pz(), _top2.E());
                  TLorentzVector lm(_top2c[0].px(), _top2c[0].py(), _top2c[0].pz(), _top2c[0].E());
                  mea.calculate(1., t, tbar, lp, lm);


                  vector<double> bts = BaumgartTweedie(t, tbar, lp, lm);
  //                cout << bts[0] << "     " << bts[1] << "      " << _top1.mass() << "      " << _top2.mass() << "      " << mea.getHelicityTheta1() * mea.getHelicityTheta2() << "     " << mea.getLHCOptimisedTheta1() * mea.getLHCOptimisedTheta2() << (_top1c[1]+_top1c[2]).mass() << "       " << (_top2c[1]+_top2c[2]).mass() << "       " << weight << "       " << fabs(lp.DeltaPhi(lm)) << endl;

                  _histos["LEADINGT_MASS"]->fill(_top1.mass(), weight);
                  _histos["LEADINGJ_PT"]->fill(jets[0].pT(), weight);
                  _histos["SUBLEADINGT_MASS"]->fill(_top2.mass(), weight);
                  _histos["WP_MASS"]->fill( (_top1c[1]+_top1c[2]).mass(), weight);
                  _histos["WM_MASS"]->fill( (_top2c[1]+_top2c[2]).mass(), weight);


                  _histos["DPHI_LPLM"]->fill(fabs(lp.DeltaPhi(lm)),weight);
                  _histos["COSTHETACOSTHETA"]->fill(mea.getHelicityTheta1() * mea.getHelicityTheta2(), weight);
                  _histos["COSTHETA1"]->fill(mea.getHelicityTheta1(), weight);
                  _histos["COSTHETA2"]->fill(mea.getHelicityTheta2(), weight);
                  _histos["COSTHETACOSTHETA_UWER"]->fill(mea.getLHCOptimisedTheta1() * mea.getLHCOptimisedTheta2(), weight);

                  _histos["BT_DIFF"]->fill(bts[0], weight);
                  _histos["BT_SUM"]->fill(bts[1], weight);

                }
              }
            }

            return;
        }


        /// Normalise histograms etc., after the run
        void finalize() {
          cout << "Finalizing..." << endl;
          double luminosity = 20./femtobarn;
          double norm = crossSection() * luminosity;
          cout << crossSection() << endl;
          cout << crossSection() * passweight/totalweight << endl;

          BOOST_FOREACH(HistoMap::value_type &histo, _histos) {
            histo.second->normalize(norm * (passweight/totalweight));
          }

          outFile << "Number of events: " << evno << endl;
          outFile << "Cross section of all events: " << crossSection() << endl;
          outFile << "Total weight of events: " << sumOfWeights() << endl;
          outFile << "Total weight of all events normalised to luminosity: " << norm << endl;
          outFile << "Total passed weight of all events normalised to luminosity: " << norm * (passweight / totalweight)  << endl;
          outFile << "Acceptance: " << 100 * (passweight / totalweight) << endl;

          outFile << "Correct b-lepton pairing in " << correct_pair/pass_to_pair << " of the events." << endl;
          outFile << "Average number of b jets: " << nbj/totalweightuptobj << endl;

          outFile.close();
        }

        //@}


    private:


    private:

        unsigned int evno;
        double totalweight, passweight, correct_pair, pass_to_pair, nbj,totalweightuptobj;
        HistoMap _histos;
        vector<FourMomentum> _top1c, _top2c;
        FourMomentum _top1,_top2;

        std::ofstream outFile;

    };

    // This global object acts as a hook for the plugin system
    AtomPlugin(ATOM_TopPolarisation)

}
