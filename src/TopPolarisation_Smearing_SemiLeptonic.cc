// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/SmearedParticles.hh"
#include "Rivet/Projections/SmearedJets.hh"
#include "Rivet/Projections/SmearedMET.hh"
#include "Rivet/Projections/WFinder.hh"
#include "../HEPTopTagger/HEPTopTagger.hh"
#include "TLorentzVector.h"
#include "TVector3.h"
#include "TVector2.h"
#include "MatrixElementAnalysis.h"
#include "lester_mt2_bisect.h"
#include <random>
#include <gsl/gsl_fit.h>


namespace Rivet {

  bool isBMeson(int pdgId){
    if( fabs(pdgId)==511 || fabs(pdgId)==531 || fabs(pdgId)==521
             || fabs(pdgId)==5122 || fabs(pdgId)==5232 ||  fabs(pdgId)==5132 || fabs(pdgId)==5332
             || fabs(pdgId)==5322 || fabs(pdgId)==5312 || fabs(pdgId)==533 || fabs(pdgId)==513
             || fabs(pdgId)==523  || fabs(pdgId)==541 || fabs(pdgId)==543
             || fabs(pdgId)==5114 || fabs(pdgId)==5212 || fabs(pdgId)==5224 || fabs(pdgId)==5222
             || fabs(pdgId)==5214 || fabs(pdgId)==5112 || fabs(pdgId)==5324 || fabs(pdgId)==5314
             || fabs(pdgId)==5334 )
      return true;
    return false;
  }

  double _transMass(double ptLep, double phiLep, double met, double phiMet) {
    return sqrt(2.0*ptLep*met*(1 - cos(phiLep-phiMet)));
  }

  HepMC::GenVertex::particle_iterator findLepton(HepMC::GenVertex::particle_iterator p, int id, FourMomentum *ret) {
    if( !(*p)->end_vertex() ) return p;
    for ( HepMC::GenVertex::particle_iterator daughter = (*p)->end_vertex()-> particles_begin(HepMC::descendants);
                      daughter != (*p)->end_vertex()-> particles_end(HepMC::descendants); ++daughter ) {
//      cout << (*daughter)->pdg_id() << endl;
      if((*daughter)->pdg_id() == id) {
//        cout << endl;
        FourMomentum tmp = FourMomentum((*daughter)->momentum().e(),(*daughter)->momentum().px(), (*daughter)->momentum().py(), (*daughter)->momentum().pz());
        *ret = tmp;
        return daughter;
      }
//      else if ((*daughter) != (*p)) { findLepton(daughter, id, ret); }
    }
    return p;
  }

  vector<double> BaumgartTweedie(TLorentzVector t, TLorentzVector tbar, TLorentzVector lp, TLorentzVector lm) {
//      const double pt = t.Pt() + tbar.Pt();

//      cout << t.Pt() << "   " << tbar.Pt() << "   " << lp.Pt() << "   " << lm.Pt() << endl;

      //boost to com
      TLorentzVector ttbar = t + tbar;
      TVector3 boost_to_ttbar = -1. * ttbar.BoostVector();

      t.Boost(boost_to_ttbar);
      tbar.Boost(boost_to_ttbar);
      lp.Boost(boost_to_ttbar);
      lm.Boost(boost_to_ttbar);

      //boost from ZMF to top frame
      TVector3 boost_to_top = -1. * t.BoostVector();
      lp.Boost(boost_to_top);

      //boost from ZMF to tbar frame
      TVector3 boost_to_tbar = -1. * tbar.BoostVector();
      lm.Boost(boost_to_tbar);

      TVector3 beamZ(0, 0, 1);

      TVector3 newZ = t.Vect().Unit();
      TVector3 newY = t.Vect().Unit().Cross(beamZ).Unit();
      TVector3 newX = newY.Cross(newZ).Unit();

      TRotation m;
      m.RotateAxes(newX, newY, newZ);
      m = m.Inverse();
      lp.Transform(m);
      lm.Transform(m);

      vector<double> ret;

      ret.push_back(TVector2::Phi_mpi_pi(lp.Phi() - lm.Phi()));
      ret.push_back(TVector2::Phi_mpi_pi(lp.Phi() + lm.Phi()));

      return ret;
  }

  /// @brief top polarisation
  class TopPolarisation_Smearing_SemiLeptonic : public Analysis {
  public:

    /// Default constructor
    TopPolarisation_Smearing_SemiLeptonic() : Analysis("TopPolarisation_Smearing_SemiLeptonic")
    {    }


    /// @name Analysis methods
    //@{

    void init() {

      // Initialise and register projections
      FinalState calofs(Cuts::abseta < 4.8);
      MissingMomentum mm(calofs);
      declare(mm, "TruthMET");
      declare(SmearedMET(mm, MET_SMEAR_ATLAS_RUN2), "RecoMET");

      PromptFinalState es(Cuts::abseta < 2.47 && Cuts::abspid == PID::ELECTRON, true, true);
      declare(es, "TruthElectrons");
      declare(SmearedParticles(es, ELECTRON_EFF_ATLAS_RUN2, ELECTRON_SMEAR_ATLAS_RUN2), "RecoElectrons");

      PromptFinalState mus(Cuts::abseta < 2.7 && Cuts::abspid == PID::MUON, true);
      declare(mus, "TruthMuons");
      declare(SmearedParticles(mus, MUON_EFF_ATLAS_RUN2, MUON_SMEAR_ATLAS_RUN2), "RecoMuons");

      IdentifiedFinalState nu_id;
      nu_id.acceptNeutrinos();
      PromptFinalState neutrinos(nu_id);
      neutrinos.acceptTauDecays(true);
      addProjection(neutrinos, "neutrinos");

      VetoedFinalState vcalofs(calofs);
      vcalofs.addVetoOnThisFinalState(mus);
      vcalofs.addVetoOnThisFinalState(es);
      vcalofs.addVetoOnThisFinalState(neutrinos);
      FastJets fj(vcalofs, FastJets::ANTIKT, 0.4);
      FastJets fjFat(vcalofs, FastJets::CAM, 1.2);
      declare(fj, "TruthJets");
      declare(fjFat, "FatJets");
      declare(SmearedJets(fj, JET_SMEAR_ATLAS_RUN2, JET_BTAG_ATLAS_RUN2_MV2C20), "RecoJets");
      declare(SmearedJets(fjFat, JET_SMEAR_ATLAS_RUN2, JET_BTAG_ATLAS_RUN2_MV2C20), "RecoJetsFat");

      massbins = 20;
      minmass = 2000.;
      maxmass = 6000.;
      cosbins = 10;

      _histos["LEADINGJ_PT"]   = bookHisto1D("leadingjetpt", 50, 0, 500, "", "leading jet pt", "Relative Occurence");
      _histos["HADRONICT_MASS"]   = bookHisto1D("HADRONICtopmass", 20, 0, 300, "", "HADRONICtop mass", "Relative Occurence");
      _histos["LEPTONICT_MASS"]   = bookHisto1D("LEPTONICtopmass", 20, 0, 300, "", "LEPTONICtop mass", "Relative Occurence");
      _histos["LEPTONICW_MASS"]   = bookHisto1D("leptonicwmass", 20, 0, 200, "", "leptonic w mass", "Relative Occurence");
      _histos["TTBAR_MASS"]   = bookHisto1D("ttbarmass", massbins, minmass, maxmass, "", "ttbar mass", "Relative Occurence");
      _histos["COSTHETAPROD"]   = bookHisto1D("costhetaprod", cosbins, -1, 1, "", "cos theta prod", "Relative Occurence");
      _histos["COSTHETATOP"]   = bookHisto1D("costhetatop", cosbins, -1, 1, "", "cos theta top", "Relative Occurence");
      _2dhistos["COSTHETAPROD_2D"]   = bookHisto2D("COSTHETAPROD_2D", massbins, minmass, maxmass, cosbins,-1,1,"", "cos theta prod mass binned", "Relative Occurence");
      _2dhistos["COSTHETATOP_2D"]   = bookHisto2D("COSTHETATOP_2D", massbins, minmass, maxmass, cosbins,-1,1,"", "cos theta top mass binned", "Relative Occurence");
      _histos["AFB_PRENORM"]   = bookHisto1D("AFB_PRENORM", massbins, minmass, maxmass, "", "forward backward A pre norm", "Relative Occurence");
      _histos["AFB"]   = bookHisto1D("AFB", massbins, minmass, maxmass, "", "forward backward A", "Relative Occurence");
      _2dhistos["AL_PRENORM"]   = bookHisto2D("AL_PRENORM", massbins, minmass, maxmass, cosbins,-1,1,"", "lepton angle asymmetry pre norm", "Relative Occurence");
      _histos["AL"]   = bookHisto1D("AL", massbins, minmass, maxmass, "", "lepton angle asymmetry", "Relative Occurence");
      _histos["AL_NOTBINNED"]   = bookHisto1D("AL_NOTBINNED", cosbins, -1, 1, "", "lepton angle asymmetry", "Relative Occurence");

      nev = 0;
      nevb = 0;
      correct_pair = 0;
      nbjets = 0;
      totalweight = 0.;
      passweight = 0.;
      pairweight = 0.;
      beforetag = 0.;
      aftertag = 0.;
      _cuts["m init"] = 0;
      // specify an input file
      ascii_in = new HepMC::IO_GenEvent("herwig_noshower.hepmc",std::ios::in);
      // get the first event
      evt1 = ascii_in->read_next_event();
    }


    void analyze(const Event& event) {

      double weight = event.weight();
      const HepMC::GenEvent *evt = event.genEvent();
      _dressedleptons.clear();
      _bjets.clear();
      _top1c.clear();
      _top2c.clear();


      vector<FourMomentum> blp(3), blm(3);
      vector<FourMomentum> bMesons,bMestmp;
      unsigned int bs = 0;
      FourMomentum hard1, hard2, top,topbar;

      HepMC::GenEvent::particle_const_iterator e1 = evt->particles_end();
      for ( HepMC::GenEvent::particle_const_iterator p1 = evt->particles_begin(); p1 != e1; ++p1 )
      {
        if ( isBMeson( (*p1)->pdg_id() ) && (*p1)->status() == 1  )
          bMestmp.push_back(FourMomentum((*p1)->momentum().e(),(*p1)->momentum().px(), (*p1)->momentum().py(), (*p1)->momentum().pz()));
      }

      HepMC::GenEvent::particle_const_iterator e = evt1->particles_end();
      for ( HepMC::GenEvent::particle_const_iterator p = evt1->particles_begin(); p != e; ++p )
      {
        if ( isBMeson( (*p)->pdg_id() ) && (*p)->status() == 1  )
          bMestmp.push_back(FourMomentum((*p)->momentum().e(),(*p)->momentum().px(), (*p)->momentum().py(), (*p)->momentum().pz()));
        if ( abs( (*p)->pdg_id() ) == 5 )
          bs++;


        if ( (*p)->pdg_id()==6) {
          top = FourMomentum((*p)->momentum().e(),(*p)->momentum().px(), (*p)->momentum().py(), (*p)->momentum().pz());
          if ( (*p)->end_vertex() )
          {
            for ( HepMC::GenVertex::particle_iterator daughter = (*p)->end_vertex()-> particles_begin(HepMC::descendants);
                  daughter != (*p)->end_vertex()-> particles_end(HepMC::descendants); ++daughter )
            {
             if ((*daughter)->pdg_id() == 5) { blp.push_back(FourMomentum((*daughter)->momentum().e(),(*daughter)->momentum().px(), (*daughter)->momentum().py(), (*daughter)->momentum().pz()));  break;}
              if ((*daughter)->pdg_id() == 5) { blp[0]=FourMomentum((*daughter)->momentum().e(),(*daughter)->momentum().px(), (*daughter)->momentum().py(), (*daughter)->momentum().pz());  break;}
            }
            for ( HepMC::GenVertex::particle_iterator daughter = (*p)->end_vertex()-> particles_begin(HepMC::descendants);
                  daughter != (*p)->end_vertex()-> particles_end(HepMC::descendants); ++daughter )
            {
              if ( (*daughter)->pdg_id() == 24 && (*daughter)->end_vertex() ) {
                FourMomentum lepton1(0.,0.,0.,0.);
                FourMomentum lepton2(0.,0.,0.,0.);
//                 HepMC::GenVertex::particle_iterator lep1, lep2;
//                 lep1 = findLepton(daughter, -11, &lepton1);
//                 lep2 = findLepton(daughter, 12, &lepton2);
//                 if(lep1==lep2) {
//                   lep1 = findLepton(daughter, -13, &lepton1);
//                   lep2 = findLepton(daughter, 14, &lepton2);
//                 }
//                 if(lep1!=lep2) {
// //                  blp.push_back(lepton1);
// //                  blp.push_back(lepton2);
//                   blp[1]=lepton1;
//                   blp[2]=lepton2;
//                 }
//                 break;
              }
            }
          }
        }

        if ( (*p)->pdg_id()==-6) {
          topbar = FourMomentum((*p)->momentum().e(),(*p)->momentum().px(), (*p)->momentum().py(), (*p)->momentum().pz());
          if ( (*p)->end_vertex() )
          {
            for ( HepMC::GenVertex::particle_iterator daughter = (*p)->end_vertex()-> particles_begin(HepMC::descendants);
                  daughter != (*p)->end_vertex()-> particles_end(HepMC::descendants); ++daughter )
            {
             if ((*daughter)->pdg_id() == -5) { blm.push_back(FourMomentum((*daughter)->momentum().e(),(*daughter)->momentum().px(), (*daughter)->momentum().py(), (*daughter)->momentum().pz())); break;}
              if ((*daughter)->pdg_id() == -5) { blm[0]=FourMomentum((*daughter)->momentum().e(),(*daughter)->momentum().px(), (*daughter)->momentum().py(), (*daughter)->momentum().pz()); break;}
            }
            for ( HepMC::GenVertex::particle_iterator daughter = (*p)->end_vertex()-> particles_begin(HepMC::descendants);
                  daughter != (*p)->end_vertex()-> particles_end(HepMC::descendants); ++daughter )
            {
              if ( (*daughter)->pdg_id() == -24  ) {
                FourMomentum lepton1(0.,0.,0.,0.);
                FourMomentum lepton2(0.,0.,0.,0.);
//                 HepMC::GenVertex::particle_iterator lep1, lep2;
//                 lep1 = findLepton(daughter, 11, &lepton1);
//                 lep2 = findLepton(daughter, -12, &lepton2);
//                 if(lep1 == lep2 ) {
//                   lep1 = findLepton(daughter, 13, &lepton1);
//                   lep2 = findLepton(daughter, -14, &lepton2);
//                 }
//                 if(lep1 != lep2) {
// //                  blm.push_back(lepton1);
// //                  blm.push_back(lepton2);
//                   blm[1]=lepton1;
//                   blm[2]=lepton2;
//                 }
//                 break;
              }
            }
          }
        }
      }
      nev++;
//      delete evt1;
//      if(nev != 1000000)
//        *ascii_in >> evt1;
//
//      if(nev == 195394) {
//        delete evt1;
//        *ascii_in >> evt1;
//      }


//      cout << blp.size() << "       " << blm.size() << endl;
//      cout << (blm[0]+blm[1]+blm[2]).mass() << endl;


      if(blp.size() < 3 || blm.size() < 3) { cout << "veto" << endl; vetoEvent; }

      for(size_t i = 0; i < bMestmp.size(); ++i) {
        if ( bMestmp[i].pT() > 10. ) bMesons.push_back(bMestmp[i]);
      }
    //  cout << bMesons.size() << endl;


      map<int,FourMomentum> bMes;
      for(unsigned int i = 0; i < bMesons.size(); ++i) {
        bMes[i] = bMesons[i];
      }


      if (nev % 1000 == 999) {
        cout << "Passing rate " << (double) passweight/totalweight << endl;
        cout << "Correct pairing " << (double) correct_pair/pairweight << endl;
      }

      Particles _dressedelectrons = apply<ParticleFinder>(event, "RecoElectrons").particles(Cuts::pT > 10*GeV);
      Particles _dressedmuons = apply<ParticleFinder>(event, "RecoMuons").particles(Cuts::pT > 10*GeV);
      Jets _jets = apply<JetAlg>(event, "RecoJets").jetsByPt(Cuts::pT > 20*GeV && Cuts::abseta < 2.8); ///< @todo Pile-up subtraction
      Jets _fatjets = apply<JetAlg>(event, "RecoJetsFat").jetsByPt(Cuts::pT > 250*GeV && Cuts::abseta < 2.0); ///< @todo Pile-up subtraction
      /// Note that we use unsmeared fat jets to be able to use HEPTopTagger
      vector<fastjet::PseudoJet> _fatpseudojets = applyProjection<FastJets>(event, "FatJets").pseudoJetsByPt(250*GeV); ///< @todo Pile-up subtraction
      ifilter_discard(_fatpseudojets, [&](const fastjet::PseudoJet& jet){ return abs(jet.eta()) > 2.0; });
      // Same MET cut for all signal regions
      //const Vector3 vmet = -apply<MissingMomentum>(event, "TruthMET").vectorEt();
      const Vector3 vmet = -apply<SmearedMET>(event, "RecoMET").vectorEt();
      FourMomentum pmet = FourMomentum(vmet.perp(), vmet.x(), vmet.y(), 0.);

      ///
      ///
      ///
      ///


      /// Use truth b-jet+lepton pairings (with truth four momenta)?
      bool truth = false;

      /// Use MT2 reconstructed neutrino guesses (otherwise truth neutrinos)?
      bool mt2reconstruct = true;


      ///
      ///
      ///
      ///

        /// We don't do this to allow boosted configurations.
      // Jet/electron/muons overlap removal and selection
      // Remove any |eta| < 2.8 jet within dR = 0.2 of a baseline electron
//      for (const Particle& e : _dressedelectrons)
//        ifilter_discard(_jets, deltaRLess(e, 0.2, RAPIDITY));
      // Remove any electron or muon with dR < 0.4 of a remaining (Nch > 3) jet
//      for (const Jet& j : _jets) {
//        /// @todo Add track efficiency random filtering
//        ifilter_discard(_dressedelectrons, deltaRLess(j, 0.4, RAPIDITY));
//        if (j.particles(Cuts::abscharge > 0 && Cuts::pT > 500*MeV).size() >= 3)
//          ifilter_discard(_dressedmuons, deltaRLess(j, 0.4, RAPIDITY));
//      }
      // Discard the softer of any electrons within dR < 0.05
      for (size_t i = 0; i < _dressedelectrons.size(); ++i) {
        const Particle& e1 = _dressedelectrons[i];
        /// @todo Would be nice to pass a "tail view" for the filtering, but awkward without range API / iterator guts
        ifilter_discard(_dressedelectrons, [&](const Particle& e2){ return e2.pT() < e1.pT() && deltaR(e1,e2) < 0.05; });
      }

      _met_et = pmet.pT();
      _met_phi = pmet.phi();

      std::random_device rd;
      std::mt19937 generator (rd());

      std::uniform_real_distribution<double> dis(0.0, 1.0);


//      cout << "start of loop" << endl;

      for(unsigned int ev = 0; ev < 1; ++ev) {

        totalweight += weight;
        _cuts["a total"] += weight;

        if ( _jets.size() < 1 ) continue;
        // if ( _fatjets.size() < 1 ) continue;
        if ( _fatpseudojets.size() < 1 ) continue;

        _cuts["b jetmult"] += weight;

        ifilter_discard(_jets, [&](const Jet& jet){ return deltaR(jet,Jet(_fatpseudojets[0]) ) < 1.4; });
        // ifilter_discard(_jets, [&](const Jet& jet){ return deltaR(jet,_fatjets[0] ) < 1.4; });
      //  cout << _jets.size() << "      " << bMes.size() << endl;


        unsigned int i,j;
        _jet_ntag = 0;

        pmet.setPz(0.);
        pmet.setE(0.);

        vector<int> used;
        Jets btmps;

        for(j = 0; j < bMes.size(); ++j) {
          double mindR = 1000.;
          int ind = -1;
          for(i = 0; i < _jets.size(); ++i) {
            if( deltaR(_jets[i], bMes[j]) < mindR ) {
              mindR = deltaR(_jets[i], bMes[j]);
              ind = i;
            }
          }
          if (mindR < 0.4) {
            if(std::find(used.begin(), used.end(), ind) != used.end()) {
              continue;
            } else {
              used.push_back(ind);
              btmps.push_back(_jets[ind]);
            }
          }
        }

        // cout << btmps.size() << endl;

        for(i = 0; i < _jets.size(); ++i) {
          double rand = dis(generator);
          if(std::find(used.begin(), used.end(), i) != used.end()) {
            if(rand < 0.7) _bjets.push_back(_jets[i]);
          }
          else if (rand < 0.01 || (_jets[i].momentum().pT() > 300. && rand < 0.1) ) _bjets.push_back(_jets[i]);
        }

  //      cout << _bjets.size() << endl;
  //      cout << _jets.size() << endl;
  //      cout << endl;
        nevb++;
        nbjets+=_bjets.size();

        // need two leptons
        if(_dressedelectrons.size() + _dressedmuons.size() < 1 ) continue;
        _cuts["c lepton"] += weight;

        foreach(const DressedLepton& el, _dressedelectrons) {
          _dressedleptons.push_back(el);
        }
        foreach(const DressedLepton& mu, _dressedmuons) {
          _dressedleptons.push_back(mu);
        }

        sortByPt(_dressedleptons);

        // want one jet with b tags
        if(_bjets.size() < 1) continue;
        _cuts["d bjetmult"] += weight;
        // want et_miss > 20 GeV and et_miss + mT > 60 GeV
        if(pmet.pT() < 20.) continue;
        if(_transMass(_dressedleptons[0].pT(), _dressedleptons[0].phi(), pmet.pT(), pmet.phi()) + pmet.pT()  < 60.) continue;
        _cuts["e met"] += weight;
        // mtt_T2 cut, see 1109.2201v3
        vector<FourMomentum> w_leptonic;

        double mw = 80.4;
        double pzv1=0., Ev1=0., pzv2=0., Ev2=0.;

        bool success = false;

        while(true) {
          double pxv = pmet.px();
          double pyv = pmet.py();
          double El = _dressedleptons[0].momentum().E();
          double pxl = _dressedleptons[0].momentum().px();
          double pyl = _dressedleptons[0].momentum().py();
          double pzl = _dressedleptons[0].momentum().pz();

          pzv1 = (pzl*(-pow(El,2) + pow(mw,2) + pow(pxl,2) + 2*pxl*pxv +
           pow(pyl,2) + 2*pyl*pyv + pow(pzl,2)) +
        sqrt(pow(El,2)*(pow(El,4) +
            pow(pow(mw,2) + pow(pxl,2) + 2*pxl*pxv + pow(pyl,2) +
              2*pyl*pyv,2) + 2*(pow(mw,2) + pow(pxl,2) + 2*pxl*pxv +
               2*pow(pxv,2) + pow(pyl,2) + 2*pyl*pyv + 2*pow(pyv,2))*
             pow(pzl,2) + pow(pzl,4) -
            2*pow(El,2)*(pow(mw,2) + pow(pxl,2) + 2*pxl*pxv +
               2*pow(pxv,2) + pow(pyl,2) + 2*pyl*pyv + 2*pow(pyv,2) +
               pow(pzl,2)))))/(2.*(El - pzl)*(El + pzl));
          Ev1 = (-pow(El,4) + pow(El,2)*
         (pow(mw,2) + pow(pxl,2) + 2*pxl*pxv + pow(pyl,2) + 2*pyl*pyv +
           pow(pzl,2)) + pzl*sqrt(pow(El,2)*
           (pow(El,4) + pow(pow(mw,2) + pow(pxl,2) + 2*pxl*pxv +
               pow(pyl,2) + 2*pyl*pyv,2) +
             2*(pow(mw,2) + pow(pxl,2) + 2*pxl*pxv + 2*pow(pxv,2) +
                pow(pyl,2) + 2*pyl*pyv + 2*pow(pyv,2))*pow(pzl,2) +
             pow(pzl,4) - 2*pow(El,2)*
              (pow(mw,2) + pow(pxl,2) + 2*pxl*pxv + 2*pow(pxv,2) +
                pow(pyl,2) + 2*pyl*pyv + 2*pow(pyv,2) + pow(pzl,2)))))/
      (2.*El*(El - pzl)*(El + pzl));

          pzv2 = (pzl*(-pow(El,2) + pow(mw,2) + pow(pxl,2) + 2*pxl*pxv +
           pow(pyl,2) + 2*pyl*pyv + pow(pzl,2)) -
        sqrt(pow(El,2)*(pow(El,4) +
            pow(pow(mw,2) + pow(pxl,2) + 2*pxl*pxv + pow(pyl,2) +
              2*pyl*pyv,2) + 2*(pow(mw,2) + pow(pxl,2) + 2*pxl*pxv +
               2*pow(pxv,2) + pow(pyl,2) + 2*pyl*pyv + 2*pow(pyv,2))*
             pow(pzl,2) + pow(pzl,4) -
            2*pow(El,2)*(pow(mw,2) + pow(pxl,2) + 2*pxl*pxv +
               2*pow(pxv,2) + pow(pyl,2) + 2*pyl*pyv + 2*pow(pyv,2) +
               pow(pzl,2)))))/(2.*(El - pzl)*(El + pzl));
          Ev2 = -(pow(El,4) - pow(El,2)*
          (pow(mw,2) + pow(pxl,2) + 2*pxl*pxv + pow(pyl,2) +
            2*pyl*pyv + pow(pzl,2)) +
         pzl*sqrt(pow(El,2)*(pow(El,4) +
              pow(pow(mw,2) + pow(pxl,2) + 2*pxl*pxv + pow(pyl,2) +
                2*pyl*pyv,2) + 2*
               (pow(mw,2) + pow(pxl,2) + 2*pxl*pxv + 2*pow(pxv,2) +
                 pow(pyl,2) + 2*pyl*pyv + 2*pow(pyv,2))*pow(pzl,2) +
              pow(pzl,4) - 2*pow(El,2)*
               (pow(mw,2) + pow(pxl,2) + 2*pxl*pxv + 2*pow(pxv,2) +
                 pow(pyl,2) + 2*pyl*pyv + 2*pow(pyv,2) + pow(pzl,2)))))/
      (2.*El*(El - pzl)*(El + pzl));

          /// hit numerical issue reconstructing the neutrino
          if (pzv1 != pzv1 || pzv2 != pzv2 || Ev1 != Ev1 || Ev2 != Ev2) {
            if(mw > 150.) break;
            mw += 1.;
          }
          else {
            success = true;
            break;
          }
        }
        if(!success) continue;

        _cuts["f neutrino"] += weight;
        w_leptonic.push_back(FourMomentum(Ev1,pmet.px(),pmet.py(),pzv1)+_dressedleptons[0]);
        w_leptonic.push_back(FourMomentum(Ev2,pmet.px(),pmet.py(),pzv2)+_dressedleptons[0]);

        // cout << w_leptonic[0].mass() << "       " << w_leptonic[1].mass() << endl;

        beforetag += weight;
        FourMomentum hadronic_candidate;
        FourMomentum leptonic_candidate;
        FourMomentum used_leptonicW;


        // we will assume the leading fat jet is the top candidate
        HEPTopTagger::HEPTopTagger tagger(_fatpseudojets[0]);

        // Unclustering, Filtering & Subjet Settings
        tagger.set_max_subjet_mass(30.);
        tagger.set_mass_drop_threshold(0.8);
        tagger.set_filtering_R(0.3);
        tagger.set_filtering_n(5);
        tagger.set_filtering_minpt_subjet(20.);

        // How to select among candidates
        tagger.set_mode(HEPTopTagger::EARLY_MASSRATIO_SORT_MASS);

        // Requirements to accept a candidate
        tagger.set_top_minpt(250);
        tagger.set_top_mass_range(130., 210.);
        tagger.set_fw(0.15);

        // Run the tagger
        tagger.run();

        if (tagger.is_tagged()){
          hadronic_candidate = FourMomentum(tagger.t().E(), tagger.t().px(), tagger.t().py(), tagger.t().pz());
          leptonic_candidate = abs( (w_leptonic[0] + _bjets[0]).mass() - 172.5) < abs( (w_leptonic[1] + _bjets[0]).mass() - 172.5)? w_leptonic[0] + _bjets[0] : w_leptonic[1] + _bjets[0];
          used_leptonicW = abs( (w_leptonic[0] + _bjets[0]).mass() - 172.5) < abs( (w_leptonic[1] + _bjets[0]).mass() - 172.5)? w_leptonic[0] : w_leptonic[1];
        } // end of boosted analysis
        else continue;

        // if(_fatjets[0].mass() < 140. || _fatjets[0].mass() > 210.) continue;

        /// This is for just jet mass top tagging, insufficient to kill Wjj
        // if(_fatjets[0].mass() < 100.) continue;
        // else {
        //   hadronic_candidate = _fatjets[0];
        //   leptonic_candidate = abs( (w_leptonic[0] + _bjets[0]).mass() - 172.5) < abs( (w_leptonic[1] + _bjets[0]).mass() - 172.5)? w_leptonic[0] + _bjets[0] : w_leptonic[1] + _bjets[0];
        //   used_leptonicW = abs( (w_leptonic[0] + _bjets[0]).mass() - 172.5) < abs( (w_leptonic[1] + _bjets[0]).mass() - 172.5)? w_leptonic[0] : w_leptonic[1];
        // }


        _cuts["g toptag"] += weight;
        aftertag += weight;

        if ( abs(leptonic_candidate.mass() - 172.5) > 40. )
          continue;

        _cuts["h masswindow"] += weight;

        _top1 = hadronic_candidate;
        _top2 = leptonic_candidate;

        passweight += weight;
//        cout << "passed" << endl;

//            cout << _top1.mass() << "         " << _top2.mass() << endl;

        MatrixElementAnalysis mea;
        TLorentzVector t(_top2.px(), _top2.py(), _top2.pz(), _top2.E());
        TLorentzVector tbar(_top1.px(), _top1.py(), _top1.pz(), _top1.E());
        TLorentzVector lp(_dressedleptons[0].px(), _dressedleptons[0].py(), _dressedleptons[0].pz(), _dressedleptons[0].E());
        // Need to plug in two leptons but this is a dummy, only use the leptonic side obvz
        TLorentzVector lm(_dressedleptons[0].px(), _dressedleptons[0].py(), _dressedleptons[0].pz(), _dressedleptons[0].E());
        mea.calculate(0., t, tbar, lp, lm);

        FourMomentum ttbar = _top1+_top2;
        if(ttbar.mass() < 2000.) continue;
        _cuts["i ttbar mass"] += weight;

        double afb = _top1.rapidity() - _top2.rapidity() > 0 ? weight : -weight;
        MatrixElementAnalysis mea_ttbar;
        TLorentzVector tt(ttbar.px(), ttbar.py(), ttbar.pz(), ttbar.E());
        mea_ttbar.calculate(0., tt, tbar, t, lm);


        _histos["HADRONICT_MASS"]->fill(_top1.mass(),weight);
        _histos["LEADINGJ_PT"]->fill(_jets[0].pT(),weight);
        _histos["LEPTONICT_MASS"]->fill(_top2.mass(),weight);
        _histos["LEPTONICW_MASS"]->fill( used_leptonicW.mass(), weight);

        _histos["TTBAR_MASS"]->fill(ttbar.mass(),weight);
        _histos["COSTHETAPROD"]->fill(cos(ttbar.theta()) ,weight);
        _histos["COSTHETATOP"]->fill(mea_ttbar.getHelicityTheta1() ,weight);
        _2dhistos["COSTHETAPROD_2D"]->fill(ttbar.mass(),cos(ttbar.theta()),weight);
        _2dhistos["COSTHETATOP_2D"]->fill(ttbar.mass(),mea_ttbar.getHelicityTheta1(),weight);
        _histos["AFB_PRENORM"]->fill(ttbar.mass(), afb);
        _2dhistos["AL_PRENORM"]->fill(ttbar.mass(), mea.getHelicityTheta1(),weight);
        _histos["AL_NOTBINNED"]->fill(mea.getHelicityTheta1(),weight);
      }
    }

    void finalize() {
      for(size_t i = 0; i < massbins; ++i) {
        double val = minmass + (maxmass-minmass)/(2*massbins) + i*(maxmass-minmass)/(massbins);
        if(_histos["TTBAR_MASS"]->bin(i).height() == 0)
          _histos["AFB"]->fill( val, 0);
        else
          _histos["AFB"]->fill( val, _histos["AFB_PRENORM"]->bin(i).height()/(2*_histos["TTBAR_MASS"]->bin(i).height())*100 );
      }
      for(size_t i = 0; i < massbins; ++i) {
        double val = minmass + (maxmass-minmass)/(2*massbins) + i*(maxmass-minmass)/(massbins);
        vector<double> vals(10,0);
        vector<double> xs(10,0);
        for(size_t j = 0; j < 10; ++j) {
          xs[j] = -1 + j*0.2 + 0.1;
          int index = _2dhistos["AL_PRENORM"]->binIndexAt(val,-1 + j*0.2 + 0.1);
          if(_histos["TTBAR_MASS"]->bin(i).height() == 0)
            vals[j] = 0;
          else
            vals[j] = _2dhistos["AL_PRENORM"]->bin(index).height()/_histos["TTBAR_MASS"]->bin(i).height();
        }
        double c0, c1, cov00, cov01, cov11, sumsq;
        gsl_fit_linear (xs.data(), 1, vals.data(), 1, 10, &c0, &c1, &cov00, &cov01, &cov11, &sumsq);
        cout << c1 << endl;
        _histos["AL"]->fill( val, c1 * 100 );
      }

      scale(_histos["HADRONICT_MASS"], crossSection()/totalweight );
      scale(_histos["LEADINGJ_PT"], crossSection()/totalweight );
      scale(_histos["LEPTONICT_MASS"], crossSection()/totalweight );
      scale(_histos["LEPTONICW_MASS"], crossSection()/totalweight );
      scale(_histos["TTBAR_MASS"], crossSection()/totalweight );
      scale(_histos["AL_PRENORM"], crossSection()/totalweight );
      scale(_histos["AL_NOTBINNED"], crossSection()/totalweight );
      delete ascii_in;
      cout << "Total passed cross section " << crossSection() * passweight/totalweight << endl;
      cout << "Integral of histogram " << _histos["TTBAR_MASS"]->integral(false) << endl;
      cout << "Passing rate " << (double) passweight/totalweight << endl;
      cout << "Top tagging rate " << (double) aftertag/beforetag << endl;
      cout << "Average number of b jets " << (double) nbjets/nevb << endl;
      cout << "Correct pairing " << (double) correct_pair/pairweight << endl;

      double norm = totalweight;
      for(auto const& ent1 : _cuts) {
        cout << "Weight for cut: " << ent1.first << " is " << ent1.second << " (efficiency: " << ent1.second/norm << " )" << endl;
        norm=ent1.second;
      }

    }

    //@}

  private:
    /// @name Objects that are used by the event selection decisions
    //@{
    vector<DressedLepton> _dressedelectrons;
    vector<DressedLepton> _dressedmuons;
    vector<DressedLepton> _dressedleptons;
    Particles _neutrinos;
    vector<FourMomentum> _top1c, _top2c;
    FourMomentum _top1,_top2;
    Jets _jets;
    Jets _bjets;
    unsigned int _jet_ntag;
    int massbins;
    double minmass;
    double maxmass;
    int cosbins;
    /// @todo Why not store the whole MET FourMomentum?
    double _met_et, _met_phi, totalweight, passweight, pairweight, correct_pair, beforetag, aftertag;
    bool _overlap;
    unsigned int nev,nbjets,nevb;

    std::map<std::string, Histo1DPtr> _histos;
    std::map<std::string, Histo2DPtr> _2dhistos;
    std::map<std::string, double> _cuts;
    HepMC::IO_GenEvent* ascii_in;
    HepMC::GenEvent* evt1;
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(TopPolarisation_Smearing_SemiLeptonic);

}
