// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/SmearedParticles.hh"
#include "Rivet/Projections/SmearedJets.hh"
#include "Rivet/Projections/SmearedMET.hh"
#include "TLorentzVector.h"
#include "TVector3.h"
#include "TVector2.h"
#include "MatrixElementAnalysis.h"
#include "lester_mt2_bisect.h"
#include <random>
#include <gsl/gsl_fit.h>


namespace Rivet {

  bool isBMeson(int pdgId){
    if( fabs(pdgId)==511 || fabs(pdgId)==531 || fabs(pdgId)==521
             || fabs(pdgId)==5122 || fabs(pdgId)==5232 ||  fabs(pdgId)==5132 || fabs(pdgId)==5332
             || fabs(pdgId)==5322 || fabs(pdgId)==5312 || fabs(pdgId)==533 || fabs(pdgId)==513
             || fabs(pdgId)==523  || fabs(pdgId)==541 || fabs(pdgId)==543
             || fabs(pdgId)==5114 || fabs(pdgId)==5212 || fabs(pdgId)==5224 || fabs(pdgId)==5222
             || fabs(pdgId)==5214 || fabs(pdgId)==5112 || fabs(pdgId)==5324 || fabs(pdgId)==5314
             || fabs(pdgId)==5334 )
      return true;
    return false;
  }

  double _transMass(double ptLep, double phiLep, double met, double phiMet) {
    return sqrt(2.0*ptLep*met*(1 - cos(phiLep-phiMet)));
  }

  HepMC::GenVertex::particle_iterator findLepton(HepMC::GenVertex::particle_iterator p, int id, FourMomentum *ret) {
    if( !(*p)->end_vertex() ) return p;
    for ( HepMC::GenVertex::particle_iterator daughter = (*p)->end_vertex()-> particles_begin(HepMC::descendants);
                      daughter != (*p)->end_vertex()-> particles_end(HepMC::descendants); ++daughter ) {
//      cout << (*daughter)->pdg_id() << endl;
      if((*daughter)->pdg_id() == id) {
//        cout << endl;
        FourMomentum tmp = FourMomentum((*daughter)->momentum().e(),(*daughter)->momentum().px(), (*daughter)->momentum().py(), (*daughter)->momentum().pz());
        *ret = tmp;
        return daughter;
      }
//      else if ((*daughter) != (*p)) { findLepton(daughter, id, ret); }
    }
    return p;
  }

  double KZ1(double Ep, double kxp, double kyp, double kzp, double mt, double kx, double ky) {
//    cout << "in KZ1" << endl;
    return (4*pow(Ep,2)*kzp - 8*kx*kxp*kzp -
      4*pow(kxp,2)*kzp - 8*ky*kyp*kzp - 4*pow(kyp,2)*kzp -
        4*pow(kzp,3) - 4*kzp*pow(mt,2) -
        sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
            4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
            4*pow(kyp,2)*kzp + 4*pow(kzp,3) + 4*kzp*pow(mt,2),
           2) - 4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
           (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
             4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
             4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
             pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
             4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
             4*pow(kxp,2)*ky*kyp - 2*pow(Ep,2)*pow(kyp,2) +
             4*kx*kxp*pow(kyp,2) + 2*pow(kxp,2)*pow(kyp,2) +
             4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
             pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
             4*kx*kxp*pow(kzp,2) + 2*pow(kxp,2)*pow(kzp,2) +
             4*ky*kyp*pow(kzp,2) + 2*pow(kyp,2)*pow(kzp,2) +
             pow(kzp,4) - 2*pow(Ep,2)*pow(mt,2) +
             4*kx*kxp*pow(mt,2) + 2*pow(kxp,2)*pow(mt,2) +
             4*ky*kyp*pow(mt,2) + 2*pow(kyp,2)*pow(mt,2) +
             2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
      (2.*(-4*pow(Ep,2) + 4*pow(kzp,2)));
  }

  double KZ2(double Ep, double kxp, double kyp, double kzp, double mt, double kx, double ky) {
//    cout << "in KZ2" << endl;
    return (4*pow(Ep,2)*kzp - 8*kx*kxp*kzp -
        4*pow(kxp,2)*kzp - 8*ky*kyp*kzp - 4*pow(kyp,2)*kzp -
        4*pow(kzp,3) - 4*kzp*pow(mt,2) +
        sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
            4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
            4*pow(kyp,2)*kzp + 4*pow(kzp,3) + 4*kzp*pow(mt,2),
           2) - 4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
           (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
             4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
             4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
             pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
             4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
             4*pow(kxp,2)*ky*kyp - 2*pow(Ep,2)*pow(kyp,2) +
             4*kx*kxp*pow(kyp,2) + 2*pow(kxp,2)*pow(kyp,2) +
             4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
             pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
             4*kx*kxp*pow(kzp,2) + 2*pow(kxp,2)*pow(kzp,2) +
             4*ky*kyp*pow(kzp,2) + 2*pow(kyp,2)*pow(kzp,2) +
             pow(kzp,4) - 2*pow(Ep,2)*pow(mt,2) +
             4*kx*kxp*pow(mt,2) + 2*pow(kxp,2)*pow(mt,2) +
             4*ky*kyp*pow(mt,2) + 2*pow(kyp,2)*pow(mt,2) +
             2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
      (2.*(-4*pow(Ep,2) + 4*pow(kzp,2)));
  }

  double ee1(double Ep, double kxp, double kyp, double kzp, double mt, double kx, double ky) {
//    cout << "in ee1" << endl;
    return (-pow(Ep,2) + 2*kx*kxp + pow(kxp,2) + 2*ky*kyp +
        pow(kyp,2) + pow(kzp,2) +
        (4*pow(Ep,2)*pow(kzp,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (8*kx*kxp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (4*pow(kxp,2)*pow(kzp,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (8*ky*kyp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (4*pow(kyp,2)*pow(kzp,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (4*pow(kzp,4))/(-4*pow(Ep,2) + 4*pow(kzp,2)) +
        pow(mt,2) - (4*pow(kzp,2)*pow(mt,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (kzp*sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
               4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
               4*pow(kyp,2)*kzp + 4*pow(kzp,3) +
               4*kzp*pow(mt,2),2) -
             4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
              (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
                4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
                4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
                pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
                4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
                4*pow(kxp,2)*ky*kyp -
                2*pow(Ep,2)*pow(kyp,2) +
                4*kx*kxp*pow(kyp,2) +
                2*pow(kxp,2)*pow(kyp,2) +
                4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
                pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
                4*kx*kxp*pow(kzp,2) +
                2*pow(kxp,2)*pow(kzp,2) +
                4*ky*kyp*pow(kzp,2) +
                2*pow(kyp,2)*pow(kzp,2) + pow(kzp,4) -
                2*pow(Ep,2)*pow(mt,2) + 4*kx*kxp*pow(mt,2) +
                2*pow(kxp,2)*pow(mt,2) + 4*ky*kyp*pow(mt,2) +
                2*pow(kyp,2)*pow(mt,2) +
                2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)))/(2.*Ep);
  }

  double ee2(double Ep, double kxp, double kyp, double kzp, double mt, double kx, double ky) {
//    cout << "in ee2" << endl;
    return (-pow(Ep,2) + 2*kx*kxp + pow(kxp,2) + 2*ky*kyp +
        pow(kyp,2) + pow(kzp,2) +
        (4*pow(Ep,2)*pow(kzp,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (8*kx*kxp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (4*pow(kxp,2)*pow(kzp,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (8*ky*kyp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (4*pow(kyp,2)*pow(kzp,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) -
        (4*pow(kzp,4))/(-4*pow(Ep,2) + 4*pow(kzp,2)) +
        pow(mt,2) - (4*pow(kzp,2)*pow(mt,2))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)) +
        (kzp*sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
               4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
               4*pow(kyp,2)*kzp + 4*pow(kzp,3) +
               4*kzp*pow(mt,2),2) -
             4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
              (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
                4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
                4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
                pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
                4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
                4*pow(kxp,2)*ky*kyp -
                2*pow(Ep,2)*pow(kyp,2) +
                4*kx*kxp*pow(kyp,2) +
                2*pow(kxp,2)*pow(kyp,2) +
                4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
                pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
                4*kx*kxp*pow(kzp,2) +
                2*pow(kxp,2)*pow(kzp,2) +
                4*ky*kyp*pow(kzp,2) +
                2*pow(kyp,2)*pow(kzp,2) + pow(kzp,4) -
                2*pow(Ep,2)*pow(mt,2) + 4*kx*kxp*pow(mt,2) +
                2*pow(kxp,2)*pow(mt,2) + 4*ky*kyp*pow(mt,2) +
                2*pow(kyp,2)*pow(mt,2) +
                2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
         (-4*pow(Ep,2) + 4*pow(kzp,2)))/(2.*Ep);
  }


  vector<double> BaumgartTweedie(TLorentzVector t, TLorentzVector tbar, TLorentzVector lp, TLorentzVector lm) {
//      const double pt = t.Pt() + tbar.Pt();

//      cout << t.Pt() << "   " << tbar.Pt() << "   " << lp.Pt() << "   " << lm.Pt() << endl;

      //boost to com
      TLorentzVector ttbar = t + tbar;
      TVector3 boost_to_ttbar = -1. * ttbar.BoostVector();

      t.Boost(boost_to_ttbar);
      tbar.Boost(boost_to_ttbar);
      lp.Boost(boost_to_ttbar);
      lm.Boost(boost_to_ttbar);

      //boost from ZMF to top frame
      TVector3 boost_to_top = -1. * t.BoostVector();
      lp.Boost(boost_to_top);

      //boost from ZMF to tbar frame
      TVector3 boost_to_tbar = -1. * tbar.BoostVector();
      lm.Boost(boost_to_tbar);

      TVector3 beamZ(0, 0, 1);

      TVector3 newZ = t.Vect().Unit();
      TVector3 newY = t.Vect().Unit().Cross(beamZ).Unit();
      TVector3 newX = newY.Cross(newZ).Unit();

      TRotation m;
      m.RotateAxes(newX, newY, newZ);
      m = m.Inverse();
      lp.Transform(m);
      lm.Transform(m);

      vector<double> ret;

      ret.push_back(TVector2::Phi_mpi_pi(lp.Phi() - lm.Phi()));
      ret.push_back(TVector2::Phi_mpi_pi(lp.Phi() + lm.Phi()));

      return ret;
  }

  /// @brief top polarisation
  class TopPolarisation_Smearing : public Analysis {
  public:

    /// Default constructor
    TopPolarisation_Smearing() : Analysis("TopPolarisation_Smearing")
    {    }


    /// @name Analysis methods
    //@{

    void init() {


      // Initialise and register projections
      FinalState calofs(Cuts::abseta < 4.8);

      MissingMomentum mm(calofs);
      declare(mm, "TruthMET");
      declare(SmearedMET(mm, MET_SMEAR_ATLAS_RUN2), "RecoMET");

      PromptFinalState es(Cuts::abseta < 2.47 && Cuts::abspid == PID::ELECTRON, true, true);
      declare(es, "TruthElectrons");
      declare(SmearedParticles(es, ELECTRON_EFF_ATLAS_RUN2, ELECTRON_SMEAR_ATLAS_RUN2), "RecoElectrons");

      PromptFinalState mus(Cuts::abseta < 2.7 && Cuts::abspid == PID::MUON, true);
      declare(mus, "TruthMuons");
      declare(SmearedParticles(mus, MUON_EFF_ATLAS_RUN2, MUON_SMEAR_ATLAS_RUN2), "RecoMuons");

      IdentifiedFinalState nu_id;
      nu_id.acceptNeutrinos();
      PromptFinalState neutrinos(nu_id);
      neutrinos.acceptTauDecays(true);
      addProjection(neutrinos, "neutrinos");

      VetoedFinalState vcalofs(calofs);
      vcalofs.addVetoOnThisFinalState(mus);
      vcalofs.addVetoOnThisFinalState(es);
      vcalofs.addVetoOnThisFinalState(neutrinos);
      FastJets fj(vcalofs, FastJets::ANTIKT, 0.4);
      declare(fj, "TruthJets");
      declare(SmearedJets(fj, JET_SMEAR_ATLAS_RUN2, JET_BTAG_ATLAS_RUN2_MV2C20), "RecoJets");

      massbins = 20;
      minmass = 2000.;
      maxmass = 6000.;
      cosbins = 10;


      _histos["DPHI_LPLM"]   = bookHisto1D("dphi_lplm", cosbins, 0, M_PI, "", "dphi(lp lm)", "Relative Occurence");
      _2dhistos["DPHI_LPLM_2D"]   = bookHisto2D("dphi_lplm2d", massbins, minmass, maxmass, cosbins, 0, M_PI, "", "dphi(lp lm)", "Relative Occurence");
      _histos["COSTHETACOSTHETA"]   = bookHisto1D("costhetacostheta", cosbins, -1, 1, "", "cos theta+ cos theta-", "Relative Occurence");
      _histos["COSTHETAPROD"]   = bookHisto1D("costhetaprod", cosbins, -1, 1, "", "cos theta prod", "Relative Occurence");
      _histos["COSTHETATOP"]   = bookHisto1D("costhetatop", cosbins, -1, 1, "", "cos theta top", "Relative Occurence");
      _2dhistos["COSTHETACOSTHETA_2D"]   = bookHisto2D("COSTHETACOSTHETA_2D", massbins, minmass, maxmass, cosbins,-1,1,"", "cos theta+ cos theta- mass binned", "Relative Occurence");
      _2dhistos["COSTHETAPROD_2D"]   = bookHisto2D("COSTHETAPROD_2D", massbins, minmass, maxmass, cosbins,-1,1,"", "cos theta prod mass binned", "Relative Occurence");
      _2dhistos["COSTHETATOP_2D"]   = bookHisto2D("COSTHETATOP_2D", massbins, minmass, maxmass, cosbins,-1,1,"", "cos theta top mass binned", "Relative Occurence");
      _histos["COSTHETACOSTHETA_UWER"]   = bookHisto1D("costhetacostheta_uwer", cosbins, -1, 1, "", "cos theta+ cos theta- Uwer basis", "Relative Occurence");
      _2dhistos["COSTHETACOSTHETA_UWER_2D"]   = bookHisto2D("COSTHETACOSTHETA_UWER_2D", massbins, minmass, maxmass, cosbins,-1,1,"", "cos theta+ cos theta- Uwer basis mass binned", "Relative Occurence");
      _histos["BT_DIFF"]   = bookHisto1D("bt_diff", cosbins, -M_PI, M_PI, "", "dphi BT", "Relative Occurence");
      _2dhistos["BT_DIFF_2D"]   = bookHisto2D("BT_DIFF_2D", massbins, minmass, maxmass, cosbins,-M_PI,M_PI,"", "dphi BT mass binned", "Relative Occurence");
      _histos["BT_SUM"]   = bookHisto1D("bt_sum", cosbins, -M_PI, M_PI, "", "sum phi BT", "Relative Occurence");
      _2dhistos["BT_SUM_2D"]   = bookHisto2D("BT_SUM_2D", massbins, minmass, maxmass, cosbins,-M_PI,M_PI,"", "sum phi BT mass binned", "Relative Occurence");
      _histos["LEADINGJ_PT"]   = bookHisto1D("leadingjetpt", 20, 0, 300, "", "leading jet pt", "Relative Occurence");
      _histos["LEADINGT_MASS"]   = bookHisto1D("top1mass", 20, 0, 300, "", "top1 mass", "Relative Occurence");
      _histos["SUBLEADINGT_MASS"]   = bookHisto1D("top2mass", 20, 0, 300, "", "top2 mass", "Relative Occurence");
      _histos["DKT"]   = bookHisto1D("dkt", 20, 0, 10, "", "delta kt", "Relative Occurence");
      _histos["DKL"]   = bookHisto1D("dkl", 20, -10, 10, "", "delta kl", "Relative Occurence");
      _histos["DKT_TOP"]   = bookHisto1D("dkt_top", 20, 0, 5, "", "delta kt top", "Relative Occurence");
      _histos["DKL_TOP"]   = bookHisto1D("dkl_top", 20, -5, 5, "", "delta kl top", "Relative Occurence");
      _histos["DKT_TBAR"]   = bookHisto1D("dkt_tbar", 20, 0, 5, "", "delta kt tbar", "Relative Occurence");
      _histos["DKL_TBAR"]   = bookHisto1D("dkl_tbar", 20, -5, 5, "", "delta kl tbar", "Relative Occurence");
      _histos["W+_MASS"]   = bookHisto1D("w+mass", 20, 0, 200, "", "w+ mass", "Relative Occurence");
      _histos["W-_MASS"]   = bookHisto1D("w-mass", 20, 0, 200, "", "w- mass", "Relative Occurence");
      _histos["TTBAR_MASS"]   = bookHisto1D("ttbarmass", massbins, minmass, maxmass, "", "ttbar mass", "Relative Occurence");
      _histos["AFB_PRENORM"]   = bookHisto1D("AFB_PRENORM", massbins, minmass, maxmass, "", "forward backward A pre norm", "Relative Occurence");
      _histos["AFB"]   = bookHisto1D("AFB", massbins, minmass, maxmass, "", "forward backward A", "Relative Occurence");
      _2dhistos["AL_PRENORM"]   = bookHisto2D("AL_PRENORM", massbins, minmass, maxmass, cosbins,-1,1,"", "lepton angle asymmetry pre norm", "Relative Occurence");
      _histos["AL"]   = bookHisto1D("AL", massbins, minmass, maxmass, "", "lepton angle asymmetry", "Relative Occurence");
      _histos["AL_NOTBINNED"]   = bookHisto1D("AL_NOTBINNED", cosbins, -1, 1, "", "lepton angle asymmetry", "Relative Occurence");

      nev = 0;
      nevb = 0;
      correct_pair = 0;
      nbjets = 0;
      totalweight = 0.;
      passweight = 0.;
      pairweight = 0.;
      _cuts["m init"] = 0;
      // specify an input file
      ascii_in = new HepMC::IO_GenEvent("herwig_noshower.hepmc",std::ios::in);
      // get the first event
      evt1 = ascii_in->read_next_event();
    }


    void analyze(const Event& event) {

      double weight = event.weight();
      const HepMC::GenEvent *evt = event.genEvent();
      _dressedleptons.clear();
      _bjets.clear();
      _top1c.clear();
      _top2c.clear();


      vector<FourMomentum> blp(3), blm(3);
      vector<FourMomentum> bMesons,bMestmp;
      unsigned int bs = 0;
      FourMomentum hard1, hard2, top,topbar;

      HepMC::GenEvent::particle_const_iterator e1 = evt->particles_end();
      for ( HepMC::GenEvent::particle_const_iterator p1 = evt->particles_begin(); p1 != e1; ++p1 )
      {
        if ( isBMeson( (*p1)->pdg_id() ) && (*p1)->status() == 1  )
          bMestmp.push_back(FourMomentum((*p1)->momentum().e(),(*p1)->momentum().px(), (*p1)->momentum().py(), (*p1)->momentum().pz()));
      }


//          cout << "in finder    " << evno1 << endl;
      HepMC::GenEvent::particle_const_iterator e = evt1->particles_end();
      for ( HepMC::GenEvent::particle_const_iterator p = evt1->particles_begin(); p != e; ++p )
      {
        if ( isBMeson( (*p)->pdg_id() ) && (*p)->status() == 1  )
          bMestmp.push_back(FourMomentum((*p)->momentum().e(),(*p)->momentum().px(), (*p)->momentum().py(), (*p)->momentum().pz()));
        if ( abs( (*p)->pdg_id() ) == 5 )
          bs++;


        if ( (*p)->pdg_id()==6) {
          top = FourMomentum((*p)->momentum().e(),(*p)->momentum().px(), (*p)->momentum().py(), (*p)->momentum().pz());
          if ( (*p)->end_vertex() )
          {
            for ( HepMC::GenVertex::particle_iterator daughter = (*p)->end_vertex()-> particles_begin(HepMC::descendants);
                  daughter != (*p)->end_vertex()-> particles_end(HepMC::descendants); ++daughter )
            {
//              if ((*daughter)->pdg_id() == 5) { blp.push_back(FourMomentum((*daughter)->momentum().e(),(*daughter)->momentum().px(), (*daughter)->momentum().py(), (*daughter)->momentum().pz()));  break;}
              if ((*daughter)->pdg_id() == 5) { blp[0]=FourMomentum((*daughter)->momentum().e(),(*daughter)->momentum().px(), (*daughter)->momentum().py(), (*daughter)->momentum().pz());  break;}
            }
            for ( HepMC::GenVertex::particle_iterator daughter = (*p)->end_vertex()-> particles_begin(HepMC::descendants);
                  daughter != (*p)->end_vertex()-> particles_end(HepMC::descendants); ++daughter )
            {
              if ( (*daughter)->pdg_id() == 24 && (*daughter)->end_vertex() ) {
                FourMomentum lepton1(0.,0.,0.,0.);
                FourMomentum lepton2(0.,0.,0.,0.);
                HepMC::GenVertex::particle_iterator lep1, lep2;
                lep1 = findLepton(daughter, -11, &lepton1);
                lep2 = findLepton(daughter, 12, &lepton2);
                if(lep1==lep2) {
                  lep1 = findLepton(daughter, -13, &lepton1);
                  lep2 = findLepton(daughter, 14, &lepton2);
                }
                if(lep1!=lep2) {
//                  blp.push_back(lepton1);
//                  blp.push_back(lepton2);
                  blp[1]=lepton1;
                  blp[2]=lepton2;
                }
                break;
              }
            }
          }
        }

        if ( (*p)->pdg_id()==-6) {
          topbar = FourMomentum((*p)->momentum().e(),(*p)->momentum().px(), (*p)->momentum().py(), (*p)->momentum().pz());
          if ( (*p)->end_vertex() )
          {
            for ( HepMC::GenVertex::particle_iterator daughter = (*p)->end_vertex()-> particles_begin(HepMC::descendants);
                  daughter != (*p)->end_vertex()-> particles_end(HepMC::descendants); ++daughter )
            {
//              if ((*daughter)->pdg_id() == -5) { blm.push_back(FourMomentum((*daughter)->momentum().e(),(*daughter)->momentum().px(), (*daughter)->momentum().py(), (*daughter)->momentum().pz())); break;}
              if ((*daughter)->pdg_id() == -5) { blm[0]=FourMomentum((*daughter)->momentum().e(),(*daughter)->momentum().px(), (*daughter)->momentum().py(), (*daughter)->momentum().pz()); break;}
            }
            for ( HepMC::GenVertex::particle_iterator daughter = (*p)->end_vertex()-> particles_begin(HepMC::descendants);
                  daughter != (*p)->end_vertex()-> particles_end(HepMC::descendants); ++daughter )
            {
              if ( (*daughter)->pdg_id() == -24  ) {
                FourMomentum lepton1(0.,0.,0.,0.);
                FourMomentum lepton2(0.,0.,0.,0.);
                HepMC::GenVertex::particle_iterator lep1, lep2;
                lep1 = findLepton(daughter, 11, &lepton1);
                lep2 = findLepton(daughter, -12, &lepton2);
                if(lep1 == lep2 ) {
                  lep1 = findLepton(daughter, 13, &lepton1);
                  lep2 = findLepton(daughter, -14, &lepton2);
                }
                if(lep1 != lep2) {
//                  blm.push_back(lepton1);
//                  blm.push_back(lepton2);
                  blm[1]=lepton1;
                  blm[2]=lepton2;
                }
                break;
              }
            }
          }
        }
      }
      nev++;
//      delete evt1;
//      if(nev != 1000000)
//        *ascii_in >> evt1;
//
//      if(nev == 195394) {
//        delete evt1;
//        *ascii_in >> evt1;
//      }


//      cout << blp.size() << "       " << blm.size() << endl;
//      cout << (blm[0]+blm[1]+blm[2]).mass() << endl;


      if(blp.size() < 3 || blm.size() < 3) { cout << "veto" << endl; vetoEvent; }

      for(size_t i = 0; i < bMestmp.size(); ++i) {
        if ( bMestmp[i].pT() > 10. ) bMesons.push_back(bMestmp[i]);
      }
//      cout << bMesons.size() << endl;


      map<int,FourMomentum> bMes;
      for(unsigned int i = 0; i < bMesons.size(); ++i) {
        bMes[i] = bMesons[i];
      }


      if (nev % 1000 == 999) {
        cout << "Passing rate " << (double) passweight/totalweight << endl;
        cout << "Correct pairing " << (double) correct_pair/pairweight << endl;
      }

      Particles _dressedelectrons = apply<ParticleFinder>(event, "RecoElectrons").particlesByPt(Cuts::pT > 10*GeV);
      Particles _dressedmuons = apply<ParticleFinder>(event, "RecoMuons").particlesByPt(Cuts::pT > 10*GeV);
      Jets _jets = apply<JetAlg>(event, "RecoJets").jetsByPt(Cuts::pT > 20*GeV && Cuts::abseta < 2.8); ///< @todo Pile-up subtraction
      // Same MET cut for all signal regions
      //const Vector3 vmet = -apply<MissingMomentum>(event, "TruthMET").vectorEt();
      const Vector3 vmet = -apply<SmearedMET>(event, "RecoMET").vectorEt();
      FourMomentum pmet = FourMomentum(vmet.perp(), vmet.x(), vmet.y(), 0.);

      ///
      ///
      ///
      ///


      /// Use truth b-jet+lepton pairings (with truth four momenta)?
      bool truth = false;

      /// Use MT2 reconstructed neutrino guesses (otherwise truth neutrinos)?
      bool mt2reconstruct = true;


      ///
      ///
      ///
      ///


      if(truth && !mt2reconstruct) {

            _top1 = blp[0]+blp[1]+blp[2];
            _top2 = blm[0]+blm[1]+blm[2];
            _top1c.push_back(blp[0]);
            _top1c.push_back(blp[1]);
            _top1c.push_back(blp[2]);
            _top2c.push_back(blm[0]);
            _top2c.push_back(blm[1]);
            _top2c.push_back(blm[2]);


            MatrixElementAnalysis mea;
            TLorentzVector t(_top1.px(), _top1.py(), _top1.pz(), _top1.E());
            TLorentzVector lp(_top1c[1].px(), _top1c[1].py(), _top1c[1].pz(), _top1c[1].E());
            TLorentzVector tbar(_top2.px(), _top2.py(), _top2.pz(), _top2.E());
            TLorentzVector lm(_top2c[1].px(), _top2c[1].py(), _top2c[1].pz(), _top2c[1].E());
            mea.calculate(0., t, tbar, lp, lm);

            vector<double> bts = BaumgartTweedie(t, tbar, lp, lm);

            FourMomentum ttbar = _top1+_top2;
            double afb = _top1.rapidity() - _top2.rapidity() > 0 ? weight : -weight;


            _histos["LEADINGT_MASS"]->fill(_top1.mass(),weight);
            _histos["SUBLEADINGT_MASS"]->fill(_top2.mass(),weight);
            _histos["W+_MASS"]->fill( (_top1c[1]+_top1c[2]).mass(), weight);
            _histos["W-_MASS"]->fill( (_top2c[1]+_top2c[2]).mass(), weight);


            _histos["DPHI_LPLM"]->fill(fabs(lp.DeltaPhi(lm)),weight);
            _histos["COSTHETACOSTHETA"]->fill(mea.getHelicityTheta1() * mea.getHelicityTheta2(),weight);
            _histos["COSTHETACOSTHETA_UWER"]->fill(mea.getLHCOptimisedTheta1() * mea.getLHCOptimisedTheta2(),weight);

            _histos["BT_DIFF"]->fill(bts[0],weight);
            _histos["BT_SUM"]->fill(bts[1],weight);

            _histos["TTBAR_MASS"]->fill(ttbar.mass(),weight);
            _histos["AFB_PRENORM"]->fill(ttbar.mass(), afb);
            _2dhistos["AL_PRENORM"]->fill(ttbar.mass(), mea.getHelicityTheta1(),weight);
            _2dhistos["AL_PRENORM"]->fill(ttbar.mass(), mea.getHelicityTheta2(),weight);
            _histos["AL_NOTBINNED"]->fill(mea.getHelicityTheta1(),weight);
            _histos["AL_NOTBINNED"]->fill(mea.getHelicityTheta2(),weight);

            vetoEvent;
      }

        /// We don't do this to allow boosted configurations.
      // Jet/electron/muons overlap removal and selection
      // Remove any |eta| < 2.8 jet within dR = 0.2 of a baseline electron
//      for (const Particle& e : _dressedelectrons)
//        ifilter_discard(_jets, deltaRLess(e, 0.2, RAPIDITY));
      // Remove any electron or muon with dR < 0.4 of a remaining (Nch > 3) jet
//      for (const Jet& j : _jets) {
//        /// @todo Add track efficiency random filtering
//        ifilter_discard(_dressedelectrons, deltaRLess(j, 0.4, RAPIDITY));
//        if (j.particles(Cuts::abscharge > 0 && Cuts::pT > 500*MeV).size() >= 3)
//          ifilter_discard(_dressedmuons, deltaRLess(j, 0.4, RAPIDITY));
//      }
      // Discard the softer of any electrons within dR < 0.05
      for (size_t i = 0; i < _dressedelectrons.size(); ++i) {
        const Particle& e1 = _dressedelectrons[i];
        /// @todo Would be nice to pass a "tail view" for the filtering, but awkward without range API / iterator guts
        ifilter_discard(_dressedelectrons, [&](const Particle& e2){ return e2.pT() < e1.pT() && deltaR(e1,e2) < 0.05; });
      }

      _met_et = pmet.pT();
      _met_phi = pmet.phi();

      std::random_device rd;
      std::mt19937 generator (rd());

      std::uniform_real_distribution<double> dis(0.0, 1.0);


//      cout << "start of loop" << endl;

      for(unsigned int ev = 0; ev < 1; ++ev) {

        totalweight += weight;
        _cuts["a total"] += weight;

        if ( _jets.size() < 2 ) continue;

        _cuts["b jetmult"] += weight;

        unsigned int i,j;
        _jet_ntag = 0;

        pmet.setPz(0.);
        pmet.setE(0.);

        vector<int> used;
        Jets btmps;

        for(j = 0; j < bMes.size(); ++j) {
          double mindR = 1000.;
          int ind = -1;
          for(i = 0; i < _jets.size(); ++i) {
            if( deltaR(_jets[i], bMes[j]) < mindR ) {
              mindR = deltaR(_jets[i], bMes[j]);
              ind = i;
            }
          }
          if (mindR < 0.4) {
            if(std::find(used.begin(), used.end(), ind) != used.end()) {
              continue;
            } else {
              used.push_back(ind);
              btmps.push_back(_jets[ind]);
              _jet_ntag++;
            }
          }
        }

        for(i = 0; i < _jets.size(); ++i) {
          double rand = dis(generator);
          if(std::find(used.begin(), used.end(), i) != used.end()) {
            if(rand < 0.7) _bjets.push_back(_jets[i]);
          }
          else if (rand < 0.01 || (_jets[i].momentum().pT() > 300. && rand < 0.1) ) _bjets.push_back(_jets[i]);
        }

  //      cout << _bjets.size() << endl;
  //      cout << _jets.size() << endl;
  //      cout << endl;
        nevb++;
        nbjets+=_bjets.size();

        // need two leptons
        if(_dressedelectrons.size() + _dressedmuons.size() < 2 ) continue;
        _cuts["c leptons"] += weight;
        foreach(const DressedLepton& el, _dressedelectrons) {
          _dressedleptons.push_back(el);
        }
        foreach(const DressedLepton& mu, _dressedmuons) {
          _dressedleptons.push_back(mu);
        }
        sortByPt(_dressedleptons);
        // veto same signed leptons
        if(_dressedleptons[0].charge() * _dressedleptons[1].charge() > 0) continue;
        _cuts["d leptoncharge"] += weight;
        // want two jets with b tags
        if(_bjets.size() < 2) continue;
        _cuts["e bjetmult"] += weight;
        // want et_miss > 60 GeV
        if(_met_et < 60.) continue;
        _cuts["f met"] += weight;
        // mtt_T2 cut, see 1109.2201v3
        FourMomentum visible = _dressedleptons[0].momentum() + _dressedleptons[1].momentum() + _bjets[0].momentum() + _bjets[1].momentum();
        double mtt_T2 = visible.mass2() + 2 * (sqrt(visible.pT() * visible.pT() + visible.mass2()) * _met_et -  visible.dot(pmet));
        if( sqrt(mtt_T2) < 277. ) continue;
        _cuts["g mttT2"] += weight;

        double mVisA1 = (_dressedleptons[0].momentum()+_bjets[0].momentum()).mass(); // mass of visible object on side A.  Must be >=0.
        double pxA1 = (_dressedleptons[0].momentum()+_bjets[0].momentum()).px(); // x momentum of visible object on side A.
        double pyA1 = (_dressedleptons[0].momentum()+_bjets[0].momentum()).py(); // y momentum of visible object on side A.

        double mVisB1 = (_dressedleptons[1].momentum()+_bjets[1].momentum()).mass(); // mass of visible object on side B.  Must be >=0.
        double pxB1 = (_dressedleptons[1].momentum()+_bjets[1].momentum()).px(); // x momentum of visible object on side B.
        double pyB1 = (_dressedleptons[1].momentum()+_bjets[1].momentum()).py(); // y momentum of visible object on side B.

        double pxMiss1 = pmet.px(); // x component of missing transverse momentum.
        double pyMiss1 = pmet.py(); // y component of missing transverse momentum.

        double chiA = 0.; // hypothesised mass of invisible on side A.  Must be >=0.
        double chiB = 0.; // hypothesised mass of invisible on side B.  Must be >=0.

        double desiredPrecisionOnMt2 = 0.01; // Must be >=0.  If 0 alg aims for machine precision.  if >0, MT2 computed to supplied absolute precision.

        asymm_mt2_lester_bisect::disableCopyrightMessage();

        double MT2_1 =  asymm_mt2_lester_bisect::get_mT2(
                   mVisA1, pxA1, pyA1,
                   mVisB1, pxB1, pyB1,
                   pxMiss1, pyMiss1,
                   chiA, chiB,
                   desiredPrecisionOnMt2);

        double mVisA2 = (_dressedleptons[0].momentum()+_bjets[1].momentum()).mass(); // mass of visible object on side A.  Must be >=0.
        double pxA2 = (_dressedleptons[0].momentum()+_bjets[1].momentum()).px(); // x momentum of visible object on side A.
        double pyA2 = (_dressedleptons[0].momentum()+_bjets[1].momentum()).py(); // y momentum of visible object on side A.

        double mVisB2 = (_dressedleptons[1].momentum()+_bjets[0].momentum()).mass(); // mass of visible object on side B.  Must be >=0.
        double pxB2 = (_dressedleptons[1].momentum()+_bjets[0].momentum()).px(); // x momentum of visible object on side B.
        double pyB2 = (_dressedleptons[1].momentum()+_bjets[0].momentum()).py(); // y momentum of visible object on side B.

        double pxMiss2 = pmet.px(); // x component of missing transverse momentum.
        double pyMiss2 = pmet.py(); // y component of missing transverse momentum.

        double MT2_2 =  asymm_mt2_lester_bisect::get_mT2(
                   mVisA2, pxA2, pyA2,
                   mVisB2, pxB2, pyB2,
                   pxMiss2, pyMiss2,
                   chiA, chiB,
                   desiredPrecisionOnMt2);

        bool usefirst = true;
        bool usesecond = true;

        if ( (_dressedleptons[0].momentum() + _bjets[0].momentum()).mass() > 153.2 || (_dressedleptons[1].momentum() + _bjets[1].momentum()).mass() > 153.2 || MT2_1 > 172.5 || MT2_1 == asymm_mt2_lester_bisect::MT2_ERROR )
          usefirst = false;

        if ( (_dressedleptons[0].momentum() + _bjets[1].momentum()).mass() > 153.2 || (_dressedleptons[1].momentum() + _bjets[0].momentum()).mass() > 153.2 || MT2_2 > 172.5 || MT2_2 == asymm_mt2_lester_bisect::MT2_ERROR )
          usesecond = false;

        while(true) {

          std::pair <double,double> bestsol_1 = ben_findsols(MT2_1, pxA1, pyA1, mVisA1, chiA, pxB1, pyB1, pxMiss1, pyMiss1, mVisB1, chiB);
          std::pair <double,double> bestsol_2 = ben_findsols(MT2_2, pxA2, pyA2, mVisA2, chiA, pxB2, pyB2, pxMiss2, pyMiss2, mVisB2, chiB);

          double Ep = (_dressedleptons[0].momentum()+_bjets[0].momentum()).E();
          double kxp = (_dressedleptons[0].momentum()+_bjets[0].momentum()).px();
          double kyp = (_dressedleptons[0].momentum()+_bjets[0].momentum()).py();
          double kzp = (_dressedleptons[0].momentum()+_bjets[0].momentum()).pz();
          double mt = 172.5;
          double kx = bestsol_1.first;
          double ky = bestsol_1.second;

          double kz1 = KZ1(Ep,kxp,kyp,kzp,mt,kx,ky);
          double EE1 = ee1(Ep,kxp,kyp,kzp,mt,kx,ky);

          double kz2 = KZ2(Ep,kxp,kyp,kzp,mt,kx,ky);
          double EE2 = ee2(Ep,kxp,kyp,kzp,mt,kx,ky);

          if(EE1 != EE1 || EE2 != EE2 || kz1 != kz1 || kz2 != kz2) { usefirst = false; break; }

          vector<FourMomentum> MAOS11;
          MAOS11.push_back(FourMomentum(EE1, kx, ky, kz1));
          MAOS11.push_back(FourMomentum(EE2, kx, ky, kz2));

          Ep = (_dressedleptons[1].momentum()+_bjets[1].momentum()).E();
          kxp = (_dressedleptons[1].momentum()+_bjets[1].momentum()).px();
          kyp = (_dressedleptons[1].momentum()+_bjets[1].momentum()).py();
          kzp = (_dressedleptons[1].momentum()+_bjets[1].momentum()).pz();
          mt = 172.5;
          kx = pmet.px() - bestsol_1.first;
          ky = pmet.py() - bestsol_1.second;

          kz1 = KZ1(Ep,kxp,kyp,kzp,mt,kx,ky);
          EE1 = ee1(Ep,kxp,kyp,kzp,mt,kx,ky);

          kz2 = KZ2(Ep,kxp,kyp,kzp,mt,kx,ky);
          EE2 = ee2(Ep,kxp,kyp,kzp,mt,kx,ky);

          if(EE1 != EE1 || EE2 != EE2 || kz1 != kz1 || kz2 != kz2) { usefirst = false; break; }

          vector<FourMomentum> MAOS12;
          MAOS12.push_back(FourMomentum(EE1, kx, ky, kz1));
          MAOS12.push_back(FourMomentum(EE2, kx, ky, kz2));

           Ep = (_dressedleptons[0].momentum()+_bjets[1].momentum()).E();
           kxp = (_dressedleptons[0].momentum()+_bjets[1].momentum()).px();
           kyp = (_dressedleptons[0].momentum()+_bjets[1].momentum()).py();
           kzp = (_dressedleptons[0].momentum()+_bjets[1].momentum()).pz();
           mt = 172.5;
           kx = bestsol_2.first;
           ky = bestsol_2.second;

           kz1 = KZ1(Ep,kxp,kyp,kzp,mt,kx,ky);
           EE1 = ee1(Ep,kxp,kyp,kzp,mt,kx,ky);

           kz2 = KZ2(Ep,kxp,kyp,kzp,mt,kx,ky);
           EE2 = ee2(Ep,kxp,kyp,kzp,mt,kx,ky);

          if(EE1 != EE1 || EE2 != EE2 || kz1 != kz1 || kz2 != kz2) { usesecond = false; break; }

          vector<FourMomentum> MAOS21;
          MAOS21.push_back(FourMomentum(EE1, kx, ky, kz1));
          MAOS21.push_back(FourMomentum(EE2, kx, ky, kz2));

          Ep = (_dressedleptons[1].momentum()+_bjets[0].momentum()).E();
          kxp = (_dressedleptons[1].momentum()+_bjets[0].momentum()).px();
          kyp = (_dressedleptons[1].momentum()+_bjets[0].momentum()).py();
          kzp = (_dressedleptons[1].momentum()+_bjets[0].momentum()).pz();
          mt = 172.5;
          kx = pmet.px() - bestsol_2.first;
          ky = pmet.py() - bestsol_2.second;

          kz1 = KZ1(Ep,kxp,kyp,kzp,mt,kx,ky);
          EE1 = ee1(Ep,kxp,kyp,kzp,mt,kx,ky);

          kz2 = KZ2(Ep,kxp,kyp,kzp,mt,kx,ky);
          EE2 = ee2(Ep,kxp,kyp,kzp,mt,kx,ky);

          if(EE1 != EE1 || EE2 != EE2 || kz1 != kz1 || kz2 != kz2) { usesecond = false; break; }

          vector<FourMomentum> MAOS22;
          MAOS22.push_back(FourMomentum(EE1, kx, ky, kz1));
          MAOS22.push_back(FourMomentum(EE2, kx, ky, kz2));

          int count1 = 0;
          int count2 = 0;

          if (MT2_1 <= MT2_2) count1++;
          else count2++;

          double t3_1 = 0,t3_2 = 0,t4_1 = 0,t4_2 = 0;
          t3_1 += abs( (_dressedleptons[0].momentum() + _bjets[0].momentum() + MAOS11[0]).mass() - 172.5 );
          t3_1 += abs( (_dressedleptons[0].momentum() + _bjets[0].momentum() + MAOS11[1]).mass() - 172.5 );
          t3_1 += abs( (_dressedleptons[1].momentum() + _bjets[1].momentum() + MAOS12[0]).mass() - 172.5 );
          t3_1 += abs( (_dressedleptons[1].momentum() + _bjets[1].momentum() + MAOS12[1]).mass() - 172.5 );

          t3_2 += abs( (_dressedleptons[0].momentum() + _bjets[1].momentum() + MAOS21[0]).mass() - 172.5 );
          t3_2 += abs( (_dressedleptons[0].momentum() + _bjets[1].momentum() + MAOS21[1]).mass() - 172.5 );
          t3_2 += abs( (_dressedleptons[1].momentum() + _bjets[0].momentum() + MAOS22[0]).mass() - 172.5 );
          t3_2 += abs( (_dressedleptons[1].momentum() + _bjets[0].momentum() + MAOS22[1]).mass() - 172.5 );

          t4_1 += abs( (_dressedleptons[0].momentum() + MAOS11[0]).mass() - 80.4 );
          t4_1 += abs( (_dressedleptons[0].momentum() + MAOS11[1]).mass() - 80.4 );
          t4_1 += abs( (_dressedleptons[1].momentum() + MAOS12[0]).mass() - 80.4 );
          t4_1 += abs( (_dressedleptons[1].momentum() + MAOS12[1]).mass() - 80.4 );

          t4_2 += abs( (_dressedleptons[0].momentum() + MAOS21[0]).mass() - 80.4 );
          t4_2 += abs( (_dressedleptons[0].momentum() + MAOS21[1]).mass() - 80.4 );
          t4_2 += abs( (_dressedleptons[1].momentum() + MAOS22[0]).mass() - 80.4 );
          t4_2 += abs( (_dressedleptons[1].momentum() + MAOS22[1]).mass() - 80.4 );

          if (t3_1 <= t3_2) count1++;
          else count2++;

          if (t4_1 <= t4_2) count1++;
          else count2++;

          if (count1 > count2) usesecond = false;
          else usefirst = false;

          break;
        }

        if(!usefirst && !usesecond && !truth)
          continue;
        _cuts["h pairing"] += weight;
//        cout << "found pair" << endl;

        if(!truth) {
          if( usefirst ) {
//            cout << "should be 0: " <<_top1c.size() << endl;
            if(_dressedleptons[0].charge() > 0) {
              _top1 = _dressedleptons[0].momentum() + _bjets[0].momentum();
              _top1c.push_back(_bjets[0].momentum()); _top1c.push_back(_dressedleptons[0].momentum());
              _top2 = _dressedleptons[1].momentum() + _bjets[1].momentum();
              _top2c.push_back(_bjets[1].momentum()); _top2c.push_back(_dressedleptons[1].momentum());
            }
            else {
              _top2 = _dressedleptons[0].momentum() + _bjets[0].momentum();
              _top2c.push_back(_bjets[0].momentum()); _top2c.push_back(_dressedleptons[0].momentum());
              _top1 = _dressedleptons[1].momentum() + _bjets[1].momentum();
              _top1c.push_back(_bjets[1].momentum()); _top1c.push_back(_dressedleptons[1].momentum());
            }
          }
          else {
            if(_dressedleptons[0].charge() > 0) {
              _top1 = _dressedleptons[0].momentum() + _bjets[1].momentum();
              _top1c.push_back(_bjets[1].momentum()); _top1c.push_back(_dressedleptons[0].momentum());
              _top2 = _dressedleptons[1].momentum() + _bjets[0].momentum();
              _top2c.push_back(_bjets[0].momentum()); _top2c.push_back(_dressedleptons[1].momentum());
            }
            else {
              _top2 = _dressedleptons[0].momentum() + _bjets[1].momentum();
              _top2c.push_back(_bjets[1].momentum()); _top2c.push_back(_dressedleptons[0].momentum());
              _top1 = _dressedleptons[1].momentum() + _bjets[0].momentum();
              _top1c.push_back(_bjets[0].momentum()); _top1c.push_back(_dressedleptons[1].momentum());
            }
          }
        }
        else {
            _top1 = blp[0] + blp[1];
            _top1c.clear();
            _top1c.push_back(blp[0]);
            _top1c.push_back(blp[1]);

            _top2 = blm[0] + blm[1];
            _top2c.clear();
            _top2c.push_back(blm[0]);
            _top2c.push_back(blm[1]);
        }

        pairweight += weight;

//        cout << deltaR(blp[0],_top1c[1]) + deltaR(blm[0],_top2c[1]) << "      " << deltaR(blm[0],_top1c[1]) + deltaR(blp[0],_top2c[1]) << endl;

        if ( deltaR(blp[0],_top1c[0]) + deltaR(blm[0],_top2c[0]) + deltaR(blp[1],_top1c[1]) + deltaR(blm[1],_top2c[1]) < deltaR(blm[0],_top1c[0]) + deltaR(blp[0],_top2c[0]) + deltaR(blm[1],_top1c[1]) + deltaR(blp[1],_top2c[1]) )
          {
            correct_pair += weight;
//            cout << "correct: " << nev << endl;
          }
//        else {cout << "wrong: " << nev << endl; }


        double mVisA = _top1.mass(); // mass of visible object on side A.  Must be >=0.
        double pxA = _top1.px(); // x momentum of visible object on side A.
        double pyA = _top1.py(); // y momentum of visible object on side A.

        double mVisB = _top2.mass(); // mass of visible object on side B.  Must be >=0.
        double pxB = _top2.px(); // x momentum of visible object on side B.
        double pyB = _top2.py(); // y momentum of visible object on side B.

        double pxMiss = pmet.px(); // x component of missing transverse momentum.
        double pyMiss = pmet.py(); // y component of missing transverse momentum.

        double MT2 =  asymm_mt2_lester_bisect::get_mT2(
                   mVisA, pxA, pyA,
                   mVisB, pxB, pyB,
                   pxMiss, pyMiss,
                   chiA, chiB,
                   desiredPrecisionOnMt2);

        std::pair <double,double> bestsol = ben_findsols(MT2, pxA, pyA, mVisA, chiA, pxB, pyB, pxMiss, pyMiss, mVisB, chiB);

        double Ep = _top1.E();
        double kxp = _top1.px();
        double kyp = _top1.py();
        double kzp = _top1.pz();
        double mt = 172.5;
        double kx = bestsol.first;
        double ky = bestsol.second;

        double kz1 = KZ1(Ep,kxp,kyp,kzp,mt,kx,ky);
        double EE1 = ee1(Ep,kxp,kyp,kzp,mt,kx,ky);

        double kz2 = KZ2(Ep,kxp,kyp,kzp,mt,kx,ky);
        double EE2 = ee2(Ep,kxp,kyp,kzp,mt,kx,ky);

        if(EE1 != EE1 || EE2 != EE2 || kz1 != kz1 || kz2 != kz2) continue;

        vector<FourMomentum> MAOS1;
        MAOS1.push_back(FourMomentum(EE1, kx, ky, kz1));
        MAOS1.push_back(FourMomentum(EE2, kx, ky, kz2));

        Ep = _top2.E();
        kxp = _top2.px();
        kyp = _top2.py();
        kzp = _top2.pz();
        mt = 172.5;
        kx = pmet.px() - bestsol.first;
        ky = pmet.py() - bestsol.second;

        kz1 = KZ1(Ep,kxp,kyp,kzp,mt,kx,ky);
        EE1 = ee1(Ep,kxp,kyp,kzp,mt,kx,ky);

        kz2 = KZ2(Ep,kxp,kyp,kzp,mt,kx,ky);
        EE2 = ee2(Ep,kxp,kyp,kzp,mt,kx,ky);

        if(EE1 != EE1 || EE2 != EE2 || kz1 != kz1 || kz2 != kz2) continue;

        vector<FourMomentum> MAOS2;
        MAOS2.push_back(FourMomentum(EE1, kx, ky, kz1));
        MAOS2.push_back(FourMomentum(EE2, kx, ky, kz2));


//        cout << MAOS1[0].pT() << "      " << MAOS1[1].pT() << "       " << MAOS2[0].pT() << "       " << MAOS2[1].pT() << endl;
//        cout << blm[2].pT() << "      " << blp[2].pT() << endl;
//
//        cout << MAOS1[0].pz() << "      " << MAOS1[1].pz() << "       " << MAOS2[0].pz() << "       " << MAOS2[1].pz() << endl;
//        cout << blm[2].pz() << "      " << blp[2].pz() << endl;

        if( (MAOS1[0] - blm[2]).pT() + (MAOS2[0] - blp[2]).pT() < (MAOS1[0] - blp[2]).pT() + (MAOS2[0]- blm[2]).pT() ) {
          _histos["DKT"]->fill( (MAOS1[0] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[0] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[0] - blm[2]).pz()/abs(blm[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[0] - blp[2]).pz()/abs(blp[2].pz()),weight);
        }
        else {
          _histos["DKT"]->fill( (MAOS1[0] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[0] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[0] - blp[2]).pz()/abs(blp[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[0] - blm[2]).pz()/abs(blm[2].pz()),weight);
        }
        if( (MAOS1[0] - blm[2]).pT() + (MAOS2[1] - blp[2]).pT() < (MAOS1[0] - blp[2]).pT() + (MAOS2[1]- blm[2]).pT() ) {
          _histos["DKT"]->fill( (MAOS1[0] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[1] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[0] - blm[2]).pz()/abs(blm[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[1] - blp[2]).pz()/abs(blp[2].pz()),weight);
        }
        else {
          _histos["DKT"]->fill( (MAOS1[0] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[1] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[0] - blp[2]).pz()/abs(blp[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[1] - blm[2]).pz()/abs(blm[2].pz()),weight);
        }
        if( (MAOS1[1] - blm[2]).pT() + (MAOS2[0] - blp[2]).pT() < (MAOS1[1] - blp[2]).pT() + (MAOS2[0]- blm[2]).pT() ) {
          _histos["DKT"]->fill( (MAOS1[1] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[0] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[1] - blm[2]).pz()/abs(blm[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[0] - blp[2]).pz()/abs(blp[2].pz()),weight);
        }
        else {
          _histos["DKT"]->fill( (MAOS1[1] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[0] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[1] - blp[2]).pz()/abs(blp[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[0] - blm[2]).pz()/abs(blm[2].pz()),weight);
        }
        if( (MAOS1[1] - blm[2]).pT() + (MAOS2[1] - blp[2]).pT() < (MAOS1[1] - blp[2]).pT() + (MAOS2[1]- blm[2]).pT() ) {
          _histos["DKT"]->fill( (MAOS1[1] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[1] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[1] - blm[2]).pz()/abs(blm[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[1] - blp[2]).pz()/abs(blp[2].pz()),weight);
        }
        else {
          _histos["DKT"]->fill( (MAOS1[1] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[1] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[1] - blp[2]).pz()/abs(blp[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[1] - blm[2]).pz()/abs(blm[2].pz()),weight);
        }

        FourMomentum top1bef = _top1;
        FourMomentum top2bef = _top2;
        vector<FourMomentum> top1cbef = _top1c;
        vector<FourMomentum> top2cbef = _top2c;

        passweight += weight;
        weight = weight/4;
//        cout << "passed" << endl;

        for(size_t i = 0; i < MAOS1.size(); ++i) {
          for(size_t j = 0; j < MAOS2.size(); ++j) {

//            if(mt2reconstruct) {
              _top1 = top1bef;
              _top2 = top2bef;
              _top1c = top1cbef;
              _top2c = top2cbef;

              _top1 = _top1 + MAOS1[i];
              _top2 = _top2 + MAOS2[j];
              _top1c.push_back(MAOS1[i]);
              _top2c.push_back(MAOS2[j]);
//            }
//            else {
//              _top1 = top1bef + blp[2];
//              _top2 = top2bef + blm[2];
//            }

//            cout << _top1.mass() << "         " << _top2.mass() << endl;

            _histos["DKT_TBAR"]->fill((_top2-topbar).pT()/topbar.pT(),weight);
            _histos["DKL_TBAR"]->fill((_top2-topbar).pz()/topbar.pz(),weight);
            _histos["DKT_TOP"]->fill((_top1-top).pT()/top.pT(),weight);
            _histos["DKL_TOP"]->fill((_top1-top).pz()/top.pz(),weight);


            MatrixElementAnalysis mea;
            TLorentzVector t(_top1.px(), _top1.py(), _top1.pz(), _top1.E());
            TLorentzVector lp(_top1c[1].px(), _top1c[1].py(), _top1c[1].pz(), _top1c[1].E());
            TLorentzVector tbar(_top2.px(), _top2.py(), _top2.pz(), _top2.E());
            TLorentzVector lm(_top2c[1].px(), _top2c[1].py(), _top2c[1].pz(), _top2c[1].E());
            mea.calculate(0., t, tbar, lp, lm);

            vector<double> bts = BaumgartTweedie(t, tbar, lp, lm);

            FourMomentum ttbar = _top1+_top2;
            if(ttbar.mass() < 2000.) continue;
            _cuts["i ttbar mass"] += weight;
            double afb = _top1.rapidity() - _top2.rapidity() > 0 ? weight : -weight;


//            cout << "before fill" << endl;


            _histos["LEADINGT_MASS"]->fill(_top1.mass(),weight);
            _histos["LEADINGJ_PT"]->fill(_jets[0].pT(),weight);
            _histos["SUBLEADINGT_MASS"]->fill(_top2.mass(),weight);
            _histos["W+_MASS"]->fill( (_top1c[1]+_top1c[2]).mass(), weight);
            _histos["W-_MASS"]->fill( (_top2c[1]+_top2c[2]).mass(), weight);


            _histos["DPHI_LPLM"]->fill(fabs(lp.DeltaPhi(lm)),weight);
            _2dhistos["DPHI_LPLM_2D"]->fill(ttbar.mass(),fabs(lp.DeltaPhi(lm)),weight);
            _histos["COSTHETACOSTHETA"]->fill(mea.getHelicityTheta1() * mea.getHelicityTheta2(),weight);
            _2dhistos["COSTHETACOSTHETA_2D"]->fill(ttbar.mass(),mea.getHelicityTheta1() * mea.getHelicityTheta2(),weight);
            _histos["COSTHETACOSTHETA_UWER"]->fill(mea.getLHCOptimisedTheta1() * mea.getLHCOptimisedTheta2(),weight);
            _2dhistos["COSTHETACOSTHETA_UWER_2D"]->fill(ttbar.mass(),mea.getLHCOptimisedTheta1() * mea.getLHCOptimisedTheta2(),weight);

            _histos["BT_DIFF"]->fill(bts[0],weight);
            _2dhistos["BT_DIFF_2D"]->fill(ttbar.mass(),bts[0],weight);
            _histos["BT_SUM"]->fill(bts[1],weight);
            _2dhistos["BT_SUM_2D"]->fill(ttbar.mass(),bts[1],weight);

            MatrixElementAnalysis mea_ttbar;
            TLorentzVector tt(ttbar.px(), ttbar.py(), ttbar.pz(), ttbar.E());
            mea_ttbar.calculate(0., tt, tbar, t, lm);


            _histos["TTBAR_MASS"]->fill(ttbar.mass(),weight);
            _histos["COSTHETAPROD"]->fill(cos(ttbar.theta()) ,weight);
            _histos["COSTHETATOP"]->fill(mea_ttbar.getHelicityTheta1() ,weight);
            _2dhistos["COSTHETAPROD_2D"]->fill(ttbar.mass(),cos(ttbar.theta()),weight);
            _2dhistos["COSTHETATOP_2D"]->fill(ttbar.mass(),mea_ttbar.getHelicityTheta1(),weight);
            _histos["AFB_PRENORM"]->fill(ttbar.mass(), afb);
            _2dhistos["AL_PRENORM"]->fill(ttbar.mass(), mea.getHelicityTheta1(),weight);
            _2dhistos["AL_PRENORM"]->fill(ttbar.mass(), mea.getHelicityTheta2(),weight);
            _histos["AL_NOTBINNED"]->fill(mea.getHelicityTheta1(),weight);
            _histos["AL_NOTBINNED"]->fill(mea.getHelicityTheta2(),weight);

//            cout << "after fill" << endl;

          }
        }
      }

    }

    void finalize() {
      for(size_t i = 0; i < massbins; ++i) {
        double val = minmass + (maxmass-minmass)/(2*massbins) + i*(maxmass-minmass)/(massbins);
        if(_histos["TTBAR_MASS"]->bin(i).height() == 0)
          _histos["AFB"]->fill( val, 0);
        else
          _histos["AFB"]->fill( val, _histos["AFB_PRENORM"]->bin(i).height()/(2*_histos["TTBAR_MASS"]->bin(i).height())*100 );
      }
      for(size_t i = 0; i < massbins; ++i) {
        double val = minmass + (maxmass-minmass)/(2*massbins) + i*(maxmass-minmass)/(massbins);
        vector<double> vals(10,0);
        vector<double> xs(10,0);
        for(size_t j = 0; j < 10; ++j) {
          xs[j] = -1 + j*0.2 + 0.1;
          int index = _2dhistos["AL_PRENORM"]->binIndexAt(val,-1 + j*0.2 + 0.1);
          if(_histos["TTBAR_MASS"]->bin(i).height() == 0)
            vals[j] = 0;
          else
            vals[j] = _2dhistos["AL_PRENORM"]->bin(index).height()/_histos["TTBAR_MASS"]->bin(i).height();
        }
        double c0, c1, cov00, cov01, cov11, sumsq;
        gsl_fit_linear (xs.data(), 1, vals.data(), 1, 10, &c0, &c1, &cov00, &cov01, &cov11, &sumsq);
        cout << c1 << endl;
        _histos["AL"]->fill( val, c1 * 100 );
      }

      scale(_histos["LEADINGT_MASS"], crossSection()/totalweight );
      scale(_histos["LEADINGJ_PT"], crossSection()/totalweight );
      scale(_histos["SUBLEADINGT_MASS"], crossSection()/totalweight );
      scale(_histos["W+_MASS"], crossSection()/totalweight );
      scale(_histos["W-_MASS"], crossSection()/totalweight );
      scale(_histos["DPHI_LPLM"], crossSection()/totalweight );
      scale(_2dhistos["DPHI_LPLM_2D"], crossSection()/totalweight );
      scale(_histos["COSTHETACOSTHETA"], crossSection()/totalweight );
      scale(_histos["COSTHETAPROD"], crossSection()/totalweight );
      scale(_histos["COSTHETATOP"], crossSection()/totalweight );
      scale(_2dhistos["COSTHETAPROD_2D"], crossSection()/totalweight );
      scale(_2dhistos["COSTHETATOP_2D"], crossSection()/totalweight );
      scale(_histos["COSTHETACOSTHETA_UWER"], crossSection()/totalweight );
      scale(_2dhistos["COSTHETACOSTHETA_2D"], crossSection()/totalweight );
      scale(_2dhistos["COSTHETACOSTHETA_UWER_2D"], crossSection()/totalweight );
      scale(_histos["BT_DIFF"], crossSection()/totalweight );
      scale(_2dhistos["BT_DIFF_2D"], crossSection()/totalweight );
      scale(_histos["BT_SUM"], crossSection()/totalweight );
      scale(_2dhistos["BT_SUM_2D"], crossSection()/totalweight );
      scale(_histos["TTBAR_MASS"], crossSection()/totalweight );
      scale(_2dhistos["AL_PRENORM"], crossSection()/totalweight );
      scale(_histos["AL_NOTBINNED"], crossSection()/totalweight );
      delete ascii_in;
      cout << "Total passed cross section " << crossSection() * passweight/totalweight << endl;
      cout << "Integral of histogram " << _histos["DPHI_LPLM"]->integral(false) << endl;
      cout << "Passing rate " << (double) passweight/totalweight << endl;
      cout << "Average number of b jets " << (double) nbjets/totalweight << endl;
      cout << "Correct pairing " << (double) correct_pair/pairweight << endl;

      double norm = totalweight;
      for(auto const& ent1 : _cuts) {
        cout << "Weight for cut: " << ent1.first << " is " << ent1.second << " (efficiency: " << ent1.second/norm << " )" << endl;
        norm=ent1.second;
      }

    }

    //@}

  private:
    /// @name Objects that are used by the event selection decisions
    //@{
    vector<DressedLepton> _dressedelectrons;
    vector<DressedLepton> _dressedmuons;
    vector<DressedLepton> _dressedleptons;
    Particles _neutrinos;
    vector<FourMomentum> _top1c, _top2c;
    FourMomentum _top1,_top2;
    Jets _jets;
    Jets _bjets;
    unsigned int _jet_ntag;
    int massbins;
    double minmass;
    double maxmass;
    int cosbins;
    /// @todo Why not store the whole MET FourMomentum?
    double _met_et, _met_phi, totalweight, passweight, pairweight, correct_pair;
    bool _overlap;
    unsigned int nev,nbjets,nevb;

    std::map<std::string, Histo1DPtr> _histos;
    std::map<std::string, Histo2DPtr> _2dhistos;
    std::map<std::string, double> _cuts;
    HepMC::IO_GenEvent* ascii_in;
    HepMC::GenEvent* evt1;
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(TopPolarisation_Smearing);

}
