#! /bin/bash

dir=$1

for d in ${dir}/* do
  if [[ -d $d ]]; then
    echo $d `grep "Total passed weight of all events normalised to luminosity:" ${d}/atom_results.data`
  fi
done
exit
