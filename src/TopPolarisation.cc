// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "TLorentzVector.h"
#include "TVector3.h"
#include "TVector2.h"
#include "MatrixElementAnalysis.h"
#include "SonnenscheinEngine.h"
#include "lester_mt2_bisect.h"
#include <random>

namespace Rivet {

  bool isBMeson(int pdgId){
    if( fabs(pdgId)==511 || fabs(pdgId)==531 || fabs(pdgId)==521
             || fabs(pdgId)==5122 || fabs(pdgId)==5232 ||  fabs(pdgId)==5132 || fabs(pdgId)==5332
             || fabs(pdgId)==5322 || fabs(pdgId)==5312 || fabs(pdgId)==533 || fabs(pdgId)==513
             || fabs(pdgId)==523  || fabs(pdgId)==541 || fabs(pdgId)==543
             || fabs(pdgId)==5114 || fabs(pdgId)==5212 || fabs(pdgId)==5224 || fabs(pdgId)==5222
             || fabs(pdgId)==5214 || fabs(pdgId)==5112 || fabs(pdgId)==5324 || fabs(pdgId)==5314
             || fabs(pdgId)==5334 )
      return true;
    return false;
  }

  double _transMass(double ptLep, double phiLep, double met, double phiMet) {
    return sqrt(2.0*ptLep*met*(1 - cos(phiLep-phiMet)));
  }

  HepMC::GenVertex::particle_iterator findLepton(HepMC::GenVertex::particle_iterator p, int id, FourMomentum *ret) {
    if( !(*p)->end_vertex() ) return p;
    for ( HepMC::GenVertex::particle_iterator mother = (*p)->end_vertex()-> particles_begin(HepMC::family);
                      mother != (*p)->end_vertex()-> particles_end(HepMC::family); ++mother ) {
//      cout << (*mother)->pdg_id() << endl;
      if((*mother)->pdg_id() == id) {
        FourMomentum tmp = FourMomentum((*mother)->momentum().e(),(*mother)->momentum().px(), (*mother)->momentum().py(), (*mother)->momentum().pz());
        *ret = tmp;
        return mother;
      }
      else if ((*mother) != (*p)) findLepton(mother, id, ret);
    }
    return p;
  }

  vector<double> BaumgartTweedie(TLorentzVector t, TLorentzVector tbar, TLorentzVector lp, TLorentzVector lm) {
      const double pt = t.Pt() + tbar.Pt();

      //boost to com
      TLorentzVector ttbar = t + tbar;
      TVector3 boost_to_ttbar = -1. * ttbar.BoostVector();

      t.Boost(boost_to_ttbar);
      tbar.Boost(boost_to_ttbar);
      lp.Boost(boost_to_ttbar);
      lm.Boost(boost_to_ttbar);

      //boost from ZMF to top frame
      TVector3 boost_to_top = -1. * t.BoostVector();
      lp.Boost(boost_to_top);

      //boost from ZMF to tbar frame
      TVector3 boost_to_tbar = -1. * tbar.BoostVector();
      lm.Boost(boost_to_tbar);

      TVector3 beamZ(0, 0, 1);

      TVector3 newZ = t.Vect().Unit();
      TVector3 newY = t.Vect().Unit().Cross(beamZ).Unit();
      TVector3 newX = newY.Cross(newZ).Unit();

      TRotation m;
      m.RotateAxes(newX, newY, newZ);
      m = m.Inverse();
      lp.Transform(m);
      lm.Transform(m);

      vector<double> ret;

      ret.push_back(TVector2::Phi_mpi_pi(lp.Phi() - lm.Phi()));
      ret.push_back(TVector2::Phi_mpi_pi(lp.Phi() + lm.Phi()));

      return ret;
  }

  /// @brief top polarisation
  class TopPolarisation : public Analysis {
  public:

    /// Default constructor
    TopPolarisation() : Analysis("TopPolarisation")
    {    }


    /// @name Analysis methods
    //@{

    void init() {

      // Eta ranges
      Cut eta_full = Cuts::abseta < 5.0 && Cuts::pT > 500.0*MeV;
      Cut eta_lep = Cuts::abseta < 2.5;

      // All final state particles
      FinalState fs(eta_full);

      // Get photons to dress leptons
      IdentifiedFinalState photons(fs);
      photons.acceptIdPair(PID::PHOTON);

      // Projection to find the electrons
      IdentifiedFinalState el_id(fs);
      el_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState electrons(el_id);
      electrons.acceptTauDecays(true);
      addProjection(electrons, "electrons");
      DressedLeptons dressedelectrons(photons, electrons, 0.1, eta_lep && Cuts::pT > 25*GeV, true, true);
      addProjection(dressedelectrons, "dressedelectrons");
      DressedLeptons ewdressedelectrons(photons, electrons, 0.1, eta_full, true, true);
      addProjection(ewdressedelectrons, "ewdressedelectrons");

      // Projection to find the muons
      IdentifiedFinalState mu_id(fs);
      mu_id.acceptIdPair(PID::MUON);
      PromptFinalState muons(mu_id);
      muons.acceptTauDecays(true);
      addProjection(muons, "muons");
      vector<pair<double, double> > eta_muon;
      DressedLeptons dressedmuons(photons, muons, 0.1, eta_lep && Cuts::pT >= 25*GeV, true, true);
      addProjection(dressedmuons, "dressedmuons");
      DressedLeptons ewdressedmuons(photons, muons, 0.1, eta_full, true, true);
      addProjection(ewdressedmuons, "ewdressedmuons");

      // Projection to find neutrinos and produce MET
      IdentifiedFinalState nu_id;
      nu_id.acceptNeutrinos();
      PromptFinalState neutrinos(nu_id);
      neutrinos.acceptTauDecays(true);
      addProjection(neutrinos, "neutrinos");

      MissingMomentum met( fs );
      //met.setSmearingParams( FinalState::SELECTED, &dp.metEff( "Jet_PlaceHolder" ) );
      addProjection(met, "MissingEt");


      // Jet clustering.
      VetoedFinalState vfs;
      vfs.addVetoOnThisFinalState(ewdressedelectrons);
      vfs.addVetoOnThisFinalState(ewdressedmuons);
      vfs.addVetoOnThisFinalState(neutrinos);
      FastJets jets(vfs, FastJets::ANTIKT, 0.4);
//      jets.useInvisibles();
      addProjection(jets, "jets");

      _histos["DPHI_LPLM"]   = bookHisto1D("dphi_lplm", 20, 0, M_PI, "", "dphi(lp lm)", "Relative Occurence");
      _histos["COSTHETACOSTHETA"]   = bookHisto1D("costhetacostheta", 20, -1, 1, "", "cos theta+ cos theta-", "Relative Occurence");
      _histos["COSTHETACOSTHETA_UWER"]   = bookHisto1D("costhetacostheta_uwer", 20, -1, 1, "", "cos theta+ cos theta- Uwer basis", "Relative Occurence");
      _histos["BT_DIFF"]   = bookHisto1D("bt_diff", 20, -M_PI, M_PI, "", "dphi BT", "Relative Occurence");
      _histos["BT_SUM"]   = bookHisto1D("bt_sum", 20, -M_PI, M_PI, "", "sum phi BT", "Relative Occurence");
      _histos["LEADINGJ_PT"]   = bookHisto1D("leadingjetpt", 20, 0, 300, "", "leading jet pt", "Relative Occurence");
      _histos["LEADINGT_MASS"]   = bookHisto1D("top1mass", 20, 0, 300, "", "top1 mass", "Relative Occurence");
      _histos["SUBLEADINGT_MASS"]   = bookHisto1D("top2mass", 20, 0, 300, "", "top2 mass", "Relative Occurence");
      _histos["DKT"]   = bookHisto1D("dkt", 20, 0, 10, "", "delta kt", "Relative Occurence");
      _histos["DKL"]   = bookHisto1D("dkl", 20, -10, 10, "", "delta kl", "Relative Occurence");
      _histos["DKT_TOP"]   = bookHisto1D("dkt_top", 20, 0, 5, "", "delta kt top", "Relative Occurence");
      _histos["DKL_TOP"]   = bookHisto1D("dkl_top", 20, -5, 5, "", "delta kl top", "Relative Occurence");
      _histos["W+_MASS"]   = bookHisto1D("w+mass", 20, 0, 200, "", "w+ mass", "Relative Occurence");
      _histos["W-_MASS"]   = bookHisto1D("w-mass", 20, 0, 200, "", "w- mass", "Relative Occurence");

      nev = 0;
      nevb = 0;
      n_pairing_correct = 0;
      nbjets = 0;
      totalweight = 0.;
      passweight = 0.;

    }


    void analyze(const Event& event) {

      double weight = event.weight();
      const HepMC::GenEvent *evt = event.genEvent();
      _dressedleptons.clear();
      _bjets.clear();
      _top1c.clear();
      _top2c.clear();

      vector<FourMomentum> blp, blm;
      vector<FourMomentum> bMesons,bMestmp;
      unsigned int bs = 0;
      FourMomentum hard1, hard2;

      HepMC::GenEvent::particle_const_iterator e = evt->particles_end();
      for ( HepMC::GenEvent::particle_const_iterator p = evt->particles_begin(); p != e; ++p )
      {
        if ( isBMeson( (*p)->pdg_id() ) && (*p)->status() == 1  )
          bMestmp.push_back(FourMomentum((*p)->momentum().e(),(*p)->momentum().px(), (*p)->momentum().py(), (*p)->momentum().pz()));
        if ( abs( (*p)->pdg_id() ) == 5 )
          bs++;
        if ( (*p)->pdg_id()==6) {
          if ( (*p)->end_vertex() )
          {
            for ( HepMC::GenVertex::particle_iterator mother = (*p)->end_vertex()-> particles_begin(HepMC::family);
                  mother != (*p)->end_vertex()-> particles_end(HepMC::family); ++mother )
            {
              if ((*mother)->pdg_id() == 5) { blp.push_back(FourMomentum((*mother)->momentum().e(),(*mother)->momentum().px(), (*mother)->momentum().py(), (*mother)->momentum().pz()));  break;}
            }
            for ( HepMC::GenVertex::particle_iterator mother = (*p)->end_vertex()-> particles_begin(HepMC::family);
                  mother != (*p)->end_vertex()-> particles_end(HepMC::family); ++mother )
            {
              if ( (*mother)->pdg_id() == 24 && (*mother)->end_vertex() ) {
                FourMomentum lepton1(0.,0.,0.,0.);
                FourMomentum lepton2(0.,0.,0.,0.);
                HepMC::GenVertex::particle_iterator lep1, lep2;
                lep1 = findLepton(mother, -11, &lepton1);
                lep2 = findLepton(mother, 12, &lepton2);
                if(lepton1.pT() < 0.0001 && lepton2.pT() < 0.0001) {
                  lep1 = findLepton(mother, -13, &lepton1);
                  lep2 = findLepton(mother, 14, &lepton2);
                }
                if(lepton1.pT() > 0.0001 && lepton2.pT() > 0.0001) {
                  blp.push_back(lepton1);
                  blp.push_back(lepton2);
                }
                break;
              }
            }
          }
        }
        if ( (*p)->pdg_id()==-6) {
          if ( (*p)->end_vertex() )
          {
            for ( HepMC::GenVertex::particle_iterator mother = (*p)->end_vertex()-> particles_begin(HepMC::family);
                  mother != (*p)->end_vertex()-> particles_end(HepMC::family); ++mother )
            {
              if ((*mother)->pdg_id() == -5) { blm.push_back(FourMomentum((*mother)->momentum().e(),(*mother)->momentum().px(), (*mother)->momentum().py(), (*mother)->momentum().pz()));  break;}
            }
            for ( HepMC::GenVertex::particle_iterator mother = (*p)->end_vertex()-> particles_begin(HepMC::family);
                  mother != (*p)->end_vertex()-> particles_end(HepMC::family); ++mother )
            {
              if ( (*mother)->pdg_id() == -24 && (*mother)->end_vertex() ) {
                FourMomentum lepton1(0.,0.,0.,0.);
                FourMomentum lepton2(0.,0.,0.,0.);
                HepMC::GenVertex::particle_iterator lep1, lep2;
                lep1 = findLepton(mother, 11, &lepton1);
                lep2 = findLepton(mother, -12, &lepton2);
                if(lepton1.pT() < 0.0001 && lepton2.pT() < 0.0001) {
                  lep1 = findLepton(mother, 13, &lepton1);
                  lep2 = findLepton(mother, -14, &lepton2);
                }
                if(lepton1.pT() > 0.0001 && lepton2.pT() > 0.0001) {
                  blm.push_back(lepton1);
                  blm.push_back(lepton2);
                }
                break;
              }
            }
          }
        }
      }
      for(int i = 0; i < bMestmp.size(); ++i) {
        if ( bMestmp[i].pT() > 10. ) bMesons.push_back(bMestmp[i]);
      }

//      cout << bMesons.size() << endl;
//      cout << blp.size() << "       " << blm.size() << endl;

//      if (blp.size() != 3 || blm.size() != 3) vetoEvent;

      nev++;

      map<int,FourMomentum> bMes;
      for(unsigned int i = 0; i < bMesons.size(); ++i) {
        bMes[i] = bMesons[i];
      }

      // Get the selected objects, using the projections.
      _dressedelectrons = sortByPt(applyProjection<DressedLeptons>(event, "dressedelectrons").dressedLeptons());

      _dressedmuons = sortByPt(applyProjection<DressedLeptons>(event, "dressedmuons").dressedLeptons());

      _neutrinos = applyProjection<PromptFinalState>(event, "neutrinos").particlesByPt();

      _jets = applyProjection<FastJets>(event, "jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::abseta < 2.5);

      const MissingMomentum& met = applyProjection<MissingMomentum>(event, "MissingEt");
      FourMomentum pmet = -met.visibleMomentum();

      // Calculate the missing ET, using the prompt neutrinos only (really?)
//      /// @todo Why not use MissingMomentum?
//      FourMomentum pmet;
//      foreach (const Particle& p, _neutrinos) pmet += p.momentum();
      _met_et = pmet.pT();
      _met_phi = pmet.phi();

      std::random_device rd;
      std::mt19937 generator (rd());

      std::uniform_real_distribution<double> dis(0.0, 1.0);

      for(unsigned int ev = 0; ev < 1; ++ev) {

        totalweight += weight;

        if ( _jets.size() < 2 ) continue;

        // Check overlap of jets/leptons.
        unsigned int i,j;
        _jet_ntag = 0;
        _overlap = false;

        for (i = 0; i < _jets.size(); i++) {
          const Jet& jet = _jets[i];
          // If dR(met,jet) < 0.4 skip the event
          if(deltaR(jet,pmet) < 0.4) _overlap = true;
          // If dR(el,jet) < 0.4 skip the event
          foreach (const DressedLepton& el, _dressedelectrons) {
            if (deltaR(jet, el) < 0.4) _overlap = true;
          }
          // If dR(mu,jet) < 0.4 skip the event
          foreach (const DressedLepton& mu, _dressedmuons) {
            if (deltaR(jet, mu) < 0.4) _overlap = true;
          }
          // If dR(jet,jet) < 0.5 skip the event
          for (j = 0; j < _jets.size(); j++) {
            const Jet& jet2 = _jets[j];
            if (i == j) continue; // skip the diagonal
            if (deltaR(jet, jet2) < 0.5) _overlap = true;
          }
        }

  //      cout << _jet_ntag << endl;

        pmet.setPz(0.);
        pmet.setE(0.);

        // no overlap
        if(_overlap) continue;

        vector<int> used;
        Jets btmps;

        for(j = 0; j < bMes.size(); ++j) {
          double mindR = 1000.;
          unsigned int ind = -1;
          for(i = 0; i < _jets.size(); ++i) {
            if( deltaR(_jets[i], bMes[j]) < mindR ) {
              mindR = deltaR(_jets[i], bMes[j]);
              ind = i;
            }
          }
          if (mindR < 0.4) {
            if(std::find(used.begin(), used.end(), ind) != used.end()) {
              continue;
            } else {
              used.push_back(ind);
              btmps.push_back(_jets[ind]);
              _jet_ntag++;
            }
          }
        }

        for(i = 0; i < _jets.size(); ++i) {
          double rand = dis(generator);
          if(std::find(used.begin(), used.end(), i) != used.end()) {
            if(rand < 0.7) _bjets.push_back(_jets[i]);
          }
          else if (rand < 0.01) _bjets.push_back(_jets[i]);
        }

  //      cout << _bjets.size() << endl;
  //      cout << _jets.size() << endl;
  //      cout << endl;
        nevb++;
        nbjets+=_bjets.size();

        // need two leptons
        if(_dressedelectrons.size() + _dressedmuons.size() != 2 ) continue;
        foreach(const DressedLepton& el, _dressedelectrons) {
          _dressedleptons.push_back(el);
        }
        foreach(const DressedLepton& mu, _dressedmuons) {
          _dressedleptons.push_back(mu);
        }
        // veto same signed leptons
        if(_dressedleptons[0].charge() * _dressedleptons[1].charge() > 0) continue;
        // want two jets with b tags
        if(_jet_ntag < 2) continue;
        // want et_miss > 60 GeV
        if(_met_et < 60.) continue;
        // mtt_T2 cut, see 1109.2201v3
        FourMomentum visible = _dressedleptons[0].momentum() + _dressedleptons[1].momentum() + _bjets[0].momentum() + _bjets[1].momentum();
        double mtt_T2 = visible.mass2() + 2 * (sqrt(visible.pT() * visible.pT() + visible.mass2()) * _met_et -  visible.dot(pmet));
        if( sqrt(mtt_T2) < 277. ) continue;

        bool truth = true;

        double mVisA1 = (_dressedleptons[0].momentum()+_bjets[0].momentum()).mass(); // mass of visible object on side A.  Must be >=0.
        double pxA1 = (_dressedleptons[0].momentum()+_bjets[0].momentum()).px(); // x momentum of visible object on side A.
        double pyA1 = (_dressedleptons[0].momentum()+_bjets[0].momentum()).py(); // y momentum of visible object on side A.

        double mVisB1 = (_dressedleptons[1].momentum()+_bjets[1].momentum()).mass(); // mass of visible object on side B.  Must be >=0.
        double pxB1 = (_dressedleptons[1].momentum()+_bjets[1].momentum()).px(); // x momentum of visible object on side B.
        double pyB1 = (_dressedleptons[1].momentum()+_bjets[1].momentum()).py(); // y momentum of visible object on side B.

        double pxMiss1 = pmet.px(); // x component of missing transverse momentum.
        double pyMiss1 = pmet.py(); // y component of missing transverse momentum.

        double chiA = 0.; // hypothesised mass of invisible on side A.  Must be >=0.
        double chiB = 0.; // hypothesised mass of invisible on side B.  Must be >=0.

        double desiredPrecisionOnMt2 = 0.01; // Must be >=0.  If 0 alg aims for machine precision.  if >0, MT2 computed to supplied absolute precision.

         asymm_mt2_lester_bisect::disableCopyrightMessage();

        double MT2_1 =  asymm_mt2_lester_bisect::get_mT2(
                   mVisA1, pxA1, pyA1,
                   mVisB1, pxB1, pyB1,
                   pxMiss1, pyMiss1,
                   chiA, chiB,
                   desiredPrecisionOnMt2);

        double mVisA2 = (_dressedleptons[0].momentum()+_bjets[1].momentum()).mass(); // mass of visible object on side A.  Must be >=0.
        double pxA2 = (_dressedleptons[0].momentum()+_bjets[1].momentum()).px(); // x momentum of visible object on side A.
        double pyA2 = (_dressedleptons[0].momentum()+_bjets[1].momentum()).py(); // y momentum of visible object on side A.

        double mVisB2 = (_dressedleptons[1].momentum()+_bjets[0].momentum()).mass(); // mass of visible object on side B.  Must be >=0.
        double pxB2 = (_dressedleptons[1].momentum()+_bjets[0].momentum()).px(); // x momentum of visible object on side B.
        double pyB2 = (_dressedleptons[1].momentum()+_bjets[0].momentum()).py(); // y momentum of visible object on side B.

        double pxMiss2 = pmet.px(); // x component of missing transverse momentum.
        double pyMiss2 = pmet.py(); // y component of missing transverse momentum.

        double MT2_2 =  asymm_mt2_lester_bisect::get_mT2(
                   mVisA2, pxA2, pyA2,
                   mVisB2, pxB2, pyB2,
                   pxMiss2, pyMiss2,
                   chiA, chiB,
                   desiredPrecisionOnMt2);

        bool usefirst = false;
        bool usesecond = false;

        if ( MT2_1 < MT2_2 )
          usefirst = true;
        else
          usesecond = true;

        if ( (_dressedleptons[0].momentum() + _bjets[0].momentum()).mass() > 153.2 || (_dressedleptons[1].momentum() + _bjets[1].momentum()).mass() > 153.2 || MT2_1 > 172.5 )
          usefirst = false;

        if ( (_dressedleptons[0].momentum() + _bjets[1].momentum()).mass() > 153.2 || (_dressedleptons[1].momentum() + _bjets[0].momentum()).mass() > 153.2 || MT2_2 > 172.5 )
          usesecond = false;


        if(!usefirst && !usesecond)
          continue;


        if(!truth) {
  //        if( max((_dressedleptons[0].momentum()+_bjets[0].momentum()).mass(), (_dressedleptons[1].momentum()+_bjets[1].momentum()).mass()) < max((_dressedleptons[0].momentum()+_bjets[1].momentum()).mass() , (_dressedleptons[1].momentum()+_bjets[0].momentum()).mass()) ) {
  //          _top1 = _dressedleptons[0].momentum() + _bjets[0].momentum();
  //          _top1c.push_back(_bjets[0].momentum()); _top1c.push_back(_dressedleptons[0].momentum());
  //          _top2 = _dressedleptons[1].momentum() + _bjets[1].momentum();
  //          _top2c.push_back(_bjets[1].momentum()); _top2c.push_back(_dressedleptons[1].momentum());
  //        }
          if( MT2_1 < MT2_2 ) {
            if(_dressedleptons[0].charge() > 0) {
              _top1 = _dressedleptons[0].momentum() + _bjets[0].momentum();
              _top1c.push_back(_bjets[0].momentum()); _top1c.push_back(_dressedleptons[0].momentum());
              _top2 = _dressedleptons[1].momentum() + _bjets[1].momentum();
              _top2c.push_back(_bjets[1].momentum()); _top2c.push_back(_dressedleptons[1].momentum());
            }
            else {
              _top2 = _dressedleptons[0].momentum() + _bjets[0].momentum();
              _top2c.push_back(_bjets[0].momentum()); _top2c.push_back(_dressedleptons[0].momentum());
              _top1 = _dressedleptons[1].momentum() + _bjets[1].momentum();
              _top1c.push_back(_bjets[1].momentum()); _top1c.push_back(_dressedleptons[1].momentum());
            }
          }
          else {
            if(_dressedleptons[0].charge() > 0) {
              _top1 = _dressedleptons[0].momentum() + _bjets[1].momentum();
              _top1c.push_back(_bjets[1].momentum()); _top1c.push_back(_dressedleptons[0].momentum());
              _top2 = _dressedleptons[1].momentum() + _bjets[0].momentum();
              _top2c.push_back(_bjets[0].momentum()); _top2c.push_back(_dressedleptons[1].momentum());
            }
            else {
              _top2 = _dressedleptons[0].momentum() + _bjets[1].momentum();
              _top2c.push_back(_bjets[1].momentum()); _top2c.push_back(_dressedleptons[0].momentum());
              _top1 = _dressedleptons[1].momentum() + _bjets[0].momentum();
              _top1c.push_back(_bjets[0].momentum()); _top1c.push_back(_dressedleptons[1].momentum());
            }
          }
        }
        else {
            _top1 = blm[0] + blm[1];
            _top1c = blm;
            _top2 = blp[0] + blp[1];
            _top2c = blp;
        }
        /// At this point we have tops before adding neutrinos so still "realistic"

        double mVisA = _top1.mass(); // mass of visible object on side A.  Must be >=0.
        double pxA = _top1.px(); // x momentum of visible object on side A.
        double pyA = _top1.py(); // y momentum of visible object on side A.

        double mVisB = _top2.mass(); // mass of visible object on side B.  Must be >=0.
        double pxB = _top2.px(); // x momentum of visible object on side B.
        double pyB = _top2.py(); // y momentum of visible object on side B.

        double pxMiss = pmet.px(); // x component of missing transverse momentum.
        double pyMiss = pmet.py(); // y component of missing transverse momentum.

        double MT2 =  asymm_mt2_lester_bisect::get_mT2(
                   mVisA, pxA, pyA,
                   mVisB, pxB, pyB,
                   pxMiss, pyMiss,
                   chiA, chiB,
                   desiredPrecisionOnMt2);

        std::pair <double,double> bestsol = ben_findsols(MT2, pxA, pyA, mVisA, chiA, pxB, pyB, pxMiss, pyMiss, mVisB, chiB);

        double Ep = _top1.E();
        double kxp = _top1.px();
        double kyp = _top1.py();
        double kzp = _top1.pz();
        double mt = 172.5;
        double kx = bestsol.first;
        double ky = bestsol.second;

       double kz1 = (4*pow(Ep,2)*kzp - 8*kx*kxp*kzp -
    4*pow(kxp,2)*kzp - 8*ky*kyp*kzp - 4*pow(kyp,2)*kzp -
    4*pow(kzp,3) - 4*kzp*pow(mt,2) -
    sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
        4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
        4*pow(kyp,2)*kzp + 4*pow(kzp,3) + 4*kzp*pow(mt,2),
       2) - 4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
       (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
         4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
         4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
         pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
         4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
         4*pow(kxp,2)*ky*kyp - 2*pow(Ep,2)*pow(kyp,2) +
         4*kx*kxp*pow(kyp,2) + 2*pow(kxp,2)*pow(kyp,2) +
         4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
         pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
         4*kx*kxp*pow(kzp,2) + 2*pow(kxp,2)*pow(kzp,2) +
         4*ky*kyp*pow(kzp,2) + 2*pow(kyp,2)*pow(kzp,2) +
         pow(kzp,4) - 2*pow(Ep,2)*pow(mt,2) +
         4*kx*kxp*pow(mt,2) + 2*pow(kxp,2)*pow(mt,2) +
         4*ky*kyp*pow(mt,2) + 2*pow(kyp,2)*pow(mt,2) +
         2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
  (2.*(-4*pow(Ep,2) + 4*pow(kzp,2)));
        double EE1 = (-pow(Ep,2) + 2*kx*kxp + pow(kxp,2) + 2*ky*kyp +
    pow(kyp,2) + pow(kzp,2) +
    (4*pow(Ep,2)*pow(kzp,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (8*kx*kxp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (4*pow(kxp,2)*pow(kzp,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (8*ky*kyp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (4*pow(kyp,2)*pow(kzp,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (4*pow(kzp,4))/(-4*pow(Ep,2) + 4*pow(kzp,2)) +
    pow(mt,2) - (4*pow(kzp,2)*pow(mt,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (kzp*sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
           4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
           4*pow(kyp,2)*kzp + 4*pow(kzp,3) +
           4*kzp*pow(mt,2),2) -
         4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
          (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
            4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
            4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
            pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
            4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
            4*pow(kxp,2)*ky*kyp -
            2*pow(Ep,2)*pow(kyp,2) +
            4*kx*kxp*pow(kyp,2) +
            2*pow(kxp,2)*pow(kyp,2) +
            4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
            pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
            4*kx*kxp*pow(kzp,2) +
            2*pow(kxp,2)*pow(kzp,2) +
            4*ky*kyp*pow(kzp,2) +
            2*pow(kyp,2)*pow(kzp,2) + pow(kzp,4) -
            2*pow(Ep,2)*pow(mt,2) + 4*kx*kxp*pow(mt,2) +
            2*pow(kxp,2)*pow(mt,2) + 4*ky*kyp*pow(mt,2) +
            2*pow(kyp,2)*pow(mt,2) +
            2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)))/(2.*Ep);

        double kz2 = (4*pow(Ep,2)*kzp - 8*kx*kxp*kzp -
    4*pow(kxp,2)*kzp - 8*ky*kyp*kzp - 4*pow(kyp,2)*kzp -
    4*pow(kzp,3) - 4*kzp*pow(mt,2) +
    sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
        4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
        4*pow(kyp,2)*kzp + 4*pow(kzp,3) + 4*kzp*pow(mt,2),
       2) - 4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
       (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
         4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
         4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
         pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
         4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
         4*pow(kxp,2)*ky*kyp - 2*pow(Ep,2)*pow(kyp,2) +
         4*kx*kxp*pow(kyp,2) + 2*pow(kxp,2)*pow(kyp,2) +
         4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
         pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
         4*kx*kxp*pow(kzp,2) + 2*pow(kxp,2)*pow(kzp,2) +
         4*ky*kyp*pow(kzp,2) + 2*pow(kyp,2)*pow(kzp,2) +
         pow(kzp,4) - 2*pow(Ep,2)*pow(mt,2) +
         4*kx*kxp*pow(mt,2) + 2*pow(kxp,2)*pow(mt,2) +
         4*ky*kyp*pow(mt,2) + 2*pow(kyp,2)*pow(mt,2) +
         2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
  (2.*(-4*pow(Ep,2) + 4*pow(kzp,2)));
        double EE2 = (-pow(Ep,2) + 2*kx*kxp + pow(kxp,2) + 2*ky*kyp +
    pow(kyp,2) + pow(kzp,2) +
    (4*pow(Ep,2)*pow(kzp,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (8*kx*kxp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (4*pow(kxp,2)*pow(kzp,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (8*ky*kyp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (4*pow(kyp,2)*pow(kzp,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (4*pow(kzp,4))/(-4*pow(Ep,2) + 4*pow(kzp,2)) +
    pow(mt,2) - (4*pow(kzp,2)*pow(mt,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) +
    (kzp*sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
           4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
           4*pow(kyp,2)*kzp + 4*pow(kzp,3) +
           4*kzp*pow(mt,2),2) -
         4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
          (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
            4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
            4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
            pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
            4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
            4*pow(kxp,2)*ky*kyp -
            2*pow(Ep,2)*pow(kyp,2) +
            4*kx*kxp*pow(kyp,2) +
            2*pow(kxp,2)*pow(kyp,2) +
            4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
            pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
            4*kx*kxp*pow(kzp,2) +
            2*pow(kxp,2)*pow(kzp,2) +
            4*ky*kyp*pow(kzp,2) +
            2*pow(kyp,2)*pow(kzp,2) + pow(kzp,4) -
            2*pow(Ep,2)*pow(mt,2) + 4*kx*kxp*pow(mt,2) +
            2*pow(kxp,2)*pow(mt,2) + 4*ky*kyp*pow(mt,2) +
            2*pow(kyp,2)*pow(mt,2) +
            2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)))/(2.*Ep);

        if(EE1 != EE1 || EE2 != EE2 || kz1 != kz1 || kz2 != kz2) continue;

        vector<FourMomentum> MAOS1;
        MAOS1.push_back(FourMomentum(EE1, kx, ky, kz1));
        MAOS1.push_back(FourMomentum(EE2, kx, ky, kz2));

        Ep = _top2.E();
        kxp = _top2.px();
        kyp = _top2.py();
        kzp = _top2.pz();
        mt = 172.5;
        kx = pmet.px() - bestsol.first;
        ky = pmet.py() - bestsol.second;

        kz1 = (4*pow(Ep,2)*kzp - 8*kx*kxp*kzp -
    4*pow(kxp,2)*kzp - 8*ky*kyp*kzp - 4*pow(kyp,2)*kzp -
    4*pow(kzp,3) - 4*kzp*pow(mt,2) -
    sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
        4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
        4*pow(kyp,2)*kzp + 4*pow(kzp,3) + 4*kzp*pow(mt,2),
       2) - 4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
       (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
         4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
         4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
         pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
         4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
         4*pow(kxp,2)*ky*kyp - 2*pow(Ep,2)*pow(kyp,2) +
         4*kx*kxp*pow(kyp,2) + 2*pow(kxp,2)*pow(kyp,2) +
         4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
         pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
         4*kx*kxp*pow(kzp,2) + 2*pow(kxp,2)*pow(kzp,2) +
         4*ky*kyp*pow(kzp,2) + 2*pow(kyp,2)*pow(kzp,2) +
         pow(kzp,4) - 2*pow(Ep,2)*pow(mt,2) +
         4*kx*kxp*pow(mt,2) + 2*pow(kxp,2)*pow(mt,2) +
         4*ky*kyp*pow(mt,2) + 2*pow(kyp,2)*pow(mt,2) +
         2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
  (2.*(-4*pow(Ep,2) + 4*pow(kzp,2)));
         EE1 = (-pow(Ep,2) + 2*kx*kxp + pow(kxp,2) + 2*ky*kyp +
    pow(kyp,2) + pow(kzp,2) +
    (4*pow(Ep,2)*pow(kzp,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (8*kx*kxp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (4*pow(kxp,2)*pow(kzp,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (8*ky*kyp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (4*pow(kyp,2)*pow(kzp,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (4*pow(kzp,4))/(-4*pow(Ep,2) + 4*pow(kzp,2)) +
    pow(mt,2) - (4*pow(kzp,2)*pow(mt,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (kzp*sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
           4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
           4*pow(kyp,2)*kzp + 4*pow(kzp,3) +
           4*kzp*pow(mt,2),2) -
         4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
          (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
            4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
            4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
            pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
            4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
            4*pow(kxp,2)*ky*kyp -
            2*pow(Ep,2)*pow(kyp,2) +
            4*kx*kxp*pow(kyp,2) +
            2*pow(kxp,2)*pow(kyp,2) +
            4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
            pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
            4*kx*kxp*pow(kzp,2) +
            2*pow(kxp,2)*pow(kzp,2) +
            4*ky*kyp*pow(kzp,2) +
            2*pow(kyp,2)*pow(kzp,2) + pow(kzp,4) -
            2*pow(Ep,2)*pow(mt,2) + 4*kx*kxp*pow(mt,2) +
            2*pow(kxp,2)*pow(mt,2) + 4*ky*kyp*pow(mt,2) +
            2*pow(kyp,2)*pow(mt,2) +
            2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)))/(2.*Ep);

         kz2 = (4*pow(Ep,2)*kzp - 8*kx*kxp*kzp -
    4*pow(kxp,2)*kzp - 8*ky*kyp*kzp - 4*pow(kyp,2)*kzp -
    4*pow(kzp,3) - 4*kzp*pow(mt,2) +
    sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
        4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
        4*pow(kyp,2)*kzp + 4*pow(kzp,3) + 4*kzp*pow(mt,2),
       2) - 4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
       (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
         4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
         4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
         pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
         4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
         4*pow(kxp,2)*ky*kyp - 2*pow(Ep,2)*pow(kyp,2) +
         4*kx*kxp*pow(kyp,2) + 2*pow(kxp,2)*pow(kyp,2) +
         4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
         pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
         4*kx*kxp*pow(kzp,2) + 2*pow(kxp,2)*pow(kzp,2) +
         4*ky*kyp*pow(kzp,2) + 2*pow(kyp,2)*pow(kzp,2) +
         pow(kzp,4) - 2*pow(Ep,2)*pow(mt,2) +
         4*kx*kxp*pow(mt,2) + 2*pow(kxp,2)*pow(mt,2) +
         4*ky*kyp*pow(mt,2) + 2*pow(kyp,2)*pow(mt,2) +
         2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
  (2.*(-4*pow(Ep,2) + 4*pow(kzp,2)));
         EE2 = (-pow(Ep,2) + 2*kx*kxp + pow(kxp,2) + 2*ky*kyp +
    pow(kyp,2) + pow(kzp,2) +
    (4*pow(Ep,2)*pow(kzp,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (8*kx*kxp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (4*pow(kxp,2)*pow(kzp,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (8*ky*kyp*pow(kzp,2))/(-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (4*pow(kyp,2)*pow(kzp,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) -
    (4*pow(kzp,4))/(-4*pow(Ep,2) + 4*pow(kzp,2)) +
    pow(mt,2) - (4*pow(kzp,2)*pow(mt,2))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)) +
    (kzp*sqrt(pow(-4*pow(Ep,2)*kzp + 8*kx*kxp*kzp +
           4*pow(kxp,2)*kzp + 8*ky*kyp*kzp +
           4*pow(kyp,2)*kzp + 4*pow(kzp,3) +
           4*kzp*pow(mt,2),2) -
         4*(-4*pow(Ep,2) + 4*pow(kzp,2))*
          (pow(Ep,4) - 4*pow(Ep,2)*pow(kx,2) -
            4*pow(Ep,2)*kx*kxp - 2*pow(Ep,2)*pow(kxp,2) +
            4*pow(kx,2)*pow(kxp,2) + 4*kx*pow(kxp,3) +
            pow(kxp,4) - 4*pow(Ep,2)*pow(ky,2) -
            4*pow(Ep,2)*ky*kyp + 8*kx*kxp*ky*kyp +
            4*pow(kxp,2)*ky*kyp -
            2*pow(Ep,2)*pow(kyp,2) +
            4*kx*kxp*pow(kyp,2) +
            2*pow(kxp,2)*pow(kyp,2) +
            4*pow(ky,2)*pow(kyp,2) + 4*ky*pow(kyp,3) +
            pow(kyp,4) - 2*pow(Ep,2)*pow(kzp,2) +
            4*kx*kxp*pow(kzp,2) +
            2*pow(kxp,2)*pow(kzp,2) +
            4*ky*kyp*pow(kzp,2) +
            2*pow(kyp,2)*pow(kzp,2) + pow(kzp,4) -
            2*pow(Ep,2)*pow(mt,2) + 4*kx*kxp*pow(mt,2) +
            2*pow(kxp,2)*pow(mt,2) + 4*ky*kyp*pow(mt,2) +
            2*pow(kyp,2)*pow(mt,2) +
            2*pow(kzp,2)*pow(mt,2) + pow(mt,4))))/
     (-4*pow(Ep,2) + 4*pow(kzp,2)))/(2.*Ep);

        if(EE1 != EE1 || EE2 != EE2 || kz1 != kz1 || kz2 != kz2) continue;

        vector<FourMomentum> MAOS2;
        MAOS2.push_back(FourMomentum(EE1, kx, ky, kz1));
        MAOS2.push_back(FourMomentum(EE2, kx, ky, kz2));

        bool mt2reconstruct = false;

        if( (MAOS1[0] - blm[2]).pT() + (MAOS2[0] - blp[2]).pT() < (MAOS1[0] - blp[2]).pT() + (MAOS2[0]- blm[2]).pT() ) {
          _histos["DKT"]->fill( (MAOS1[0] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[0] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[0] - blm[2]).pz()/abs(blm[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[0] - blp[2]).pz()/abs(blp[2].pz()),weight);
        }
        else {
          _histos["DKT"]->fill( (MAOS1[0] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[0] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[0] - blp[2]).pz()/abs(blp[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[0] - blm[2]).pz()/abs(blm[2].pz()),weight);
        }
        if( (MAOS1[0] - blm[2]).pT() + (MAOS2[1] - blp[2]).pT() < (MAOS1[0] - blp[2]).pT() + (MAOS2[1]- blm[2]).pT() ) {
          _histos["DKT"]->fill( (MAOS1[0] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[1] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[0] - blm[2]).pz()/abs(blm[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[1] - blp[2]).pz()/abs(blp[2].pz()),weight);
        }
        else {
          _histos["DKT"]->fill( (MAOS1[0] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[1] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[0] - blp[2]).pz()/abs(blp[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[1] - blm[2]).pz()/abs(blm[2].pz()),weight);
        }
        if( (MAOS1[1] - blm[2]).pT() + (MAOS2[0] - blp[2]).pT() < (MAOS1[1] - blp[2]).pT() + (MAOS2[0]- blm[2]).pT() ) {
          _histos["DKT"]->fill( (MAOS1[1] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[0] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[1] - blm[2]).pz()/abs(blm[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[0] - blp[2]).pz()/abs(blp[2].pz()),weight);
        }
        else {
          _histos["DKT"]->fill( (MAOS1[1] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[0] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[1] - blp[2]).pz()/abs(blp[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[0] - blm[2]).pz()/abs(blm[2].pz()),weight);
        }
        if( (MAOS1[1] - blm[2]).pT() + (MAOS2[1] - blp[2]).pT() < (MAOS1[1] - blp[2]).pT() + (MAOS2[1]- blm[2]).pT() ) {
          _histos["DKT"]->fill( (MAOS1[1] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[1] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[1] - blm[2]).pz()/abs(blm[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[1] - blp[2]).pz()/abs(blp[2].pz()),weight);
        }
        else {
          _histos["DKT"]->fill( (MAOS1[1] - blp[2]).pT()/abs(blp[2].pT()),weight);
          _histos["DKT"]->fill( (MAOS2[1] - blm[2]).pT()/abs(blm[2].pT()),weight);
          _histos["DKL"]->fill( (MAOS1[1] - blp[2]).pz()/abs(blp[2].pz()),weight);
          _histos["DKL"]->fill( (MAOS2[1] - blm[2]).pz()/abs(blm[2].pz()),weight);
        }

        FourMomentum top1bef = _top1;
        FourMomentum top2bef = _top2;
        FourMomentum _top1MAOS;
        FourMomentum _top2MAOS;
        vector<FourMomentum> top1cbef = _top1c;
        vector<FourMomentum> top2cbef = _top2c;

        if (!mt2reconstruct) {
          weight *= 1/4.;
          _top1 = blm[0] + blm[1] + blm[2];
          _top1c = blm;
          _top2 = blp[0] + blp[1] + blp[2];
          _top2c = blp;
        }

        n_pairing_correct++;

        for(size_t i = 0; i < MAOS1.size(); ++i) {
          for(size_t j = 0; j < MAOS2.size(); ++j) {

            if(mt2reconstruct) {
              _top1 = top1bef;
              _top2 = top2bef;
              _top1c = top1cbef;
              _top2c = top2cbef;

              _top1 = _top1 + MAOS1[i];
              _top2 = _top2 + MAOS2[j];
              _top1c.push_back(MAOS1[i]);
              _top2c.push_back(MAOS2[j]);
            }
            else {
              _top1MAOS = top1bef;
              _top2MAOS = top2bef;
              _top1MAOS = _top1MAOS + MAOS1[i];
              _top2MAOS = _top1MAOS + MAOS2[j];
              _histos["DKT_TOP"]->fill((_top1MAOS-_top1).pT()/_top1.pT(),weight);
              _histos["DKL_TOP"]->fill((_top1MAOS-_top1).pz()/_top1.pz(),weight);
            }

  //         cout << _top1.mass() << "         " << _top2.mass() << endl;

            MatrixElementAnalysis mea;
            TLorentzVector t(_top1.px(), _top1.py(), _top1.pz(), _top1.E());
            TLorentzVector lp(_top1c[1].px(), _top1c[1].py(), _top1c[1].pz(), _top1c[1].E());
            TLorentzVector tbar(_top2.px(), _top2.py(), _top2.pz(), _top2.E());
            TLorentzVector lm(_top2c[1].px(), _top2c[1].py(), _top2c[1].pz(), _top2c[1].E());
            mea.calculate(1., t, tbar, lp, lm);

            vector<double> bts = BaumgartTweedie(t, tbar, lp, lm);


            _histos["LEADINGT_MASS"]->fill(_top1.mass(),weight);
            _histos["LEADINGJ_PT"]->fill(_jets[0].pT(),weight);
            _histos["SUBLEADINGT_MASS"]->fill(_top2.mass(),weight);
            _histos["W+_MASS"]->fill( (_top1c[1]+_top1c[2]).mass(), weight);
            _histos["W-_MASS"]->fill( (_top2c[1]+_top2c[2]).mass(), weight);


            _histos["DPHI_LPLM"]->fill(fabs(lp.DeltaPhi(lm)),weight);
            _histos["COSTHETACOSTHETA"]->fill(mea.getHelicityTheta1() * mea.getHelicityTheta2(),weight);
            _histos["COSTHETACOSTHETA_UWER"]->fill(mea.getLHCOptimisedTheta1() * mea.getLHCOptimisedTheta2(),weight);

            _histos["BT_DIFF"]->fill(bts[0],weight);
            _histos["BT_SUM"]->fill(bts[1],weight);

          }
        }
      }

    }

    void finalize() {
      /// @todo Normalise!
      cout << (double) passweight/totalweight << endl;
      cout << (double) nbjets/totalweight << endl;
    }

    //@}

  private:
    /// @name Objects that are used by the event selection decisions
    //@{
    vector<DressedLepton> _dressedelectrons;
    vector<DressedLepton> _dressedmuons;
    vector<DressedLepton> _dressedleptons;
    Particles _neutrinos;
    vector<FourMomentum> _top1c, _top2c;
    FourMomentum _top1,_top2;
    Jets _jets;
    Jets _bjets;
    unsigned int _jet_ntag;
    /// @todo Why not store the whole MET FourMomentum?
    double _met_et, _met_phi, totalweight, passweight;
    bool _overlap;
    unsigned int n_pairing_correct,nev,nbjets,nevb;

    std::map<std::string, Histo1DPtr> _histos;
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(TopPolarisation);

}
