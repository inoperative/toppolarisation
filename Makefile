## Makefile to build and install Rivet analyses

CC=g++
FLAGS= -Wall -Wextra
CFLAGS=-std=c++11 -m64 -fPIC -pg -I$(RIVETINCDIR) -O3 -march=native -pedantic
ATOMCFLAGS=-std=c++11 -m64 -pg -O2 $(WFLAGS) -pedantic -shared -fPIC

INCDIR=-I$(PWD)/include -I$(shell root-config --cflags) $(shell fastjet-config --cxxflags)
LIBDIR:=-L$(shell rivet-config --libdir) -L$(shell root-config --libdir --libs) -lHepMC -L$(shell fastjet-config --libs)
PREFIX:=$(shell rivet-config --prefix)
RIVETINCDIR:=$(shell rivet-config --includedir)
LDFLAGS:=$(shell rivet-config --ldflags)

ATOMINCDIR=$(shell atom-config --cppflags) $(shell root-config --cflags) -I$(PWD)/include
ATOMLDFLAGS=$(shell atom-config --ldflags)  -lAtomCore -lAtomBase -lAtomData -lAtomHepMC_IO -lAtomLHE_IO -lAtomProjections -lAtomSimulation -lAtomStdHep_IO -lAtomTools -lAtomWeight_IO -lRivetProjections -lRivetTools -lfastjet -lfastjettools -lfastjetplugins $(shell root-config --libs)


## Get the first part of the string passed
ANALYSISPATH = $(firstword $(subst :, ,$1))

all: recotest MatrixElement.o RivetTopPolarisation_Smearing.so RivetTopPolarisation_Smearing_SemiLeptonic.so
RivetTopPolarisation_Smearing.so: src/TopPolarisation_Smearing.cc MatrixElement.o SonnenscheinEngine.o
	rivet-buildplugin RivetTopPolarisation_Smearing.so src/TopPolarisation_Smearing.cc $(CFLAGS) $(INCDIR) $(LIBDIR) MatrixElement.o SonnenscheinEngine.o

RivetTopPolarisation_Smearing_SemiLeptonic.so: src/TopPolarisation_Smearing_SemiLeptonic.cc MatrixElement.o SonnenscheinEngine.o lib/HEPTopTagger.o
	rivet-buildplugin RivetTopPolarisation_Smearing_SemiLeptonic.so src/TopPolarisation_Smearing_SemiLeptonic.cc lib/HEPTopTagger.o lib/libNsubjettiness.a lib/libQjets.a $(CFLAGS) $(INCDIR)  $(LIBDIR) MatrixElement.o SonnenscheinEngine.o

RivetTopPolarisation_Smearing_CheckQuality.so: src/TopPolarisation_Smearing_CheckQuality.cc MatrixElement.o SonnenscheinEngine.o
	rivet-buildplugin RivetTopPolarisation_Smearing_CheckQuality.so src/TopPolarisation_Smearing_CheckQuality.cc $(CFLAGS) $(INCDIR) $(LIBDIR) MatrixElement.o SonnenscheinEngine.o

MatrixElement.o: src/MatrixElementAnalysis.cxx include/MatrixElementAnalysis.h
	${CC} ${CFLAGS} -o $@ -c $< -I${INCDIR} -lm ${INCDIR} ${LIBDIR}

lib/HEPTopTagger.o: HEPTopTagger/HEPTopTagger.cc HEPTopTagger/HEPTopTagger.hh
	cd HEPTopTagger; make install

atom: libAtom_TopPolarisation.so libAtom_TopPolarisation_2b.so

libAtom_TopPolarisation.so: src/ATOM_TopPolarisation.cc MatrixElement.o
	$(CC) $(ATOMCFLAGS) $(ATOMINCDIR) -o "libAtom_TopPolarisation.so" src/ATOM_TopPolarisation.cc $(ATOMLDFLAGS) MatrixElement.o
	cp -f libAtom_TopPolarisation.so $(call ANALYSISPATH, $(ATOM_ANALYSIS_PATH))

libAtom_TopPolarisation_2b.so: src/ATOM_TopPolarisation_2b.cc MatrixElement.o
	$(CC) $(ATOMCFLAGS) $(ATOMINCDIR) -o "libAtom_TopPolarisation_2b.so" src/ATOM_TopPolarisation_2b.cc $(ATOMLDFLAGS) MatrixElement.o
	cp -f libAtom_TopPolarisation_2b.so $(call ANALYSISPATH, $(ATOM_ANALYSIS_PATH))

SonnenscheinEngine.o: src/SonnenscheinEngine.cxx include/SonnenscheinEngine.h
	${CC} ${CFLAGS} -o $@ -c $< ${INCDIR}

recotest.o: util/recotest.cxx
	${CC} ${CFLAGS} -o $@ -c $< ${INCDIR}

recotest: recotest.o SonnenscheinEngine.o
	$(CC) -o recotest $(INCDIR)  recotest.o SonnenscheinEngine.o $(LIBDIR)

install: RivetTopPolarisation_Smearing.so RivetTopPolarisation_Smearing_CheckQuality.so RivetTopPolarisation_Smearing_SemiLeptonic.so
	cp -f RivetTopPolarisation_Smearing.so $(call ANALYSISPATH, $(RIVET_ANALYSIS_PATH))
	cp -f RivetTopPolarisation_Smearing_CheckQuality.so $(call ANALYSISPATH, $(RIVET_ANALYSIS_PATH))
	cp -f RivetTopPolarisation_Smearing_SemiLeptonic.so $(call ANALYSISPATH, $(RIVET_ANALYSIS_PATH))
	@export LD_LIBRARY_PATH=$(LD_LIBRARY_PATH):$(PWD)/lib
clean:
	rm -f *.o  *.so lib/*.so lib/*.o
