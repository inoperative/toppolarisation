import matplotlib.pyplot as plt
import matplotlib
import matplotlib.lines as mlines
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import matplotlib.patches as mpatches
import pickle

import numpy as np
from scipy import interpolate
from scipy import optimize

matplotlib.rcParams.update({'font.size': 26})
matplotlib.rcParams['xtick.major.size'] = 6
matplotlib.rcParams['xtick.major.width'] = 1
matplotlib.rcParams['xtick.minor.size'] = 4
matplotlib.rcParams['xtick.minor.width'] = 1
matplotlib.rcParams['ytick.major.size'] = 6
matplotlib.rcParams['ytick.major.width'] = 1
matplotlib.rcParams['ytick.minor.size'] = 4
matplotlib.rcParams['ytick.minor.width'] = 1
matplotlib.rcParams['xtick.major.pad']='8'
matplotlib.rcParams['ytick.major.pad']='10'

masses = [3000,3500,4000] # in fb
for model in ["gluon_semil/", "gluon_dilep/", "graviton_semil/", "graviton_dilep/"]:
    if "gluon" in model:
        mus = [0.01,0.1,0.5,1,2,5,10,100]
        params = ["g2.yoda", "g6.yoda"]
        maxlim = 2.
    else:
        mus = [1,10,50,100,500,1000,2000,10000]
        params = ["c1.yoda", "c2.yoda"]
        maxlim = 4.
    for param in params:
        lim_angle_mass = []
        lim_angle_mass_p1 = []
        lim_angle_mass_p2 = []
        lim_angle_mass_m1 = []
        lim_angle_mass_m2 = []
        lim_mtt_mass = []
        lim_mtt_mass_p1 = []
        lim_mtt_mass_p2 = []
        lim_mtt_mass_m1 = []
        lim_mtt_mass_m2 = []
        for dir in ["./30tev/"+model, "./35tev/"+model, "./40tev/"+model]:
            with open (dir+param+".lim", 'rb') as fp:
                result = pickle.load(fp)

            x = np.array(np.log10(mus))
            # x = np.array([np.log10(result[mu][0][1]),np.log10(result[mu][1][1]),np.log10(result[mu][2][1]),np.log10(result[mu][3][1])])

            lim_angle = []
            lim_angle_m1 = []
            lim_angle_m2 = []
            lim_angle_p1 = []
            lim_angle_p2 = []

            lim_mtt = []
            lim_mtt_m1 = []
            lim_mtt_m2 = []
            lim_mtt_p1 = []
            lim_mtt_p2 = []

            print result

            for i in range(4):
                y_angle = np.array([result[0][i][2],result[1][i][2],result[2][i][2],result[3][i][2],result[4][i][2],result[5][i][2],result[6][i][2],result[7][i][2]])
                y_angle_m1 = np.array([result[0][i][3],result[1][i][3],result[2][i][3],result[3][i][3],result[4][i][3],result[5][i][3],result[6][i][3],result[7][i][3]])
                y_angle_m2 = np.array([result[0][i][4],result[1][i][4],result[2][i][4],result[3][i][4],result[4][i][4],result[5][i][4],result[6][i][4],result[7][i][4]])
                y_angle_p1 = np.array([result[0][i][5],result[1][i][5],result[2][i][5],result[3][i][5],result[4][i][5],result[5][i][5],result[6][i][5],result[7][i][5]])
                y_angle_p2 = np.array([result[0][i][6],result[1][i][6],result[2][i][6],result[3][i][6],result[4][i][6],result[5][i][6],result[6][i][6],result[7][i][6]])

                y_mtt = np.array([result[0][i][7],result[1][i][7],result[2][i][7],result[3][i][7],result[4][i][7],result[5][i][7],result[6][i][7],result[7][i][7]])
                y_mtt_m1 = np.array([result[0][i][8], result[1][i][8], result[2][i][8], result[3][i][8], result[4][i][8], result[5][i][8], result[6][i][8], result[7][i][8]])
                y_mtt_m2 = np.array([result[0][i][9], result[1][i][9], result[2][i][9], result[3][i][9], result[4][i][9], result[5][i][9], result[6][i][9], result[7][i][9]])
                y_mtt_p1 = np.array([result[0][i][10],result[1][i][10],result[2][i][10],result[3][i][10],result[4][i][10],result[5][i][10],result[6][i][10],result[7][i][10]])
                y_mtt_p2 = np.array([result[0][i][11],result[1][i][11],result[2][i][11],result[3][i][11],result[4][i][11],result[5][i][11],result[6][i][11],result[7][i][11]])

                new_y_angle = interpolate.UnivariateSpline(x, y_angle-0.05)
                new_y_angle_m1 = interpolate.UnivariateSpline(x, y_angle_m1-0.05)
                new_y_angle_m2 = interpolate.UnivariateSpline(x, y_angle_m2-0.05)
                new_y_angle_p1 = interpolate.UnivariateSpline(x, y_angle_p1-0.05)
                new_y_angle_p2 = interpolate.UnivariateSpline(x, y_angle_p2-0.05)

                new_y_mtt = interpolate.UnivariateSpline(x, y_mtt-0.05)
                new_y_mtt_m1 = interpolate.UnivariateSpline(x, y_mtt_m1-0.05)
                new_y_mtt_m2 = interpolate.UnivariateSpline(x, y_mtt_m2-0.05)
                new_y_mtt_p1 = interpolate.UnivariateSpline(x, y_mtt_p1-0.05)
                new_y_mtt_p2 = interpolate.UnivariateSpline(x, y_mtt_p2-0.05)

                # mu_angle = optimize.brentq(new_y_angle, 0.0, 3.3, args=())
                # mu_angle_m1 = optimize.brentq(new_y_angle_m1, 0.0, 3.3, args=())
                # mu_angle_m2 = optimize.brentq(new_y_angle_m2, 0.0, 3.3, args=())
                # mu_angle_p1 = optimize.brentq(new_y_angle_p1, 0.0, 3.3, args=())
                # mu_angle_p2 = optimize.brentq(new_y_angle_p2, 0.0, 3.3, args=())
                #
                # mu_mtt = optimize.brentq(new_y_mtt, 0.0, 3.3, args=())
                # mu_mtt_m1 = optimize.brentq(new_y_mtt_m1, 0.0, 3.3, args=())
                # mu_mtt_m2 = optimize.brentq(new_y_mtt_m2, 0.0, 3.3, args=())
                # mu_mtt_p1 = optimize.brentq(new_y_mtt_p1, 0.0, 3.3, args=())
                # mu_mtt_p2 = optimize.brentq(new_y_mtt_p2, 0.0, 3.3, args=())

                # print lumis[i]
                if new_y_angle.roots().size == 0:
                    mu_angle = maxlim
                else:
                    mu_angle = min(new_y_angle.roots()  )
                if new_y_angle_m1.roots().size == 0:
                    mu_angle_m1 = maxlim
                else:
                    mu_angle_m1 = min(new_y_angle_m1.roots() )
                if new_y_angle_m2.roots().size == 0:
                    mu_angle_m2 = maxlim
                else:
                    mu_angle_m2 = min(new_y_angle_m2.roots() )
                if new_y_angle_p1.roots().size == 0:
                    mu_angle_p1 = maxlim
                else:
                    mu_angle_p1 = min(new_y_angle_p1.roots() )
                if new_y_angle_p2.roots().size == 0:
                    mu_angle_p2 = maxlim
                else:
                    mu_angle_p2 = min(new_y_angle_p2.roots() )


                if new_y_mtt.roots().size == 0:
                    mu_mtt = maxlim
                else:
                    mu_mtt = min(new_y_mtt.roots()  )
                if new_y_mtt_m1.roots().size == 0:
                    mu_mtt_m1 = maxlim
                else:
                    mu_mtt_m1 = min(new_y_mtt_m1.roots() )
                if new_y_mtt_m2.roots().size == 0:
                    mu_mtt_m2 = maxlim
                else:
                    mu_mtt_m2 = min(new_y_mtt_m2.roots() )
                if new_y_mtt_p1.roots().size == 0:
                    mu_mtt_p1 = maxlim
                else:
                    mu_mtt_p1 = min(new_y_mtt_p1.roots() )
                if new_y_mtt_p2.roots().size == 0:
                    mu_mtt_p2 = maxlim
                else:
                    mu_mtt_p2 = min(new_y_mtt_p2.roots() )

                lim_angle.append(pow(10.,mu_angle))
                lim_angle_m1.append(pow(10.,mu_angle_m1))
                lim_angle_m2.append(pow(10.,mu_angle_m2))
                lim_angle_p1.append(pow(10.,mu_angle_p1))
                lim_angle_p2.append(pow(10.,mu_angle_p2))

                lim_mtt.append(pow(10.,mu_mtt))
                lim_mtt_m1.append(pow(10.,mu_mtt_m1))
                lim_mtt_m2.append(pow(10.,mu_mtt_m2))
                lim_mtt_p1.append(pow(10.,mu_mtt_p1))
                lim_mtt_p2.append(pow(10.,mu_mtt_p2))
            lim_angle_mass.append(lim_angle[1])
            lim_angle_mass_p1.append(lim_angle_p1[1])
            lim_angle_mass_p2.append(lim_angle_p2[1])
            lim_angle_mass_m1.append(lim_angle_m1[1])
            lim_angle_mass_m2.append(lim_angle_m2[1])
            lim_mtt_mass.append(lim_mtt[1])
            lim_mtt_mass_p1.append(lim_mtt_p1[1])
            lim_mtt_mass_p2.append(lim_mtt_p2[1])
            lim_mtt_mass_m1.append(lim_mtt_m1[1])
            lim_mtt_mass_m2.append(lim_mtt_m2[1])

        plt.yscale('log')
        plt.xscale('log')
        if "graviton_semilc1" in model[:-1]+param:
            limlow = 10
            limhigh = 10000
            legloc = 3
            channel="Semi-leptonic channel"
            setting="$c=1$"
            particle="G^1"
            position=15
        if "graviton_semilc2" in model[:-1]+param:
            limlow = 10
            limhigh = 10000
            legloc = 3
            channel="Semi-leptonic channel"
            setting="$c=2$"
            particle="G^1"
            position=15
        if "graviton_dilepc1" in model[:-1]+param:
            limlow = 10
            limhigh = 10000
            legloc = 3
            channel="Di-leptonic channel"
            setting="$c=1$"
            particle="G^1"
            position=15
        if "graviton_dilepc2" in model[:-1]+param:
            limlow = 10
            limhigh = 2000
            legloc = 3
            channel="Di-leptonic channel"
            setting="$c=2$"
            particle="G^1"
            position=15


        if "gluon_semilg2" in model[:-1]+param:
            limlow = 0.01
            limhigh = 10
            legloc = 3
            channel="Semi-leptonic channel"
            setting="$g_{t_R}=2$"
            particle="g^1"
            position=0.15
        if "gluon_semilg6" in model[:-1]+param:
            limlow = 0.01
            limhigh = 10
            legloc = 3
            channel="Semi-leptonic channel"
            setting="$g_{t_R}=6$"
            particle="g^1"
            position=0.15
        if "gluon_dilepg2" in model[:-1]+param:
            limlow = 0.1
            limhigh = 20
            legloc = 3
            channel="Di-leptonic channel"
            setting="$g_{t_R}=2$"
            particle="g^1"
            position=0.15
        if "gluon_dilepg6" in model[:-1]+param:
            limlow = 0.1
            limhigh = 20
            legloc = 3
            channel="Di-leptonic channel"
            setting="$g_{t_R}=6$"
            particle="g^1"
            position=0.15

        plt.ylim([limlow,limhigh])
        # plt.plot(np.linspace(0,3), new_y(np.linspace(0,3)), color='black',linestyle='dotted', linewidth=2)
        polygon1 = Polygon(np.array([[masses[0],lim_angle_mass_p1[0]], [masses[0],lim_angle_mass_m1[0]], [masses[1],lim_angle_mass_m1[1]],[masses[2],lim_angle_mass_m1[2]],[masses[2],lim_angle_mass_p1[2]],[masses[1],lim_angle_mass_p1[1]]]), True,color='lime')
        polygon2 = Polygon(np.array([[masses[0],lim_angle_mass_p2[0]], [masses[0],lim_angle_mass_m2[0]], [masses[1],lim_angle_mass_m2[1]],[masses[2],lim_angle_mass_m2[2]],[masses[2],lim_angle_mass_p2[2]],[masses[1],lim_angle_mass_p2[1]]]), True,color='yellow')
        p2=PatchCollection([polygon2],color='yellow')
        p1=PatchCollection([polygon1],color='lime')
        fig, ax = plt.subplots()
        ax.set_yscale('log')
        ax.set_ylim([limlow,limhigh])

        ax.add_collection(p2)
        ax.add_collection(p1)
        sigma1 = mpatches.Patch(color='lime')
        sigma2 = mpatches.Patch(color='yellow')

        plt.plot(masses, lim_angle_mass, color='black',linestyle='dotted', linewidth=2)
        plt.plot(masses, lim_mtt_mass, color='red',linestyle='dotted', linewidth=2)
        angle=mlines.Line2D([], [], color='black',linestyle='dotted', linewidth=2)
        mtt=mlines.Line2D([], [], color='red',linestyle='dotted', linewidth=2)
        tmp=mlines.Line2D([], [], color='red',linestyle=' ', linewidth=2)
        ax.set_xticks([3000,3500, 4000])
        plt.legend([angle,sigma1,sigma2,mtt,tmp],(r"$\theta_l+m_{tt}$", r"$\pm 1 \sigma$", r"$\pm 2 \sigma$", "$m_{tt}$", " "),loc=legloc, numpoints=1, fontsize=22,frameon=False,labelspacing=0.2,handlelength=2.6,handletextpad=0.5)
        plt.text(3050,position,channel+" "+setting+", 10 fb$^{-1}$", fontsize = 18)
        plt.xlabel(r"$m("+particle+")$ [GeV]", fontsize = 24)
        plt.ylabel(r"95% C.L. limit on $\mu$", fontsize = 22)
        #
        plt.tight_layout()
        plt.savefig(model[:-1]+param+"mass_limits.pdf", dpi=100)
