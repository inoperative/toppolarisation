#!/bin/bash

source /export0/scratch/karl/Install/Rivet-2.5.3/rivetenv.sh

for i in ./30tev/graviton_dilep/ ./35tev/graviton_dilep/ ./40tev/graviton_dilep/ ./30tev/graviton_semil/ ./35tev/graviton_semil/ ./40tev/graviton_semil/ ./30tev/gluon_dilep/ ./35tev/gluon_dilep/ ./40tev/gluon_dilep/ ./30tev/gluon_semil/ ./35tev/gluon_semil/ ./40tev/gluon_semil/
do
	echo $RIVET_ANALYSIS_PATH
	echo $i
	nohup python calculate_limit.py $i &
done
exit 0
